import React from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
// Components
import { Stack, Text } from '@chakra-ui/react';
import { ChevronLeftIcon, ChevronRightIcon } from '../Icons';

import IconButtonWithTooltip from '../IconButtonWithTooltip/IconButtonWithTooltip';

function ListNav({
  currentPage,
  currentSize,
  disablePrev,
  disableNext,
  handlePrev,
  handleNext,
  itemCount,
  total,
}) {
  const firstItemCount = 1 + currentSize * (currentPage - 1);
  const lastItemCount =
    firstItemCount +
    (itemCount > currentPage * currentSize
      ? currentPage * currentSize
      : itemCount) -
    1;

  return (
    <Stack justifyContent='space-between' isInline>
      <Stack alignItems='center' isInline>
        <IconButtonWithTooltip
          icon={<ChevronLeftIcon />}
          label='Prev Page'
          placement='left'
          onClick={handlePrev}
          disabled={disablePrev}
          data-testid='handleprev'
        />
        <Text>{`${firstItemCount} - ${lastItemCount} of ${total}`}</Text>
        <IconButtonWithTooltip
          id='test'
          icon={<ChevronRightIcon />}
          label='Next Page'
          placement='left'
          onClick={handleNext}
          disabled={disableNext}
          data-testid='handlenext'
        />
      </Stack>
    </Stack>
  );
}

ListNav.defaultProps = {
  currentPage: 1,
  currentSize: 25,
  disablePrev: false,
  disableNext: false,
  handlePrev: () => undefined,
  handleNext: () => undefined,
  itemCount: 10,
  total: 25,
};

ListNav.propTypes = {
  currentPage: PropTypes.number,
  currentSize: PropTypes.number,
  disablePrev: PropTypes.bool,
  disableNext: PropTypes.bool,
  handlePrev: PropTypes.func,
  handleNext: PropTypes.func,
  itemCount: PropTypes.number,
  total: PropTypes.number,
};

export default observer(ListNav);
