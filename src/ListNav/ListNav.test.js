import { screen, render } from '../../util/Testing/CustomRender/TestingUtils';
import ListNav from './ListNav';

describe('<ListNav />', () => {
  const currentPage = 1;
  const currentSize = 10;
  const disablePrev = jest.fn();
  const disableNext = false;
  const handlePrev = jest.fn();
  const handleNext = jest.fn();
  const itemCount = 100;
  const total = 1000;
  const setup = () => {
    render(
      <ListNav
        currentPage={currentPage}
        currentSize={currentSize}
        disableNext={disableNext}
        disablePrev={disablePrev}
        handlePrev={handlePrev}
        itemCount={itemCount}
        total={total}
        handleNext={handleNext}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('1 - 10 of 1000')).toBeDefined();
  });
});
