import React from 'react';
import { Stack } from '@chakra-ui/react';

function ListNav() {
  return (
    <Stack justifyContent='space-between' isInline>
      <Stack alignItems='center' isInline>
        <div>MOCK PREV PAGE</div>
        <div>MOCK NEXT PAGE</div>
      </Stack>
    </Stack>
  );
}

export default ListNav;
