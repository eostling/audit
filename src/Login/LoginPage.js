import React from 'react';
import { useStore } from 'mobx-store-provider';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Checkbox,
  Divider,
  Flex,
  FormControl,
  FormErrorMessage,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  Spinner,
  Stack,
  Text,
} from '@chakra-ui/react';
// import usePageTitle from '../../hooks/PageTitle/page-title';
// import { userStoreId } from '../../models/User/User';
import { AccountIcon, AirForceIcon } from '../Icons';
import PasswordInput from '../Appbar/User/Password/PasswordInput';
import UserAgreement from '../Appbar/User/UserAgreement/UserAgreement';

export default function Login() {
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(null);
  const [accepted, setAccepted] = React.useState(false);
  const usernameRef = React.useRef();
  const history = useHistory();
  // const user = useStore(userStoreId);
  const { handleSubmit, errors, formState, register, reset } = useForm();
  const badgeRadius = 3;


  // const handleLogin = async ({ username, password }) => {
  //   setLoading(true);
  //   setError(null);
  //
  //   const userResponse = await user.login({ username, password });
  //
  //   if (userResponse instanceof Error) {
  //     setLoading(false);
  //     setError(userResponse.response.data);
  //
  //     if (userResponse.response.data === 'Unauthorized') {
  //       setError('Credentials could not be verified.');
  //     }
  //
  //     reset();
  //     usernameRef.current.focus();
  //   } else {
  //     history.push('/');
  //   }
  // };

  return (
  <>LOGIN PAGE TEMP</>
  );
}

// <Flex
//     minHeight='100vh'
//     width='100vw'
//     overflowX='hidden'
//     overflowY='auto'
//     flexDirection='column'
//     alignItems='center'
//     justifyContent='center'
//     paddingBottom='10px'
//     bgGradient='linear(135deg, gray.700, gray.900)'
// >
//   <Flex
//       alignItems='center'
//       justifyContent='center'
//       bg='gray.800'
//       height={`${badgeRadius * 2}em`}
//       width={`${badgeRadius * 2}em`}
//       borderRadius='50%'
//       marginTop={2}
//       mb={`-${badgeRadius}em`}
//       shadow='sm'
//       zIndex='docked'
//   >
//     <AirForceIcon
//         height={`${badgeRadius + 1}em`}
//         width={`${badgeRadius + 1}em`}
//         mt={`${badgeRadius / 6}em`}
//     />
//   </Flex>
//   <Box
//       w={{ base: '90%', xl: '72em' }}
//       px={8}
//       py={4}
//       pt={`${badgeRadius + 1}em`}
//       color='gray.100'
//       bg='gray.700'
//       borderRadius='md'
//       shadow='lg'
//   >
//     <Stack spacing={4}>
//       <Heading
//           as='h1'
//           textAlign='center'
//           textTransform='uppercase'
//           letterSpacing='0.15em'
//       >
//         Prime
//       </Heading>
//       <Heading as='h2' fontSize='md' color='gray.300' textAlign='center'>
//         Base Defense Operations Center
//       </Heading>
//       {error && (
//           <Alert status='error'>
//             <AlertIcon />
//             {error}
//           </Alert>
//       )}
//       <Divider />
//       <Box mt={2} p={4} bg='gray.800' borderRadius='md'>
//         <UserAgreement />
//       </Box>
//       <Divider />
//       <Flex justifyContent='center'>
//         <form onSubmit={handleSubmit(handleLogin)}>
//           <Stack spacing={4} width='25em'>
//             <FormControl isInvalid={errors.username}>
//               <InputGroup>
//                 <InputLeftElement>
//                   <AccountIcon />
//                 </InputLeftElement>
//                 <Input
//                     name='username'
//                     type='text'
//                     placeholder='Username'
//                     ref={(el) => {
//                       register(el, { required: true });
//                       usernameRef.current = el;
//                     }}
//                     isDisabled={loading}
//                 />
//               </InputGroup>
//               <FormErrorMessage>
//                 {errors.username && 'Username is required'}
//               </FormErrorMessage>
//             </FormControl>
//             <FormControl isInvalid={errors.password}>
//               <PasswordInput
//                   name='password'
//                   placeholder='Password'
//                   forwardRef={register({ required: true })}
//                   isDisabled={loading}
//               />
//               <FormErrorMessage>
//                 {errors.password && 'Password is required'}
//               </FormErrorMessage>
//             </FormControl>
//             <FormControl textAlign='center'>
//               <Checkbox
//                   name='accept'
//                   isChecked={accepted}
//                   onChange={(e) => setAccepted(e.target.checked)}
//               >
//                 Accept User Agreement
//               </Checkbox>
//             </FormControl>
//             <Divider />
//             {loading ? (
//                 <Flex alignItems='center' justifyContent='center'>
//                   <Spinner mr={2} />
//                   <Text>Verifying credentials...</Text>
//                 </Flex>
//             ) : (
//                 <Button
//                     mt={2}
//                     width='100%'
//                     isDisabled={!accepted}
//                     title={
//                       accepted
//                           ? undefined
//                           : 'You must accept the User Agreement to log in'
//                     }
//                     isLoading={formState.isSubmitting}
//                     colorScheme='blue'
//                     type='submit'
//                     shadow='md'
//                 >
//                   Login
//                 </Button>
//             )}
//           </Stack>
//         </form>
//       </Flex>
//     </Stack>
//   </Box>
// </Flex>
