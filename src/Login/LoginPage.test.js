import renderer from 'react-test-renderer';
import { Alert, Spinner } from '@chakra-ui/react';
import LoginPage from './LoginPage';
import StyleProvider from '../Providers/StyleProvider/StyleProvider';
import StoreProvider from '../Providers/StoreProvider/StoreProvider';
import { loginUser } from '../../services/Auth/auth';

let mockUser = {
  username: 'testUser',
  role: 'testRole',
  permissions: [],
};
jest.mock('../../services/Auth/auth', () => {
  return {
    ...jest.requireActual('../../services/Auth/auth'),
    loginUser: jest.fn(async () => {
      return mockUser;
    }),
  };
});
const mockPush = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockPush,
  }),
}));
describe('Login page', () => {
  let tree;
  let focused = false;
  beforeEach(() => {
    tree = renderer.create(
      <StyleProvider>
        <StoreProvider>
          <LoginPage />
        </StoreProvider>
      </StyleProvider>,
      {
        createNodeMock: (element) => {
          if (element.type === 'input') {
            // mock a focus function
            return {
              focus: () => {
                focused = true;
              },
            };
          }
          return null;
        },
      }
    );
  });
  test('matches snapshot', () => {
    expect(tree).toMatchSnapshot();
  });
  test('successful submit', async () => {
    const form = tree.root.findByType('form');
    await renderer.act(async () => {
      await form.props.onSubmit();
    });
    expect(loginUser.mock.calls.length).toBe(1);
    expect(mockPush.mock.calls.length).toBe(1);
    const spinners = tree.root.findAllByType(Spinner);
    expect(spinners.length).toBe(1);
  });
  test('failed login', async () => {
    const form = tree.root.findByType('form');
    const mockError = new Error('test');
    mockError.response = {
      data: 'test',
    };
    mockUser = mockError;
    await renderer.act(async () => {
      await form.props.onSubmit();
    });
    expect(focused).toBe(true);
    const errors = tree.root.findAllByType(Alert);
    expect(errors.length).toBe(1);
    const spinners = tree.root.findAllByType(Spinner);
    expect(spinners.length).toBe(0);
  });
});
