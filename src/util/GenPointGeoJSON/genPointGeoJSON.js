// Match a sensor to an alarm and get lat lon
const getLatLon = (sensors, alarm) => {
  if (sensors) {
    const match = sensors.currentPageItems.find(
      (sensor) =>
        sensor.sensorType === alarm.sensorType &&
        sensor.sensorName === alarm.sensorName
    );
    if (match) {
      return [match.lon, match.lat];
    }
  }
  return [alarm.lon, alarm.lat];
};

// Given an array of markers, generate a point geojson
export default function genPointGeoJSON(items, sensors) {
  const features = items.map((alarm) => ({
    type: 'Feature',
    id: alarm.alarmID,
    properties: {
      ...alarm,
    },
    geometry: {
      type: 'Point',
      coordinates: getLatLon(sensors, alarm),
    },
  }));

  return {
    type: 'FeatureCollection',
    features,
  };
}
