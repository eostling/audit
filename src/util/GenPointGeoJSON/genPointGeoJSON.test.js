import genPointGeoJSON from './genPointGeoJSON';

const mockAlarms = [
  {
    sensorName: 'TestSensorA',
    alarmID: 1234567890,
    lat: 35.35,
    lon: -77.94,
    geohash: 'dq2945r9z',
    facilityID: 'HQ-Wing',
    state: 'SECURE',
  },
  {
    sensorName: 'TestSensorB',
    alarmID: 1234567891,
    lat: 35.01,
    lon: -77.99,
    geohash: 'dq2945r9x',
    facilityID: 'HQ-Wing',
    state: 'ACCESS',
  },
  {
    sensorName: 'TestSensorC',
    alarmID: 1234567892,
    lat: 35.19,
    lon: -77.86,
    geohash: 'dq2945r98',
    facilityID: 'HQ-Wing',
    state: 'ALARM',
  },
];

describe('Generate point GeoJSON util', () => {
  test('generate list of point GeoJSON given list of markers', () => {
    const { features } = genPointGeoJSON(mockAlarms);
    expect(features.length).toBe(3);
    expect(features[0].id).toBe(1234567890);
    expect(features[0].geometry.type).toBe('Point');
    expect(features[0].geometry.coordinates[0]).toBe(-77.94);
    expect(features[0].geometry.coordinates[1]).toBe(35.35);
    expect(features[0].properties.state).toBe('SECURE');
    expect(features[1].properties.state).toBe('ACCESS');
    expect(features[2].properties.state).toBe('ALARM');
  });
});
