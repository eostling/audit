import getAlarmsFromMarkers from './getAlarmsFromMarkers';

const mockAlarms = [
  { id: 1, name: 'testA' },
  { id: 2, name: 'testB' },
  { id: 3, name: 'testC' },
];

const markers = [
  { alarms: [mockAlarms[0], mockAlarms[1]] },
  { alarms: [mockAlarms[2]] },
];

describe('Get alarms from markers utils', () => {
  test('get list of alarms given a list of markers', () => {
    const alarms = getAlarmsFromMarkers(markers);
    expect(alarms).toStrictEqual(mockAlarms);
  });
});
