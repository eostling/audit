// Given a list of markers, flat map the alarms
export default function getAlarmsFromMarkers(markers) {
  return markers.flatMap((marker) => {
    return marker.alarms;
  });
}
