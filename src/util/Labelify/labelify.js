// Given a string, replace the underscores and hyphens with spaces
// and preserve case
export default function labelify(string) {
  return string.replace(/[_-]/g, ' ');
}
