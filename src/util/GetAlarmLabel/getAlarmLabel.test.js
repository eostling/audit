import getAlarmLabel from './getAlarmLabel';

const mockAlarm = {
  sensorName: 'testThing1',
  alarmType: 'INTRUSION',
};

describe('Alarm label util', () => {
  test('get the label for an alarm', () => {
    const alarmLabel = getAlarmLabel(mockAlarm);
    expect(alarmLabel).toBe('testThing1 - INTRUSION');
  });
});
