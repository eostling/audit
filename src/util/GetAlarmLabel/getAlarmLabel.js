import labelify from '../Labelify/labelify';

// Given an alarm, generate a readable label
export default function getAlarmLabel(alarm) {
  if (alarm && alarm.sensorName && alarm.alarmType) {
    return `${labelify(alarm.sensorName)} - ${labelify(alarm.alarmType)}`;
  }
  return 'UNKNOWN ALARM';
}
