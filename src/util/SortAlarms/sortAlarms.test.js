/*
 * Copyright (C) 2020 Novetta Solutions, Inc.
 * All rights reserved
 *
 */
import sortAlarms from './sortAlarms';

const alarms = [
  {
    state: 'UNKNOWN',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
  {
    state: 'ACCESS',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
  {
    state: 'SECURE',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
  {
    state: 'ACKNOWLEDGED',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
  {
    state: 'ALARM',
    priority: 2,
    alarmTime: 1595603238241,
    facilityID: 'Facility1',
  },
  {
    state: 'ALARM',
    priority: 2,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
  {
    state: 'ALARM',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility2',
  },
  {
    state: 'ALARM',
    priority: 1,
    alarmTime: 1595603238240,
    facilityID: 'Facility1',
  },
];

const expected = [
  alarms[7],
  alarms[6],
  alarms[5],
  alarms[4],
  alarms[0],
  alarms[3],
  alarms[1],
  alarms[2],
];

describe('Sort alarms util', () => {
  test('sorts a list of alarms', () => {
    const result = sortAlarms(alarms);
    expect(result).toStrictEqual(expected);
  });
});
