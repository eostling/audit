import { STATE_ORDER } from '../Constants/constants';

// Given a list of alarms, sort by:
// state > priority > time > facilityID
export default function sortAlarms(alarms) {
  return alarms.slice().sort((a, b) => {
    // Sort by state
    const stateA = STATE_ORDER.indexOf(a.state);
    const stateB = STATE_ORDER.indexOf(b.state);
    if (stateA !== stateB) {
      return stateA < stateB ? -1 : 1;
    }
    // Sort by priority asc
    if (a.priority !== b.priority) {
      return a.priority < b.priority ? -1 : 1;
    }
    // Sort by time asc (oldest first)
    if (a.alarmTime !== b.alarmTime) {
      return a.alarmTime < b.alarmTime ? -1 : 1;
    }
    // Sort by facilityID name
    if (a.facilityID !== b.facilityID) {
      return a.facilityID < b.facilityID ? -1 : 1;
    }
    // Keep order
    return 0;
  });
}
