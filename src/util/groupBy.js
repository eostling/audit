export default function groupBy(arr, property) {
  return arr.reduce(function (sensors, x) {
    const memo = sensors;
    if (!memo[x[property]]) {
      memo[x[property]] = [];
    }
    memo[x[property]].push(x);
    return memo;
  }, {});
}
