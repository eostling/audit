import { STATE_TRANSITIONS } from '../Constants/constants';

// Given a starting state, return an array of all possible previous states
export default function getPrevStates(state) {
  return STATE_TRANSITIONS.reduce((result, [start, end]) => {
    if (end === state) {
      result.push(start);
    }
    return result;
  }, []);
}
