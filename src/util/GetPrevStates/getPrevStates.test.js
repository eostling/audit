import getPrevStates from './getPrevStates';

describe('Get previous states util', () => {
  test('gets previous ACKNOWLEDGED states', () => {
    const prevAlarmStates = getPrevStates('ACKNOWLEDGED');
    expect(prevAlarmStates).toStrictEqual(['ALARM']);
  });
  test('gets previous ACCESS states', () => {
    const prevAccessStates = getPrevStates('ACCESS');
    expect(prevAccessStates).toStrictEqual(['ACKNOWLEDGED', 'SECURE']);
  });
  test('fails to empty list', () => {
    const prevTestStates = getPrevStates('DOES_NOT_EXIST');
    expect(prevTestStates).toStrictEqual([]);
  });
});
