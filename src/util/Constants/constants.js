// Alarm state sort order
export const STATE_ORDER = [
  'ALARM',
  'UNKNOWN',
  'ACKNOWLEDGED',
  'ACK_UNKNOWN',
  'ACCESS',
  'SECURE',
];

// TODO: move state transition logic to picard-js so that it can be shared between multiple codebases
// All the valid state transitions that can be triggered by a user
// e.g. if the current state is ALARM, user can transition to ACKNOWLEDGE
export const STATE_TRANSITIONS = [
  ['ALARM', 'ACKNOWLEDGED'],
  ['ACKNOWLEDGED', 'ACCESS'],
  ['ACKNOWLEDGED', 'SECURE'],
  ['SECURE', 'ACCESS'],
  ['ACCESS', 'SECURE'],
  ['UNKNOWN', 'ACK_UNKNOWN'],
];
