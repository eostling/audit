import getThemeColor from './getThemeColor';

describe('Get theme color util', () => {
  test('can get theme color', () => {
    const activeNavColor = getThemeColor('activeNav');
    const lightBlueColor = getThemeColor('blue.200');
    const secureGreenColor = getThemeColor('states.SECURE');
    const greenColor = getThemeColor('green.600');
    const notFoundColor = getThemeColor('foo');
    const notFoundNestedColor = getThemeColor('states.foo');

    expect(activeNavColor).toBe(lightBlueColor);
    expect(lightBlueColor).toBe('#90cdf4');
    expect(secureGreenColor).toBe(greenColor);
    expect(greenColor).toBe('#2f855a');
    expect(notFoundColor).toBe('#000000');
    expect(notFoundNestedColor).toBe('#000000');
  });
});
