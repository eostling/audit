import theme from '../../theme';

// Given a selector, get the color value from the app theme, defaulting to black
export default function getThemeColor(selector) {
  const { colors } = theme;

  try {
    return selector
      .split('.')
      .reduce((obj, idx) => obj[idx], colors)
      .toLowerCase();
  } catch (e) {
    return '#000000';
  }
}
