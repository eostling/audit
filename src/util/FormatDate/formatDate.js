import { DateTime } from 'luxon';

// Given a PAPI date and output format, format the date
export default function formatDate(date, toFormat) {
  const dateTime = DateTime.fromMillis(date);
  return dateTime.setLocale('en-us').toLocaleString(toFormat);
}
