import { Settings } from 'luxon';
import formatDate from './formatDate';

describe('Format date util', () => {
  beforeAll(() => {
    Settings.defaultZoneName = 'utc';
  });

  test('formats epoch date to luxon medium format', () => {
    const date = 1595603238240;
    const formattedDate = formatDate(date);
    expect(formattedDate).toBe('7/24/2020');
  });
});
