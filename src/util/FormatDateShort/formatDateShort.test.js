import { Settings } from 'luxon';
import formatDateShort from './formatDateShort';

describe('Short date format util', () => {
  beforeAll(() => {
    Settings.defaultZoneName = 'utc';
  });

  test('formats epoch date to luxon medium format', () => {
    const date = 1595603238240;
    const formattedDate = formatDateShort(date);
    expect(formattedDate).toBe('7/24/2020');
  });
});
