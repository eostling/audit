import { DateTime } from 'luxon';
import formatDate from '../FormatDate/formatDate';

// Given a PAPI date string, format to medium length date with weekday
export default function formatDateShort(date) {
  return formatDate(date, DateTime.DATE_SHORT);
}
