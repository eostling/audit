/*
 * Copyright (C) 2020 Novetta Solutions, Inc.
 * All rights reserved
 *
 */

import { Settings } from 'luxon';
import formatTime24 from './formatTime24';

describe('24 hour time format util', () => {
  beforeAll(() => {
    Settings.defaultZoneName = 'utc';
  });

  test('formats epoch date to luxon 24 hour format', () => {
    const date = 1595603238240;
    const formattedTime = formatTime24(date);
    expect(formattedTime).toBe('15:07:18');
  });
});
