import { DateTime } from 'luxon';
import formatDate from '../FormatDate/formatDate';

// Given a PAPI date string, format to 24HR time
export default function formatTime24(date) {
  return formatDate(date, DateTime.TIME_24_WITH_SECONDS);
}
