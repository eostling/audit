/*
 * Copyright (C) 2020 Novetta Solutions, Inc.
 * All rights reserved
 *
 */
import idify from './idify';

describe('Idify util', () => {
  test('convert string to a valid ID', () => {
    const id = idify('test_Thing:1');
    expect(id).toBe('test-thing-1');
  });
});
