// Given a string, replace underscores and colons with hyphens
// then convert it to all lower case
export default function idify(string) {
  return string.replace(/[_:]/g, '-').toLowerCase();
}
