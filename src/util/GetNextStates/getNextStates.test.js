import getNextStates from './getNextStates';

describe('Get next states util', () => {
  test('gets next ALARM states', () => {
    const nextAlarmStates = getNextStates('ALARM');
    expect(nextAlarmStates).toStrictEqual(['ACKNOWLEDGED']);
  });
  test('gets next ACKNOWLEDGED states', () => {
    const nextAcknowledgedStates = getNextStates('ACKNOWLEDGED');
    expect(nextAcknowledgedStates).toStrictEqual(['ACCESS', 'SECURE']);
  });
  test('fails to empty list', () => {
    const nextTestStates = getNextStates('DOES_NOT_EXIST');
    expect(nextTestStates).toStrictEqual([]);
  });
});
