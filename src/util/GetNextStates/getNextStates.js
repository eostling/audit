import { STATE_TRANSITIONS } from '../Constants/constants';

// Given a starting state, return an array of all possible next states
export default function getNextStates(state) {
  return STATE_TRANSITIONS.reduce((result, [start, end]) => {
    if (start === state) {
      result.push(end);
    }
    return result;
  }, []);
}
