import React from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { render } from '@testing-library/react';
import PrivateStoreProvider from '../../../components/Providers/PrivateStoreProvider/PrivateStoreProvider';
import StyleProvider from '../../../components/Providers/StyleProvider/StyleProvider';
import StoreProvider from '../../../components/Providers/StoreProvider/StoreProvider';

// eslint-disable-next-line react/prop-types
const TestingUtils = ({ children }) => {
  return (
    <StyleProvider>
      <StoreProvider>
        <PrivateStoreProvider>{children}</PrivateStoreProvider>
      </StoreProvider>
    </StyleProvider>
  );
};

const customRender = (ui, options) =>
  render(ui, { wrapper: TestingUtils, ...options });

// eslint-disable-next-line import/no-extraneous-dependencies
export * from '@testing-library/react';

// override render method
export { customRender as render };
