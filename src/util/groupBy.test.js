/*
 * Copyright (C) 2020 Novetta Solutions, Inc.
 * All rights reserved
 *
 */
import groupBy from './groupBy';

describe('groupBy test', () => {
  test('splitting array of object by property', () => {
    const sensors = [
      {
        sensorName: 'HQ_Wing_PIR001',
        floor: 1,
      },
      {
        sensorName: 'HQ_Wing_PIR002',
        floor: 1,
      },
      {
        sensorName: 'HQ_Wing_PIR003',
        floor: 2,
      },
      {
        sensorName: 'HQ_Wing_PIR002',
        floor: 2,
      },
    ];
    const expected = {
      1: [
        {
          sensorName: 'HQ_Wing_PIR001',
          floor: 1,
        },
        {
          sensorName: 'HQ_Wing_PIR002',
          floor: 1,
        },
      ],
      2: [
        {
          sensorName: 'HQ_Wing_PIR003',
          floor: 2,
        },
        {
          sensorName: 'HQ_Wing_PIR002',
          floor: 2,
        },
      ],
    };
    const result = groupBy(sensors, 'floor');
    expect(result).toEqual(expected);
  });
});
