// Given a string, capitalize each word
export default function titleCase(str) {
  const tokens = str.toLowerCase().split(' ');
  const upperTokens = tokens.map((token) => {
    return `${token.charAt(0).toUpperCase()}${token.slice(1)}`;
  });
  return upperTokens.join(' ');
}
