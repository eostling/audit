import genBlueForceGeoJSON from './genBlueForceGeoJSON';

const blueForceDevices = [
  {
    sensorName: 'sensor1',
    latitude: 35.35,
    longitude: -77.94,
  },
  {
    sensorName: 'sensor2',
    latitude: 35.64,
    longitude: -77.32,
  },
];

describe('Generate Blue Force device point GeoJSON util', () => {
  test('generate list of point GeoJSON given list of markers', () => {
    const { features } = genBlueForceGeoJSON(blueForceDevices);
    expect(features.length).toBe(2);
    expect(features[0].id).toBe('sensor1');
    expect(features[0].geometry.type).toBe('Point');
    expect(features[0].geometry.coordinates).toStrictEqual([-77.94, 35.35]);
    expect(features[1].geometry.coordinates).toStrictEqual([-77.32, 35.64]);
  });
});
