// Given an array of markers, generate a point geojson
export default function genBlueForceGeoJSON(items) {
  const features = items.map((BlueForceDevice) => ({
    type: 'Feature',
    id: BlueForceDevice.sensorName,
    properties: {
      displayName: BlueForceDevice.sensorName,
    },
    geometry: {
      type: 'Point',
      coordinates: [BlueForceDevice.longitude, BlueForceDevice.latitude],
    },
  }));
  return {
    type: 'FeatureCollection',
    features,
  };
}
