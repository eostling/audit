import formatDateShort from '../FormatDateShort/formatDateShort';
import formatTime24 from '../Format24/formatTime24';

// TODO: Figure out what "mapped" means
// Given an alarm, generate metadata string
export default function genAlarmMetadata(
  {
    alarmTime,
    facilityID = 'UNK',
    floor = 'UNK',
    priority = 'UNK',
    zone = 'UNK',
  },
  mapped,
  sensorType
) {
  const date = alarmTime ? formatDateShort(alarmTime) : 'DATE UNK';
  const time = alarmTime ? formatTime24(alarmTime) : 'TIME UNK';

  const priorityLabel = `Priority ${priority}`;
  const timeLabel = `@ ${date} ${time}`;
  const zoneLabel = `Zone ${zone}`;
  const facilityLabel = `Facility ${facilityID} (Floor ${floor})`;
  const sensorTypeLabel = `Sensor Type: ${sensorType}`;

  return `${
    mapped
      ? ''
      : `${priorityLabel} ${timeLabel}\n${zoneLabel} - ${facilityLabel}\n${sensorTypeLabel}`
  }`;
}
