// Key/value map of words that don't simply add "s" to the plural variant
const pluralMap = { facility: 'facilities' };

// Given a subject and count, return the appropriate word variant
const pluralize = (input) => (count) => {
  const subject = input.toLowerCase();
  if (count === 1) return subject;
  if (Object.keys(pluralMap).includes(subject)) {
    return pluralMap[subject];
  }
  return `${subject}s`;
};

export default pluralize;
