import React from 'react';

// Pages
import LoginPage from './Login/LoginPage';
// import AlarmsPage from './components/Layout/Alarms/AlarmPage/AlarmsPage';
// import ChatPage from './components/Layout/ChatPage/ChatPage';
// import FacilitiesPage from './components/Layout/Facility/FacilitiesPage';
// import SensorsPage from './components/Layout/Sensors/SensorsPage';
// import AuditPage from './components/Layout/Audit/AuditPage/AuditPage';
// import OrvillePage from './components/Layout/Orville/OrvillePage';

// Icons
import {
  AlarmIcon,
  ChatIcon,
  FacilitiesIcon,
  SensorsIcon,
  FileClockIcon,
  OrvilleIcon,
} from './Icons';

/*
 * Route configuration is used by the Router and Navbar components.
 * Routes are one of three types:
 *
 * login - no authN/authZ required
 * user - authN/authZ required, displayed at top of navbar
 * admin - authN/authZ required, displayed at bottom of navbar
 */

const routes = {
  login: {
    path: '/login',
    component: <LoginPage />,
  },
  // default: {
  //   label: 'alarm map',
  //   path: '/',
  //   component: <AlarmsPage />,
  //   icon: <AlarmIcon />,
  // },
  // user: [
  //   {
  //     label: 'chat',
  //     path: '/chat',
  //     component: <ChatPage />,
  //     icon: <ChatIcon />,
  //     permission: 'CHAT',
  //   },
  //   {
  //     label: 'facility management',
  //     path: '/facilities',
  //     component: <FacilitiesPage />,
  //     icon: <FacilitiesIcon />,
  //     permission: 'FACILITY_MANAGEMENT',
  //   },
  //   {
  //     label: 'sensor management',
  //     path: '/sensors',
  //     component: <SensorsPage />,
  //     icon: <SensorsIcon />,
  //     permission: 'SENSOR_MANAGEMENT',
  //   },
  // ],
  // admin: [
  //   {
  //     label: 'audit report',
  //     path: '/auditReport',
  //     component: <AuditPage />,
  //     icon: <FileClockIcon />,
  //     permission: 'AUDIT',
  //   },
  //   {
  //     label: 'orville tools',
  //     path: '/orville',
  //     component: <OrvillePage />,
  //     icon: <OrvilleIcon />,
  //     permission: 'DEVELOPMENT',
  //   },
  // ],
};

export default routes;
