import React from 'react';

import { Flex } from '@chakra-ui/react';
import { ErrorBoundary } from 'react-error-boundary';
import PropTypes from 'prop-types';
import Appbar from './Appbar/Appbar';
import Navbar from './Nav/Navbar';
export default function Layout({ children }) {
    const sizeRef = React.useRef();

    return (
        <Flex
            flexDirection='column'
            height='100vh'
            width='100vw'
            backgroundColor='gray.900'
        >
            <Appbar />
            <Flex flexGrow={1} position='relative'>
                <Navbar />
                <Flex
                    ref={sizeRef}
                    flexGrow={1}
                    minHeight='100%'
                    maxHeight='1em'
                    overflowX='auto'
                    overflowY='hidden'
                >
                    <ErrorBoundary FallbackComponent={''}>
                        {children}
                    </ErrorBoundary>
                </Flex>
            </Flex>
        </Flex>
    );
}

Layout.defaultProps = { children: {} };

Layout.propTypes = {
    children: PropTypes.instanceOf(Object),
};
