import { render, screen, fireEvent } from '@testing-library/react';
import DatePicker from './DatePicker';

describe('<DatePicker />', () => {
  const handleChange = jest.fn();
  const setup = () => {
    render(<DatePicker handleChange={handleChange} />);
  };

  test('Should render without error', () => {
    setup();
    fireEvent.click(screen.getByTestId('date'));
    expect(screen.getByTestId('date')).toBeDefined();
  });
});
