import { Input, Box } from '@chakra-ui/react';
import React from 'react';
import PropTypes from 'prop-types';

const DatePicker = ({ handleChange }) => {
  return (
    <Box mr={2}>
      <Input
        bg='bgColor'
        type='date'
        onChange={handleChange}
        max={new Date().toISOString().split('T')[0]}
        data-testid='date'
      />
    </Box>
  );
};

DatePicker.defaultProps = {
  handleChange: () => undefined,
};

DatePicker.propTypes = {
  handleChange: PropTypes.func,
};

export default DatePicker;
