import {
  screen,
  fireEvent,
  render,
} from '../../../util/Testing/CustomRender/TestingUtils';
import TimePicker from './TimePicker';

describe('<TimePicker />', () => {
  const handleChange = jest.fn();
  const setup = () => {
    render(<TimePicker handleChange={handleChange} />);
  };

  test('Should render without error', () => {
    setup();
    fireEvent.click(screen.getByTestId('handle'));
    expect(screen.getByText('00:00')).toBeDefined();
  });
});
