import React, { useEffect, useState } from 'react';
import { Box, Select } from '@chakra-ui/react';
import PropTypes from 'prop-types';

const TimePicker = ({ handleChange }) => {
  const [times, setTimes] = useState([]);

  useEffect(() => {
    const interval = 30;
    const updatedTimes = [];
    let firstTime = 0;

    for (let i = 0; firstTime < 24 * 60; i += 1) {
      const hours = Math.floor(firstTime / 60);
      const min = firstTime % 60;
      updatedTimes[i] = `${`0${hours}`.slice(-2)}:${`0${min}`.slice(-2)}`;
      firstTime += interval;
    }
    setTimes(updatedTimes);
  }, []);

  return (
    <Box>
      <Select bg='bgColor' onChange={handleChange} data-testid='handle'>
        {times.map((time) => (
          <option key={time} value={time}>
            {time}
          </option>
        ))}
      </Select>
    </Box>
  );
};

TimePicker.defaultProps = {
  handleChange: () => undefined,
};

TimePicker.propTypes = {
  handleChange: PropTypes.func,
};

export default TimePicker;
