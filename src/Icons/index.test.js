import { genIcon } from '.';

describe('Generate icon util', () => {
  test('generates an icon', () => {
    const { $$typeof, render, displayName } = genIcon(
      'testIcon',
      'M 100 100 m -75 0 a 75 75 0 1 0 150 0 a 75 75 0 1 0 -150 0'
    );
    expect(displayName).toBe('testIcon');
    expect(render).not.toBeNull();
    expect($$typeof).not.toBeNull();
  });
});
