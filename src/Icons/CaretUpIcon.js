import { genIcon } from '.';

export const CaretUpIcon = genIcon('CaretUpIcon', 'M7,15L12,10L17,15H7Z');
