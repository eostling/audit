import React from 'react';
import { createIcon } from '@chakra-ui/react';

/*
 * All icon paths are pulled from Material Design Icons (https://materialdesignicons.com/)
 * All icon paths should come from there or be mashups of existing icon paths
 */

// Given a name and path, generate a Chakra UI icon with default params
export function genIcon(name, path) {
  return createIcon({
    displayName: name,
    viewBox: '0 0 24 24',
    path: <path fill='currentColor' d={path} />,
  });
}

// Dynamic icons
export * from './AlarmStateIcon';

// Static icons
export * from './AcceptIcon';
export * from './AcceptAllIcon';
export * from './AccountIcon';
export * from './AirForceIcon';
export * from './AlarmIcon';
export * from './AlarmEditIcon';
export * from './AlarmAccessIcon';
export * from './AlarmAcknowledgedIcon';
export * from './AlarmAcknowledgedUnknownIcon';
export * from './AlarmAlarmIcon';
export * from './AlarmSecureIcon';
export * from './AlarmUnknownIcon';
export * from './HomeIcon';
export * from './CaretDownIcon';
export * from './CaretLeftIcon';
export * from './CaretRightIcon';
export * from './CaretUpIcon';
export * from './ChatIcon';
export * from './ChevronDownIcon';
export * from './ChevronLeftIcon';
export * from './ChevronRightIcon';
export * from './ChevronUpIcon';
export * from './CloseIcon';
export * from './CloseCircleIcon';
export * from './CircleIcon';
export * from './CompassArrowIcon';
export * from './DeleteIcon';
export * from './EditIcon';
export * from './ExportIcon';
export * from './FacilitiesIcon';
export * from './FileClockIcon';
export * from './FilterIcon';
export * from './GearIcon';
export * from './HideIcon';
export * from './ImportIcon';
export * from './JsonIcon';
export * from './LockIcon';
export * from './LogoutIcon';
export * from './MapIcon';
export * from './MinusIcon';
export * from './NominationIcon';
export * from './OrvilleIcon';
export * from './PhoneIcon';
export * from './PlusIcon';
export * from './ReportIcon';
export * from './RocketIcon';
export * from './SatelliteIcon';
export * from './SearchIcon';
export * from './SensorsIcon';
export * from './ShowIcon';
export * from './StarIcon';
export * from './StarOutlineIcon';
export * from './TargetIcon';
export * from './ZoomInIcon';
export * from './ZoomOutIcon';
