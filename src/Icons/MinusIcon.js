import { genIcon } from '.';

export const MinusIcon = genIcon('MinusIcon', 'M19,13H5V11H19V13Z');
