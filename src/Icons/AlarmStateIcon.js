import React from 'react';
import PropTypes from 'prop-types';
import {
  AlarmAccessIcon,
  AlarmAcknowledgedIcon,
  AlarmAcknowledgedUnknownIcon,
  AlarmAlarmIcon,
  AlarmSecureIcon,
  AlarmUnknownIcon,
} from '.';

export const AlarmStateIcon = ({ state, color, ariaHidden, focusable }) => {
  if (state === 'ACCESS')
    return (
      <AlarmAccessIcon
        color={color}
        aria-hidden={ariaHidden}
        focusable={focusable}
      />
    );
  if (state === 'ACKNOWLEDGED')
    return (
      <AlarmAcknowledgedIcon
        color={color}
        aria-hidden={ariaHidden}
        focusable={focusable}
      />
    );
  if (state === 'ACK_UNKNOWN')
    return (
      <AlarmAcknowledgedUnknownIcon
        color={color}
        aria-hidden={ariaHidden}
        focusable={focusable}
      />
    );
  if (state === 'ALARM')
    return (
      <AlarmAlarmIcon
        color={color}
        aria-hidden={ariaHidden}
        focusable={focusable}
      />
    );
  if (state === 'SECURE')
    return (
      <AlarmSecureIcon
        color={color}
        aria-hidden={ariaHidden}
        focusable={focusable}
      />
    );

  return (
    <AlarmUnknownIcon
      color={color}
      aria-hidden={ariaHidden}
      focusable={focusable}
    />
  );
};

AlarmStateIcon.defaultProps = {
  state: null,
  color: '',
  ariaHidden: null,
  focusable: false,
};

AlarmStateIcon.propTypes = {
  state: PropTypes.string,
  color: PropTypes.string,
  ariaHidden: PropTypes.bool,
  focusable: PropTypes.bool,
};
