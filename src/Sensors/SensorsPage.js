import React from 'react';
import { useProvider, useCreateStore } from 'mobx-store-provider';
import SensorTypes, {
  MakeLookup,
  initialMakeModel,
  makeModelStoreId,
  initialSensorTypes,
  sensorTypesStoreId,
} from '../../../models/SensorTypes/SensorTypes';
import {
  Facilities,
  initialFacilities,
  facilitiesStoreId,
  Zones,
  initialZones,
  zonesStoreId,
} from '../../../models/Facilities/Facilities';

// Components
import DefaultPageLayout from '../DefaultPageLayout/DefaultPageLayout';
import SensorsList from './SensorList/SensorsList';

export default function SensorPage() {
  // TODO: Verify all these providers need to be separated
  const FacilitiesProvider = useProvider(facilitiesStoreId);
  const facilities = useCreateStore(() => Facilities.create(initialFacilities));
  const SensorTypesProvider = useProvider(sensorTypesStoreId);
  const sensorTypes = useCreateStore(() =>
    SensorTypes.create(initialSensorTypes)
  );
  const MakeModelProvider = useProvider(makeModelStoreId);
  const makeModels = useCreateStore(() => MakeLookup.create(initialMakeModel));
  const ZonesProvider = useProvider(zonesStoreId);
  const zones = useCreateStore(() => Zones.create(initialZones));

  React.useLayoutEffect(() => {
    facilities.load();
  }, [facilities]);

  return (
    <DefaultPageLayout title='Sensor Management'>
      <FacilitiesProvider value={facilities}>
        <ZonesProvider value={zones}>
          <SensorTypesProvider value={sensorTypes}>
            <MakeModelProvider value={makeModels}>
              <SensorsList />
            </MakeModelProvider>
          </SensorTypesProvider>
        </ZonesProvider>
      </FacilitiesProvider>
    </DefaultPageLayout>
  );
}
