import React from 'react';
import PropTypes from 'prop-types';
import { useStore } from 'mobx-store-provider';
import {
  Box,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  ModalHeader,
  Input,
  Button,
  UnorderedList,
  ListItem,
  Text,
  Flex,
  Stack,
} from '@chakra-ui/react';

import { sensorsStoreId } from '../../../../../models/Sensors/Sensors';
import { deleteSensor } from '../../../../../services/Sensors/sensors';
import useToast from '../../../../../hooks/Toast/toast';
import { titleCase, pluralize } from '../../../../../util';

const CONFIRM_TEXT = 'DELETE';

const successDelete = (sensorsToDelete) => {
  const pluralizeSensorIfNeeded =
    sensorsToDelete.length >= 2 ? 'Sensors' : 'Sensor';
  return {
    title: 'Deletion Successful',
    description: `${sensorsToDelete.length} ${pluralizeSensorIfNeeded} Deleted`,
    status: 'success',
    duration: 9000,
    isClosable: true,
  };
};

const partialDeletion = (removedSensors, sensorsToDelete) => {
  return {
    title: 'Partial Deletion Detected.',
    description: `${removedSensors.length} of ${sensorsToDelete.length} Sensors Deleted`,
    status: 'warning',
    duration: 9000,
    isClosable: true,
  };
};

const failedDelete = (err) => {
  return {
    title: 'Deletion Failed',
    description: err.message.substring(0, 10),
    status: 'error',
    duration: 9000,
    isClosable: true,
  };
};

export default function DeleteSensorsConfirmationModal({ isOpen, onClose }) {
  const [confirmation, setConfirmation] = React.useState('');
  const initialRef = React.useRef();

  const sensors = useStore(sensorsStoreId);
  const toast = useToast();
  const showToast = (props) => {
    toast(props);
    return props;
  };

  const handleConfirmation = (e) => {
    setConfirmation(e.target.value);
  };

  const validateInput = () => {
    return confirmation !== CONFIRM_TEXT;
  };

  const close = () => {
    onClose();
    setConfirmation('');
  };

  const clearSensors = (val) => {
    sensors.update(val);
  };

  const deleteSensors = async (records) => {
    // This can be a list of records or a single record
    const toDelete = records.map((record) => {
      return record.sensorName;
    });
    try {
      const result = await deleteSensor(toDelete);
      if (result.status === 201) {
        sensors.delete(toDelete);
        setConfirmation('');
        clearSensors([]);
        return showToast(successDelete(toDelete));
      }

      if (result.status === 206) {
        const removedSensors = result.data
          .filter((rec) => {
            return rec.status === 201;
          })
          .map((rec) => {
            return rec.sensorName;
          });

        if (removedSensors.length === 0)
          return showToast({
            title: 'Deletion Failed',
            description:
              'An errors has occurred while trying to delete the sensors',
            status: 'error',
            duration: 9000,
            isClosable: true,
          });

        if (removedSensors.length) {
          sensors.delete(removedSensors);
          setConfirmation('');
          clearSensors([]);
          onClose();
          return showToast(partialDeletion(removedSensors, toDelete));
        }
      }
      return showToast({
        title: 'Deletion Failed',
        description: `Failed to delete ${toDelete}`,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    } catch (err) {
      return showToast(failedDelete(err));
    }
  };

  const handleConfirmDelete = (e) => {
    e.preventDefault();
    deleteSensors(sensors.selected);
    return close();
  };

  const sensorLabel = pluralize('sensor')(sensors.selectedCount);

  return (
    <Modal
      isOpen={isOpen}
      onClose={close}
      size='xl'
      initialFocusRef={initialRef}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{`Delete ${sensors.selectedCount} ${titleCase(
          sensorLabel
        )}`}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack spacing={4}>
            <Text>
              {`Are you sure you want to delete the following ${sensors.selectedCount} ${sensorLabel}?`}
            </Text>
            <Box>
              <UnorderedList>
                {sensors.selected.map((sensor) => (
                  <ListItem key={sensor}>{sensor.sensorName}</ListItem>
                ))}
              </UnorderedList>
            </Box>
            <Text as='strong'>You cannot undo this action.</Text>
            <form onSubmit={handleConfirmDelete}>
              <Stack spacing={4}>
                <Input
                  ref={initialRef}
                  placeholder={`To confirm this action, type ${CONFIRM_TEXT}`}
                  value={confirmation}
                  onChange={handleConfirmation}
                  onPaste={(e) => e.preventDefault()}
                  data-testid='confirm'
                />
                <Flex justifyContent='flex-end'>
                  <Button onClick={close} mr={2}>
                    Cancel
                  </Button>
                  <Button
                    type='submit'
                    isDisabled={validateInput()}
                    colorScheme='red'
                    data-testid='delete'
                  >
                    Confirm Delete
                  </Button>
                </Flex>
              </Stack>
            </form>
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}

DeleteSensorsConfirmationModal.defaultProps = {
  isOpen: false,
  onClose: () => undefined,
};

DeleteSensorsConfirmationModal.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};
