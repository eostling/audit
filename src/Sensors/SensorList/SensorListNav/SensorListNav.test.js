import { render, screen } from '@testing-library/react';
import SensorsListNav from './SensorsListNav';

jest.mock('../../../../ListNav/ListNav');

describe('<SensorsListNav />', () => {
  const sensors = {
    count: 0,
    pages: 10,
    total: 100,
  };
  const setup = () => {
    render(<SensorsListNav sensors={sensors} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('MOCK PREV PAGE')).toBeDefined();
  });
});
