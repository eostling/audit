import React from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';

// Components
import ListNav from '../../../../ListNav/ListNav';

function SensorsListNav({ sensorStore }) {
  const { currentPageCount, pages, total } = sensorStore;
  return (
    <ListNav
      currentPage={pages.currentPage}
      currentSize={pages.size}
      disablePrev={!pages.hasPrev}
      disableNext={!pages.hasNext}
      handlePrev={pages.prev}
      handleNext={pages.next}
      handleChangeSize={pages.changeSize}
      itemCount={currentPageCount}
      pageCount={pages.pageCount}
      total={total}
      label='sensors'
    />
  );
}

SensorsListNav.defaultProps = {
  sensorStore: {
    count: 0,
    pages: {
      changeSize: 0,
    },
    total: 0,
  },
};

SensorsListNav.propTypes = {
  sensorStore: PropTypes.instanceOf(Object),
};

export default observer(SensorsListNav);
