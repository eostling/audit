import React, { useState } from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';
import {
  Box,
  Button,
  Flex,
  HStack,
  Stack,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import { pluralize } from '../../../../util';

// Components
import { DeleteIcon, EditIcon, PlusIcon } from '../../../Icons';
import ImportButton from '../../../Import/ImportButton';
import ExportButton from '../../../Export/ExportButton';
import DeleteSensorsConfirmationModal from './DeleteSensorConfirmationModal/DeleteSensorsConfirmationModal';
import SensorsListNav from './SensorListNav/SensorsListNav';
import Table from '../../../Table/Table';
import EditSensorDialog from './EditSensorDialog/EditSensorDialog';
import IconButtonWithTooltip from '../../../IconButtonWithTooltip/IconButtonWithTooltip';
import { sensorsStoreId } from '../../../../models/Sensors/Sensors';

function SensorList() {
  const sensorStore = useStore(sensorsStoreId);
  const editModal = useDisclosure();
  const deleteModal = useDisclosure();

  const [sensor, setSensor] = useState();
  const [isEditModal, setIsEditModal] = useState(false);

  const handleEditRow = (item) => {
    setSensor(item);
    setIsEditModal(true);
    editModal.onOpen();
  };

  const handleTypeModal = (val) => {
    setIsEditModal(val);
  };

  const handleAddSensor = () => {
    editModal.onOpen();
  };

  const columns = [
    {
      label: 'Name',
      accessor: 'sensorName',
      sortable: true,
    },
    {
      label: 'Type',
      accessor: 'sensorType',
      sortable: true,
      width: '20%',
    },
    {
      label: 'Zone',
      accessor: 'zone',
      alignment: 'center',
      sortable: true,
      width: '6em',
    },
    {
      label: 'Facility No.',
      accessor: 'facilityID',
      sortable: true,
      width: '12em',
    },
    {
      label: 'Floor',
      accessor: 'floor',
      alignment: 'center',
      sortable: true,
      width: '6em',
    },
  ];

  const { selectedCount, total } = sensorStore;
  const sensorLabel = pluralize('sensor');
  const noneSelectedLabel = `${total > 0 ? total : 'No'} ${sensorLabel(total)}`;
  const selectedLabel = `${selectedCount} ${sensorLabel(
    selectedCount
  )} selected`;

  return (
    <Stack height='100%' overflow='hidden'>
      <HStack spacing={2} justifyContent='space-between'>
        <Button
          colorScheme='green'
          onClick={handleAddSensor}
          leftIcon={<PlusIcon />}
          aria-label='Add new sensor'
        >
          New Sensor
        </Button>
        <HStack spacing={2}>
          <ImportButton type='sensors' />
          <ExportButton type='sensors' />
        </HStack>
      </HStack>
      <Flex justifyContent='space-between'>
        <HStack spacing={2}>
          {sensorStore.selectedCount === 0 && <Text>{noneSelectedLabel}</Text>}
          {sensorStore.selectedCount !== 0 && (
            <>
              <Text mr={2}>{selectedLabel}</Text>
              <IconButtonWithTooltip
                disabled={sensorStore.selectedCount === 0}
                label='Delete Selected Sensors'
                placement='top'
                icon={<DeleteIcon />}
                onClick={deleteModal.onOpen}
              />
              <DeleteSensorsConfirmationModal
                isOpen={deleteModal.isOpen}
                onClose={deleteModal.onClose}
              />
            </>
          )}
        </HStack>
        <Flex>
          <SensorsListNav sensorStore={sensorStore} />
        </Flex>
      </Flex>
      <Box overflow='auto'>
        <Table
          select
          items={sensorStore.currentPageItems}
          columns={columns}
          sorting={sensorStore.sort}
          idAccessor='sensorName'
          onSelectAll={sensorStore.selectAll}
          onDeselectAll={sensorStore.deselectAll}
          isAllSelected={sensorStore.isAllSelected}
          isIntermediateSelected={sensorStore.isIntermediateSelected}
          rowActions={(rowData) => (
            <IconButtonWithTooltip
              size='sm'
              icon={<EditIcon />}
              label='Edit Sensor'
              placement='left'
              onClick={() => handleEditRow(rowData)}
            />
          )}
        />
      </Box>
      <EditSensorDialog
        isOpen={editModal.isOpen}
        onClose={editModal.onClose}
        sensor={sensor}
        sensorStore={sensorStore}
        edit={isEditModal}
        handleType={handleTypeModal}
      />
    </Stack>
  );
}
export default observer(SensorList);
