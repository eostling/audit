/* istanbul ignore file */

import React, { useRef, useState, useEffect } from 'react';
import { useStore } from 'mobx-store-provider';
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Select,
  Stack,
  Textarea,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import {
  facilitiesStoreId,
  zonesStoreId,
} from '../../../../../models/Facilities/Facilities';
import {
  makeModelStoreId,
  sensorTypesStoreId,
} from '../../../../../models/SensorTypes/SensorTypes';
import {
  updateSensor,
  updateSensors,
} from '../../../../../services/Sensors/sensors';

import useToast from '../../../../../hooks/Toast/toast';

export default function EditSensorDialog({
  isOpen,
  onClose,
  sensor,
  sensorStore,
  edit,
  handleType,
}) {
  const [name, setName] = useState('');
  const [make, setMake] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);
  const [assetNo, setAssetNo] = useState('');
  const [zone, setZone] = useState('');
  const [facility, setFacility] = useState({});
  const [floor, setFloor] = useState(1);
  const [lat, setLat] = useState('');
  const [lon, setLon] = useState('');
  const [notes, setNotes] = useState('');
  const facilities = useStore(facilitiesStoreId);
  const makeModel = useStore(makeModelStoreId);
  const zones = useStore(zonesStoreId);
  const sensorTypes = useStore(sensorTypesStoreId);

  useEffect(() => {
    if (edit) {
      const fac = facilities.byID(sensor.facilityID);
      const sensorType = sensorTypes.list.filter(
        (st) => st.id === sensor.sensorType
      )[0];
      setName(sensor.sensorName);
      setAssetNo(sensor.assetNumber);
      setMake(sensorType.make);
      setModels(makeModel.models(sensorType.make));
      setModel(sensorType.model);
      setLat(sensor.lat);
      setLon(sensor.lon);
      setNotes(sensor.notes);
      setZone(sensor.zone);
      setFacility(fac);
      setFloor(sensor.floor);
    }
  }, [sensor, edit, facilities, makeModel, sensorTypes]);

  const toast = useToast();
  // toast() doesn't return useful information for unit testing, so we wrap in this function
  const showToast = (props) => {
    toast(props);
    return props;
  };

  const handleNameChange = (evt) => setName(evt.target.value.substring(0, 255));
  const handleMakeChange = (evt) => {
    const makeChanges = evt.target.value;
    setMake(makeChanges);
    setModel('');
    if (makeChanges !== '') {
      setModels(makeModel.models(makeChanges));
    } else {
      // disable model selector until make is selected
      setModels([]);
    }
  };
  const handleModelChange = (evt) => setModel(evt.target.value);
  const handleAssetNoChange = (evt) =>
    setAssetNo(evt.target.value.substring(0, 255));
  const handleFacilityChange = (evt) => {
    const fac = evt.target.value;
    const facilityUpdate = fac ? facilities.byID(fac) : {};
    setFacility(facilityUpdate);
    setFloor(1);
    if (fac) {
      setLat(facilityUpdate.lat.toString());
      setLon(facilityUpdate.lon.toString());
    }
    if (facilityUpdate.zoneID !== undefined && zone !== facilityUpdate.zoneID) {
      setZone(facilityUpdate.zoneID);
    }
  };
  const handleZoneChange = (evt) => {
    const zoneUpdate = evt.target.value;
    setFacility({});
    setFloor(1);
    setZone(zoneUpdate);
  };
  const handleFloorChange = (evt) => setFloor(parseInt(evt.target.value));
  const handleLatChange = (evt) => setLat(evt.target.value);
  const handleLonChange = (evt) => setLon(evt.target.value);
  const handleNotesChange = (evt) => setNotes(evt.target.value);
  const initialFocus = useRef();

  const isFloat = (s) =>
    parseFloat(s).toString() === s || typeof s === 'number';
  const validLatLon = (latitude, longitude) => {
    return (
      latitude &&
      longitude &&
      isFloat(latitude) &&
      isFloat(longitude) &&
      parseFloat(latitude) >= -90 &&
      parseFloat(latitude) <= 90 &&
      parseFloat(longitude) >= -180 &&
      parseFloat(longitude) <= 180
    );
  };
  const noWhitespace = (s) => s.trim().length !== 0;
  const formCompleted = () => {
    return (
      noWhitespace(name) &&
      make &&
      model &&
      noWhitespace(assetNo) &&
      zone &&
      facility.facilityID &&
      floor &&
      validLatLon(lat, lon)
    );
  };

  const reset = () => {
    setName('');
    setMake('');
    setModel('');
    setModels([]);
    setAssetNo('');
    setZone('');
    setFacility({});
    setFloor(1);
    setLat('');
    setLon('');
    setNotes('');
  };

  const close = () => {
    reset();
    handleType(false);
    onClose();
  };

  const onSubmit = async () => {
    const sensorType = makeModel.type(make, model);
    const updatedSensor = {
      sensorName: name.trim(),
      sensorType: sensorType.id,
      lat: parseFloat(lat),
      lon: parseFloat(lon),
      zone,
      facilityID: facility.facilityID,
      floor,
      assetNumber: assetNo.trim(),
      notes,
    };
    try {
      const result = edit
        ? await updateSensor([updatedSensor])
        : await updateSensors([updatedSensor]);
      if (result.error) {
        return showToast({
          title: 'Update Failed',
          description: result.error,
          status: 'error',
        });
      }
      sensorStore.update([updatedSensor]);
      close();
      return showToast({
        title: `Sensor ${edit ? 'Edited' : 'Added'}`,
        description: `${edit ? 'Edited' : 'Added'} sensor`,
      });
    } catch (err) {
      if (err.response.status === 409) {
        return showToast({
          title: 'Update Failed',
          description: 'Failed to update sensor: Active Alarms',
          status: 'error',
        });
      }
      return showToast({
        title: 'Update Failed',
        description: 'Failed to add sensor',
        status: 'error',
      });
    }
  };

  return (
    <>
      <Modal
        size='lg'
        initialFocusRef={initialFocus}
        isOpen={isOpen}
        onClose={close}
        closeOnOverlayClick={false}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color='white'>
            {edit ? 'Edit' : 'Add'} Sensor
          </ModalHeader>
          <ModalCloseButton color='white' onClick={close} />
          <ModalBody>
            <Stack>
              <FormControl isRequired>
                <FormLabel htmlFor='name'>Name</FormLabel>
                <Input
                  id='name'
                  placeholder='Sensor Name'
                  value={name}
                  isDisabled={edit}
                  onChange={handleNameChange}
                  ref={initialFocus}
                />
              </FormControl>
              <Flex w='100%' justify='space-between'>
                <FormControl isRequired w='47.5%'>
                  <FormLabel htmlFor='name'>Make</FormLabel>
                  <Select
                    id='make'
                    placeholder='Select Make'
                    value={make}
                    onChange={handleMakeChange}
                  >
                    {makeModel.makes.map((selectedMake) => (
                      <option key={selectedMake} value={selectedMake}>
                        {selectedMake}
                      </option>
                    ))}
                  </Select>
                </FormControl>
                <FormControl
                  isRequired
                  isDisabled={models.length === 0}
                  w='47.5%'
                >
                  <FormLabel htmlFor='name'>Model</FormLabel>
                  <Select
                    id='model'
                    placeholder='Select Model'
                    value={model}
                    onChange={handleModelChange}
                  >
                    {models.map((modelUpdate) => (
                      <option key={modelUpdate} value={modelUpdate}>
                        {modelUpdate}
                      </option>
                    ))}
                  </Select>
                </FormControl>
              </Flex>
              <FormControl isRequired>
                <FormLabel htmlFor='assetNo'>Asset Number</FormLabel>
                <Input
                  id='assetNo'
                  placeholder='Asset Number'
                  value={assetNo}
                  onChange={handleAssetNoChange}
                />
              </FormControl>
              <Flex w='100%' justify='space-between'>
                <FormControl isRequired w='30%'>
                  <FormLabel htmlFor='zone'>Zone</FormLabel>
                  <Select
                    id='zone'
                    placeholder=' '
                    value={zone}
                    onChange={handleZoneChange}
                  >
                    {zones.all.map((zoneUpdate) => (
                      <option key={zoneUpdate.zoneID} value={zoneUpdate.zoneID}>
                        {zoneUpdate.displayName}
                      </option>
                    ))}
                  </Select>
                </FormControl>
                <FormControl isRequired w='50%'>
                  <FormLabel htmlFor='facility'>Facility</FormLabel>
                  <Select
                    id='facility'
                    placeholder='Select Facility'
                    value={facility.facilityID}
                    onChange={handleFacilityChange}
                  >
                    {facilities.inZone(zone).map((fac) => (
                      <option key={fac.facilityID} value={fac.facilityID}>
                        {`${fac.facilityID} ${fac.displayName}`}
                      </option>
                    ))}
                  </Select>
                </FormControl>
                <FormControl isRequired isDisabled={!facility.floors} w='15%'>
                  <FormLabel htmlFor='floor'>Floor</FormLabel>
                  <Select id='floor' value={floor} onChange={handleFloorChange}>
                    {facility.floors &&
                      facility.floors.map((fl) => (
                        <option key={fl} value={fl}>
                          {fl}
                        </option>
                      ))}
                  </Select>
                </FormControl>
              </Flex>
              <Flex w='100%' justify='space-between'>
                <FormControl
                  isRequired
                  isInvalid={lat && !isFloat(lat)}
                  w='47.5%'
                >
                  <FormLabel htmlFor='lat'>Latitude</FormLabel>
                  <Input id='lat' value={lat} onChange={handleLatChange} />
                </FormControl>
                <FormControl
                  isRequired
                  isInvalid={lon && !isFloat(lon)}
                  w='47.5%'
                >
                  <FormLabel htmlFor='lon'>Longitude</FormLabel>
                  <Input id='lon' value={lon} onChange={handleLonChange} />
                </FormControl>
              </Flex>
              <FormControl>
                <FormLabel htmlFor='notes'>Notes</FormLabel>
                <Textarea
                  id='notes'
                  value={notes}
                  onChange={handleNotesChange}
                />
              </FormControl>
            </Stack>
          </ModalBody>
          <ModalFooter>
            <Button variant='outline' mr={3} onClick={close}>
              Cancel
            </Button>
            <Button
              variant='solid'
              isDisabled={!formCompleted()}
              onClick={onSubmit}
            >
              Save
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

EditSensorDialog.defaultProps = {
  isOpen: false,
  onClose: () => undefined,
  sensor: {},
  sensorStore: {
    update: () => undefined,
  },
  edit: false,
  handleType: () => undefined,
};

EditSensorDialog.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  sensor: PropTypes.instanceOf(Object),
  sensorStore: PropTypes.instanceOf(Object),
  edit: PropTypes.bool,
  handleType: PropTypes.func,
};
