/* istanbul ignore next */
import React from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';

import { Link } from 'react-router-dom';
import { Button, Badge, Tooltip } from '@chakra-ui/react';
import PropTypes from 'prop-types';
// import { useSocket } from 'use-socketio';
import { alarmsStoreId } from '../../models/Alarms/Alarms';
import { titleCase } from '../../util';

const NavbarItem = ({ route }) => {
  const isActive = window.location.pathname === route.path;
  const navColor = isActive ? 'activeNav' : 'inherit';
  const alarms = useStore(alarmsStoreId);
  const [connectedToServer, setConnectedToServer] = React.useState(true);

  // We use forbidden magic to append custom/computed props
  // to the icon component that gets passed in
  const iconProps = {
    height: '1.6em',
    width: '1.6em',
    color: navColor,
  };

  // useSocket('disconnect', () => {
  //   setConnectedToServer(false);
  // });
  //
  // useSocket('connect', () => {
  //   setConnectedToServer(true);
  // });

  return (
    <Tooltip label={titleCase(route.label)} placement='right'>
      <Button
        display='flex'
        alignItems='center'
        justifyContent='center'
        key={`link-${route.label}`}
        as={Link}
        to={route.path}
        variant='ghost'
        size='2em'
        p={6}
        borderRadius={0}
        position='relative'
      >
        {route.path === '/' && alarms.active.length > 0 && connectedToServer && (
          <Badge
            position='absolute'
            borderRadius='50vh'
            bg='badgeColor'
            top={2}
            right={2}
            px={2}
            py={1}
          >
            {alarms.active.length}
          </Badge>
        )}

        {React.cloneElement(route.icon, iconProps)}
      </Button>
    </Tooltip>
  );
};

NavbarItem.defaultProps = {
  route: {
    label: '',
    icon: '',
    path: '/',
  },
};

NavbarItem.propTypes = {
  route: PropTypes.instanceOf(Object),
};

export default observer(NavbarItem);
