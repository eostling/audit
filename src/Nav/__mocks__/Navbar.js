import React from 'react';
import { Flex, Stack } from '@chakra-ui/react';

export default function Navbar() {
  return (
    <Stack
      as='nav'
      bgGradient='linear(to-b, gray.700, gray.900)'
      spacing={0}
      direction='column'
      justifyContent='space-between'
      width='100%'
      maxWidth='6em'
      overflow='hidden'
    >
      <Flex direction='column'>MOCKED USER</Flex>
      <Flex direction='column'>MOCKED ADMIN</Flex>
    </Stack>
  );
}
