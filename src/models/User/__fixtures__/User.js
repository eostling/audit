export const makeUserFixture = (props) => {
  const baseObject = {
    username: 'Brain',
    role: 'Admin',
    permissions: ['Admin'],
  };
  return { ...baseObject, ...props };
};
