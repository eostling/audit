import { applySnapshot, flow, types } from 'mobx-state-tree';
import {
  getLocalUser,
  loginUser,
  logoutUser,
  refreshUser,
} from '../../services/Auth/auth';

export const userStoreId = 'userStore'; // njsscan-ignore: node_username
export const initialUser = {};

const User = types
  .model({
    username: '',
    role: '',
    permissions: types.array(types.string),
  })
  .views((self) => ({
    get isLoggedIn() {
      return self.username !== '';
    },
    get isAdmin() {
      return self.role === 'admin';
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load() {
      const user = getLocalUser();
      if (user) {
        applySnapshot(self, user);
      }
    },
    login: flow(function* login(credentials) {
      try {
        const user = yield loginUser(credentials);
        if (user.username) {
          applySnapshot(self, user);
        }
        return user;
      } catch (e) {
        return e;
      }
    }),
    logout: flow(function* logout() {
      yield logoutUser();
      applySnapshot(self, {});
    }),
    refreshToken: flow(function* refreshToken() {
      const token = yield refreshUser();
      return token;
    }),
  }));

export default User;
