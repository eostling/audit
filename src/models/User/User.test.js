import mockAxios from 'axios';
import User from './User';

const testOperator = {
  username: 'elmyra',
  role: 'operator',
};

const testAdmin = {
  username: 'elmyra',
  role: 'admin',
};

test('can create a User instance', () => {
  const user = User.create(testOperator);
  expect(user).not.toBeNull();
});

test('is logged in', () => {
  const user = User.create(testOperator);
  expect(user.username).toBe('elmyra');
});

test('is operator', () => {
  const user = User.create(testOperator);
  expect(user.role).toBe('operator');
});

test('can login', async () => {
  const user = User.create();
  mockAxios.post.mockImplementationOnce(() =>
    Promise.resolve({ data: { user: testOperator } })
  );
  const result = await user.login();
  expect(result.username).toBe(user.username);
  expect(result.role).toBe(user.role);
  expect(user.isLoggedIn).toBe(true);
});

test('bad login', async () => {
  const user = User.create();
  mockAxios.post.mockImplementationOnce(() =>
    Promise.resolve({
      data: null,
    })
  );
  const result = await user.login();
  expect(result).toBeInstanceOf(Error);
});

test('is admin', async () => {
  const user = User.create();
  mockAxios.post.mockImplementationOnce(() =>
    Promise.resolve({ data: { user: testAdmin } })
  );
  await user.login();
  expect(user.isAdmin).toBe(true);
});

test('is logged out', async () => {
  const user = User.create(testOperator);
  await user.logout();
  expect(user.username).toBe('');
  expect(user.role).toBe('');
});

test('refresh user', async () => {
  const user = User.create(testOperator);
  mockAxios.get.mockImplementationOnce(() => Promise.resolve({ data: {} }));
  expect(user.refreshToken()).not.toBeNull();
});
