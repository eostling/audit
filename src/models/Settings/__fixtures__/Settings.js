export const makeSettingsFixture = (props) => {
  const baseObject = {
    envSettings: '1,',
    papiSettings: {},
    loaded: false,
    REACT_APP_PAPI_URL: 'process.env.REACT_APP_PAPI_URL',
    REACT_APP_MAPBOX_ACCESS_TOKEN: '',
    REACT_APP_MAPBOX_GLYPHS_URL:
      'mapbox://fonts/mapbox/{fontstack}/{range}.pbf',
    REACT_APP_MAPBOX_SPRITES_URL: 'mapbox://sprites/mapbox/satellite-v9',
    REACT_APP_MAP_SETTINGS: 'sjafb',
    REACT_APP_ALARM_INTERVAL: 5000,
    REACT_APP_SESSION_WARN_MS: 3600000,
    REACT_APP_SESSION_EXPIRE_MS: 6000,
  };
  return { ...baseObject, ...props };
};
