import { flow, types } from 'mobx-state-tree';
import { getSettings } from '../../services/Settings/settings';
import { getLocalUser } from '../../services/Auth/auth';
import getMapStyles from '../../config/mapStyles';
import { allMapSettings } from '../../config/map';

export const settingsStoreId = 'settingsStore';
export const initialSettings = {};

const intOrUndefined = (str) => {
  if (typeof str === 'undefined') {
    return undefined;
  }
  return parseInt(str);
};
/*
 * Settings are queried from PAPI, but can be overridden by local environment variables
 * Overridden settings are shown in the console as a warning.
 * The only settings which must be set in the environment are those required to pull the settings from PAPI:
 *   REACT_APP_PAPI_URL
 *   NODE_TLS_REJECT_UNAUTHORIZED
 */
const Settings = types
  .model('Settings', {
    // all env/papi settings are stored separately in the model to assist with debugging
    envSettings: types.frozen({
      REACT_APP_PAPI_URL: process.env.REACT_APP_PAPI_URL,
      REACT_APP_MAPBOX_ACCESS_TOKEN: process.env.REACT_APP_MAPBOX_ACCESS_TOKEN,
      REACT_APP_MAPBOX_GLYPHS_URL: process.env.REACT_APP_MAPBOX_GLYPHS_URL,
      REACT_APP_MAPBOX_SPRITES_URL: process.env.REACT_APP_MAPBOX_SPRITES_URL,
      REACT_APP_MAP_SETTINGS: process.env.REACT_APP_MAP_SETTINGS,
      REACT_APP_ALARM_INTERVAL: intOrUndefined(
        process.env.REACT_APP_ALARM_INTERVAL
      ),
      REACT_APP_ALARM_ESCALATION: intOrUndefined(
        process.env.REACT_APP_ALARM_ESCALATION
      ),
      REACT_APP_SESSION_WARN_MS: intOrUndefined(
        process.env.REACT_APP_SESSION_WARN_MS
      ),
      REACT_APP_SESSION_EXPIRE_MS: intOrUndefined(
        process.env.REACT_APP_SESSION_EXPIRE_MS
      ),
    }),
    papiSettings: types.frozen({}),
    loaded: false,
    REACT_APP_PAPI_URL: process.env.REACT_APP_PAPI_URL,
    REACT_APP_MAPBOX_ACCESS_TOKEN: '',
    REACT_APP_MAPBOX_GLYPHS_URL:
      'mapbox://fonts/mapbox/{fontstack}/{range}.pbf',
    REACT_APP_MAPBOX_SPRITES_URL: 'mapbox://sprites/mapbox/satellite-v9',
    REACT_APP_MAP_SETTINGS: 'sjafb',
    REACT_APP_ALARM_INTERVAL: 5000,
    REACT_APP_SESSION_WARN_MS: 3600000,
    REACT_APP_SESSION_EXPIRE_MS: 100000,
  })
  .views((self) => ({
    get mapStyles() {
      return getMapStyles(
        self.REACT_APP_MAPBOX_SPRITES_URL,
        self.REACT_APP_MAPBOX_GLYPHS_URL
      );
    },
    get mapSettings() {
      return allMapSettings[self.REACT_APP_MAP_SETTINGS];
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const settings = self;
      if (!settings.loaded && getLocalUser()) {
        const papi = yield getSettings();
        Object.keys(self.envSettings).forEach((key) => {
          if (Object.prototype.hasOwnProperty.call(self.envSettings, key)) {
            const envSetting = settings.envSettings[key];
            if (typeof envSetting !== 'undefined') {
              settings[key] = envSetting;
            } else {
              const papiSetting = papi[key];
              if (typeof papiSetting === 'undefined') {
                throw new Error(
                  `Setting is not set in environment/PAPI: ${key}`
                );
              } else {
                settings[key] = papiSetting;
              }
            }
          }
        });

        settings.papiSettings = papi;
        settings.loaded = true;
      }
    }),
  }));

export default Settings;
