import { types } from 'mobx-state-tree';

export const uiStoreId = 'uiStore';
export const initialUI = {
  alarmDrawer: {},
  blueForceLayerView: {},
  content: {},
};

// Drawer for alarm queue and nomination queue
const AlarmDrawer = types
  .model({
    isOpen: true,
  })
  .actions((self) => ({
    toggle() {
      const ptr = self;
      ptr.isOpen = !ptr.isOpen;
    },
  }));

// Variable for toggling blue Force layer view
const BlueForceLayerView = types
  .model({
    isEnabled: true,
  })
  .actions((self) => ({
    toggle() {
      const ptr = self;
      ptr.isEnabled = !ptr.isEnabled;
    },
  }));

const UI = types.model({
  alarmDrawer: AlarmDrawer,
  blueForceLayerView: BlueForceLayerView,
});

export default UI;
