export const makeUIFixture = (props) => {
  const baseObject = {
    blueForceLayerView: 'BlueForceLayerView',
  };
  return { ...baseObject, ...props };
};
