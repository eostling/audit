import { applySnapshot, flow, types } from 'mobx-state-tree';
import { genBlueForceGeoJSON } from '../../util';
import {
  getBlueForceLocation,
  getBlueForceLocations,
} from '../../services/BlueForceLocations/blueForceLocations';

export const blueForceStoreId = 'blueForceStore';
export const initialBlueForceDevices = { list: [] };

const staleThreshold = process.env.REACT_APP_THRESHOLD_MS || 300000;

const BlueForceLocation = types.model({
  sensorName: types.string,
  latitude: types.number,
  longitude: types.number,
  timestamp: types.number,
});

const BlueForceDevices = types
  .model({
    list: types.array(BlueForceLocation),
  })
  .views((self) => ({
    geojson() {
      return genBlueForceGeoJSON(
        self.list.filter(({ sensorName, longitude, latitude }) => ({
          sensorName,
          longitude,
          latitude,
        }))
      );
    },
  }))
  .actions((self) => ({
    afterCreate() {},
    load: flow(function* load() {
      const blueForceLocations = yield getBlueForceLocations();
      const nonStaleDefenders = self.getNonStaleDefenders(blueForceLocations);
      applySnapshot(self.list, nonStaleDefenders);
    }),
    getNonStaleDefenders(blueForces) {
      const currentTime = Date.now();
      const nonStale = blueForces.filter((defender) => {
        const gap = currentTime - defender.timestamp;
        const isStale = gap < staleThreshold;
        return isStale;
      });
      return nonStale;
    },
    markDefendersAsStale() {
      const nonStaleDefenders = self.getNonStaleDefenders(self.list);
      applySnapshot(self.list, nonStaleDefenders);
    },
    async update(locationUpdate) {
      const { sensorName, latitude, longitude } = locationUpdate;

      const blueForceDevice = self.list.filter(
        (def) => def.sensorName === sensorName
      );

      if (blueForceDevice.length === 1) {
        blueForceDevice[0].latitude = latitude;
        blueForceDevice[0].longitude = longitude;
      } else {
        if (blueForceDevice.length !== 0)
          throw new Error('Got multiple BlueForceDevice with the same UID!');
        const newBlueForceDevice = await getBlueForceLocation(sensorName);
        if (newBlueForceDevice) {
          applySnapshot(self.list, [...self.list, newBlueForceDevice]);
        }
      }
    },
  }));

export default BlueForceDevices;
