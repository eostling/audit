export const makeBlueForceDeviceFixture = (props) => {
  const baseObject = {
    sensorName: ' BaseEvent',
    latitude: 42,
    longiture: 42,
  };
  return { ...baseObject, ...props };
};
