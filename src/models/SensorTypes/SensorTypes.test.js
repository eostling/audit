import SensorTypes from './SensorTypes';

export const testSensorTypes = {
  sensorTypes: [
    {
      id: 'a1',
      make: 'a',
      model: '1',
      description: 'da1',
      type: 'ta1',
      events: ['e2', 'e1'],
    },
    {
      id: 'b2',
      make: 'b',
      model: '2',
      description: 'db2',
      type: 'tb2',
      events: [],
    },
  ],
  total: 8,
  pagingInfo: {
    numPages: 4,
  },
};
const mockSensorTypes = testSensorTypes;
jest.mock('../../services/SensorTypes/sensorTypes', () => ({
  getSensorTypes: async () => {
    return mockSensorTypes;
  },
}));

describe('SensorTypes', () => {
  let sensorTypes;
  beforeEach(() => {
    sensorTypes = SensorTypes.create({
      list: testSensorTypes.sensorTypes,
      pages: { pageCount: 4 },
      total: 8,
    });
  });
  test('load', async () => {
    const loadTestSensorTypes = SensorTypes.create({ list: [], pages: {} });
    await loadTestSensorTypes.load();
    expect(loadTestSensorTypes.count).toBe(2);
  });

  test('SensorTypes.all', () => {
    expect(sensorTypes.all.length).toBe(2);
  });
  test('SensorTypes.count', () => {
    expect(sensorTypes.count).toBe(2);
  });
  test('SensorType.numEvents', () => {
    expect(sensorTypes.all[0].numEvents).toBe(2);
  });
  test('SensorType.eventsString', () => {
    expect(sensorTypes.all[0].eventsString).toBe('e1\ne2');
  });
});
