import { applySnapshot, flow, types } from 'mobx-state-tree';
import {
  getSensorTypes,
  getMakeLookup,
} from '../../services/SensorTypes/sensorTypes';
import Pages from '../Pages/Pages';

export const sensorTypesStoreId = 'sensorTypesStore';
export const initialSensorTypes = { list: [], pages: {} };

const SensorType = types
  .model('SensorType', {
    id: types.string,
    make: types.string,
    model: types.string,
    type: types.string,
    description: types.maybeNull(types.string),
    events: types.array(types.string),
  })
  .views((self) => ({
    get numEvents() {
      return self.events.length;
    },
    get eventsString() {
      return self.events.slice().sort().join('\n');
    },
  }));

const SensorTypes = types
  .model('SensorTypes', {
    list: types.array(SensorType),
    pages: Pages,
    total: 0,
  })
  .views((self) => ({
    get all() {
      return self.list;
    },
    get count() {
      return self.list.length;
    },
    byID(sensorTypeId) {
      return self.list.filter(
        (sensorType) => sensorType.id === sensorTypeId
      )[0];
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const selfSensorTypes = self;
      const { sensorTypes, total, pagingInfo } = yield getSensorTypes({
        page: self.pages.currentPage,
        length: self.pages.size,
      });
      selfSensorTypes.total = total;
      selfSensorTypes.pages.pageCount = pagingInfo.numPages;
      applySnapshot(self.list, sensorTypes);
    }),
    update(newType) {
      // TODO: Figure out cleaner way to do this
      const types = self;
      const existingTypes = types.list.filter(
        (sensorType) => sensorType.id !== newType.id
      );
      let draftType = types.list.filter(
        (sensorType) => sensorType.id === newType.id
      )[0];
      if (draftType) {
        draftType = { ...draftType, ...newType };
      } else {
        draftType = newType;
        types.total += 1;
      }
      const newList = [...existingTypes, draftType];
      return applySnapshot(self.list, newList);
    },
  }));

export const makeModelStoreId = 'makeModelStore';
export const initialMakeModel = { makeLookup: {} };
export const MakeLookup = types
  .model('MakeLookup', {
    makeLookup: types.map(types.array(SensorType)),
  })
  .views((self) => ({
    get makes() {
      return Array.from(self.makeLookup.keys()).slice().sort();
    },
    models(make) {
      return self.makeLookup
        .get(make)
        .map((type) => type.model)
        .slice()
        .sort();
    },
    type(make, model) {
      return self.makeLookup
        .get(make)
        .filter((type) => type.model === model)[0];
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const lookup = yield getMakeLookup();
      applySnapshot(self.makeLookup, lookup);
    }),
  }));

export default SensorTypes;
