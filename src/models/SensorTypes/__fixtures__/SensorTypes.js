export const makeSensorTypesFixture = (props) => {
  const baseObject = {
    id: '1',
    make: 'Temp',
    model: 'Temp',
    type: 'Temp',
    description: 'Temp Description',
    events: {},
  };
  return { ...baseObject, ...props };
};
