import Alarms from './Alarms';

const testAlarms = [
  {
    alarmID: 'aaa',
    label: 'TESTSENSOR_PIR001-INTRUSION',
    sensorName: 'TESTSENSOR_PIR001',
    sensorType: null,
    facilityID: 'TESTFACILITY',
    floor: 1,
    zone: null,
    geohash: 'aaa',
    alarmType: 'INTRUSION',
    active: false,
    priority: 2,
    acknowledgementTimeout: false,
    state: 'SECURE',
    lat: 10.123,
    lon: 10.1234,
    remark: null,
    alarmTime: 1234567,
    additionalAlarmDetails: {},
    nextValidActions: {
      access: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Put into Access',
        },
        conditionsToMeetInOrderToDisplay: {},
        checkConditions() {
          return true; // no conditions
        },
      },
    },
  },
  {
    alarmID: 'bbb',
    label: 'TESTSENSOR_PIR001-TAMPER',
    sensorName: 'TESTSENSOR_PIR001',
    sensorType: null,
    facilityID: 'TESTFACILITY2',
    floor: 1,
    zone: null,
    geohash: 'aaa',
    alarmType: 'TAMPER',
    active: true,
    priority: 2,
    acknowledgementTimeout: true,
    state: 'ALARM',
    lat: 10.123,
    lon: 10.1234,
    remark: null,
    alarmTime: 1234567,
    additionalAlarmDetails: {},
    nextValidActions: {
      accknowledge: {
        displayProperties: {
          isDisabled: false,
          toolTipMessage: 'Acknowledge Alarm',
        },
        conditionsToMeetInOrderToDisplay: {},
        checkConditions() {
          return true; // no conditions
        },
      },
    },
  },
];

const mockAlarms = testAlarms;
jest.mock('../../services/Alarms/alarms', () => ({
  getAlarms: async () => {
    return mockAlarms;
  },
  updateMultiAlarmState: async (alarm, newState) => {
    const resp = [{ updatedAlarm: { state: newState } }];
    return resp;
  },
}));

describe('Alarms Model', () => {
  let alarms;
  beforeEach(() => {
    alarms = Alarms.create({ list: testAlarms });
  });
  test('load', async () => {
    const loadTestAlarms = Alarms.create({ list: [] });
    await loadTestAlarms.load();
    expect(loadTestAlarms.countAll).toBe(2);
  });

  test('can create an Alarms instance', () => {
    expect(alarms).not.toBeNull();
  });
  test('Alarms.all', () => {
    expect(alarms.all.length).toBe(2);
  });
  test('Alarms.countAll', () => {
    expect(alarms.countAll).toBe(2);
    expect(alarms.exist).toBe(true);
  });
  test('filterTimedOut', () => {
    const filtered = alarms.filterTimedOut();
    expect(filtered.length).toBe(1);
    expect(filtered[0].alarmID).toBe('bbb');
  });
  test('filterByFacilityID', () => {
    const filtered = alarms.filterByFacilityID('TESTFACILITY2');
    expect(filtered.length).toBe(1);
    expect(filtered[0].alarmID).toBe('bbb');
  });
  test('filterByState', () => {
    const filtered = alarms.filterByState('ALARM');
    expect(filtered.length).toBe(1);
    expect(filtered[0].alarmID).toBe('bbb');
  });
  test('filterByCoords no results', () => {
    const filtered = alarms.filterByCoords({ lat: 3, lon: 4 });
    expect(filtered.length).toBe(0);
  });
  test('filterByCoords', () => {
    const filtered = alarms.filterByCoords({ lat: 10.123, lon: 10.1234 });
    expect(filtered.length).toBe(2);
  });
  test('geojsonByState', () => {
    const geojson = alarms.geojsonByState('ALARM');
    expect(geojson).not.toBeNull();
    expect(geojson.features.length).toBe(1);
    expect(geojson.features[0].id).toBe('bbb');
  });
  test('hasALARM', () => {
    expect(alarms.hasALARM).toBe(true);
  });
  test('hasTimeout', () => {
    expect(alarms.hasTimeout).toBe(true);
  });
  test('Alarm.updateState', async () => {
    const alarm = alarms.filterByState('ALARM')[0];
    await alarm.updateState('SECURE');
    expect(alarms.filterByState('SECURE').length).toBe(2);
  });
});
