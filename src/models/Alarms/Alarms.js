import { applySnapshot, flow, getSnapshot, types } from 'mobx-state-tree';
import { genPointGeoJSON, getAlarmLabel, sortAlarms } from '../../util';
import { getAlarms, updateMultiAlarmState } from '../../services/Alarms/alarms';

export const alarmsStoreId = 'alarmsStore';
export const initialAlarms = { list: [], draftList: [] };

const AlarmDetails = types.model({
  batteryPercentage: types.maybeNull(types.number),
  confidence: types.maybeNull(types.string),
  credentials: types.maybeNull(types.string),
  eventID: types.maybeNull(types.string),
  personDetected: types.maybeNull(types.boolean),
});

const ConditionsToMeetInOrderToDisplay = types.model({
  hasPermissions: types.maybe(types.array(types.string)),
});

const DisplayProperties = types.model({
  isDisabled: types.boolean,
  toolTipMessage: types.string,
});

const Action = types
  .model({
    displayProperties: DisplayProperties,
    conditionsToMeetInOrderToDisplay: ConditionsToMeetInOrderToDisplay,
  })
  .views((self) => ({
    checkConditions(user) {
      // return true if no conditions exist or conditions are met
      return (
        !self.conditionsToMeetInOrderToDisplay.hasPermissions ||
        self.conditionsToMeetInOrderToDisplay.hasPermissions.every(
          (permission) => user.permissions.includes(permission)
        )
      );
    },
  }));

const NextValidActions = types.model({
  acknowledge: types.maybe(Action),
  secure: types.maybe(Action),
  access: types.maybe(Action),
  acknowledgeUnknown: types.maybe(Action),
});

const Alarm = types
  .model({
    alarmID: types.identifier,
    label: types.string,
    sensorName: types.string,
    sensorType: types.maybeNull(types.string),
    facilityID: types.maybeNull(types.string),
    facilityName: types.maybeNull(types.string),
    floor: types.maybeNull(types.integer),
    zone: types.maybeNull(types.integer),
    geohash: types.string,
    alarmType: types.string,
    priority: types.integer,
    acknowledgementTimeout: types.boolean,
    active: types.boolean,
    state: types.string,
    lat: types.number,
    lon: types.number,
    remark: types.maybeNull(types.string),
    alarmTime: types.maybeNull(types.number),
    additionalAlarmDetails: types.optional(AlarmDetails, {}),
    nextValidActions: NextValidActions,
  })
  .actions((self) => ({
    updateState: flow(function* updateState(newState) {
      const alarm = self;
      try {
        const result = yield updateMultiAlarmState([self], newState);
        if (result) {
          alarm.state = result[0].updatedAlarm.state;
        } else {
          throw new Error('Alarm could not be updated');
        }
      } catch (err) {
        return err;
      }
      return null;
    }),
  }));

const Alarms = types
  .model({
    list: types.array(Alarm),
    draftList: types.array(Alarm),
  })
  .views((self) => ({
    get all() {
      return sortAlarms(self.list);
    },
    get active() {
      const activeAlarms = self.list.filter((alarm) => {
        return alarm.active;
      });
      return sortAlarms(activeAlarms);
    },
    get inactive() {
      const inactiveAlarms = self.list.filter((alarm) => {
        return !alarm.active;
      });
      return sortAlarms(inactiveAlarms);
    },
    get notSecure() {
      const unsecureAlarms = self.list.filter((alarm) => {
        return alarm.state !== 'SECURE';
      });
      return sortAlarms(unsecureAlarms);
    },
    get access() {
      const alarmsInAccess = self.filterByState('ACCESS');
      return sortAlarms(alarmsInAccess);
    },
    get secure() {
      const secureAlarms = self.filterByState('SECURE');
      return sortAlarms(secureAlarms);
    },
    get unacknowledged() {
      const unacknowledgedAlarms = self.filterByState('ALARM');
      return sortAlarms(unacknowledgedAlarms);
    },
    get acknowledged() {
      const acknowledgedAlarms = self.filterByState('ACKNOWLEDGED');
      return sortAlarms(acknowledgedAlarms);
    },
    get unknown() {
      const unknownAlarms = self.filterByState('UNKNOWN');
      return sortAlarms(unknownAlarms);
    },
    get ackUnknown() {
      const ackUnknownAlarms = self.filterByState('ACK_UNKNOWN');
      return sortAlarms(ackUnknownAlarms);
    },
    get countAll() {
      return self.list.length;
    },
    get countActive() {
      return self.active.length;
    },
    get countAccess() {
      return self.access.length;
    },
    get exist() {
      return self.countAll > 0;
    },
    get hasActive() {
      return self.countActive > 0;
    },
    get hasAccess() {
      return self.countAccess > 0;
    },
    get sortedBySensorName() {
      return self.list
        .slice()
        .sort((a, b) => a.sensorName.localeCompare(b.sensorName));
    },
    filterTimedOut() {
      return self.all.filter((alarm) => alarm.acknowledgementTimeout === true);
    },
    filterByFacilityID(facilityID) {
      return self.all.filter((alarm) => alarm.facilityID === facilityID);
    },
    filterByState(...state) {
      return self.all.filter((alarm) => state.includes(alarm.state));
    },
    filterByCoords({ lat, lon }) {
      return self.all.filter((alarm) => alarm.lat === lat && alarm.lon === lon);
    },
    geojsonByState(state, sensors) {
      const markers = self.filterByState(state);
      return genPointGeoJSON(markers, sensors);
    },
    get hasALARM() {
      return self.filterByState('ALARM').length > 0;
    },
    get hasTimeout() {
      return self.filterTimedOut().length > 0;
    },
    get hasDraftItems() {
      return !!self.draftList.length;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const alarms = yield getAlarms();
      applySnapshot(self.list, alarms);
    }),
    update(newAlarms) {
      // TEMP: We are sending updates to a draft list
      newAlarms.map((newAlarm) => {
        // TODO: Figure out cleaner way to do this
        const oldAlarms = self.draftList.filter(
          (alarm) => alarm.alarmID !== newAlarm.alarmID
        );
        let draftAlarm = self.draftList.filter(
          (alarm) => alarm.alarmID === newAlarm.alarmID
        )[0];
        if (draftAlarm) {
          draftAlarm = { ...draftAlarm, ...newAlarm };
        } else {
          draftAlarm = { ...newAlarm };
          draftAlarm.label = getAlarmLabel(newAlarm);
        }
        const newList = [...oldAlarms, draftAlarm];
        return applySnapshot(self.draftList, newList);
      });
    },
    updateList(newAlarms) {
      // TEMP: The old update function pointing at the actual list
      newAlarms.map((newAlarm) => {
        // TODO: Figure out cleaner way to do this
        const oldAlarms = self.list.filter(
          (alarm) => alarm.alarmID !== newAlarm.alarmID
        );
        let draftAlarm = self.list.filter(
          (alarm) => alarm.alarmID === newAlarm.alarmID
        )[0];
        if (draftAlarm) {
          draftAlarm = { ...draftAlarm, ...newAlarm };
        } else {
          draftAlarm = { ...newAlarm };
          draftAlarm.label = getAlarmLabel(newAlarm);
        }
        const newList = [...oldAlarms, draftAlarm];
        return applySnapshot(self.list, newList);
      });
    },
    applyDraft() {
      const alarms = self;
      // TEMP: We'll call this on a timer to effectively debounce list updates
      if (self.hasDraftItems) {
        self.updateList(getSnapshot(self.draftList));
        alarms.draftList = [];
      }
      return true;
    },
    timedOutAlarms() {
      // eslint-disable-next-line array-callback-return
      self.list.map((alarm) => {
        // eslint-disable-next-line no-param-reassign
        alarm.acknowledgementTimeout = false;
      });
    },
  }));

export default Alarms;
