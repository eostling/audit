import { v4 as uuidV4 } from 'uuid';

export const makeAlarmFixture = (props) => {
  const baseObject = {
    alarmID: uuidV4(),
    label: 'Alarm fixture id',
    sensorName: 'Base Sensor',
    sensorType: 'PIR',
    facilityID: '4100',
    facilityName: '',
    floor: 1,
    zone: 1,
    geohash: '34KJDKFJdk',
    alarmType: 'Active',
    priority: 1,
    acknowledgementTimeout: false,
    active: true,
    state: 'Active',
    lat: 79949,
    lon: 79995,
    remark: 'None',
    alarmTime: 1200,
    additionalAlarmDetails: {},
    nextValidActions: {},
  };
  return { ...baseObject, ...props };
};
