import { getParent, types } from 'mobx-state-tree';

/* Include this as a child of another model to make it sortable.
 * When data is loaded, update the field and direction attributes
 * of this model
 */
const Sort = types
  .model('Sort', {
    field: '',
    direction: 'asc',
  })
  .actions((self) => ({
    changeSortField(newField) {
      const ptr = self;
      ptr.field = newField;
      ptr.direction = 'asc';
      getParent(self).load();
    },
    toggleSortDirection() {
      const ptr = self;
      ptr.direction = self.direction === 'asc' ? 'desc' : 'asc';
      getParent(self).load();
    },
  }));

export default Sort;
