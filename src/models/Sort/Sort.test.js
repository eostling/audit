import Sort from './Sort';

const mockLoad = jest.fn(() => {});
jest.mock('mobx-state-tree', () => ({
  ...jest.requireActual('mobx-state-tree'),
  getParent: () => ({
    load: mockLoad,
  }),
}));

describe('Sort model', () => {
  test('initializes properly', () => {
    const sort = Sort.create({});
    expect(sort.field).toBe('');
    expect(sort.direction).toBe('asc');
  });

  test('can change sort field', () => {
    const sort = Sort.create({ field: 'sensorName', direction: 'asc' });
    const mockCalls = mockLoad.mock.calls.length;
    sort.changeSortField('facilityId');
    expect(mockLoad.mock.calls.length).toBe(mockCalls + 1);
    expect(sort.field).toBe('facilityId');
    expect(sort.direction).toBe('asc');
  });

  test('can toggle sort direction', () => {
    const sort = Sort.create({ field: 'sensorName', direction: 'asc' });
    const mockCalls = mockLoad.mock.calls.length;
    sort.toggleSortDirection();
    expect(mockLoad.mock.calls.length).toBe(mockCalls + 1);
    expect(sort.field).toBe('sensorName');
    expect(sort.direction).toBe('desc');
  });
});
