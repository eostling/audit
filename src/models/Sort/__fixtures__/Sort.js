export const makeSortFixture = (props) => {
  const baseObject = {
    field: 'Temp',
    direction: 'asc',
  };
  return { ...baseObject, ...props };
};
