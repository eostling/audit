import { applySnapshot, flow, types } from 'mobx-state-tree';
import { getSensors } from '../../services/Sensors/sensors';
import Pages from '../Pages/Pages';
import Sort from '../Sort/Sort';

export const sensorsStoreId = 'sensorsStore';
export const initialSensors = {
  list: [],
  pages: {},
  sort: { field: 'sensorName', direction: 'asc' },
};

// Process a sensor list to ensure prop types
const cleanSensors = (sensors) => {
  return sensors.map(({ lastModified, lat, lon, floor, zone, ...sensor }) => {
    return {
      ...sensor,
      lastModified: parseInt(lastModified) || null,
      lat: parseInt(lat) || null,
      lon: parseInt(lon) || null,
      floor: parseInt(floor) || null,
      zone: parseInt(floor) || null,
      isSelected: false,
    };
  });
};

const Sensor = types
  .model({
    sensorName: types.string,
    sensorType: types.maybeNull(types.string),
    lat: types.number,
    lon: types.number,
    geohash: types.maybeNull(types.string),
    zone: types.maybeNull(types.integer),
    facilityID: types.maybeNull(types.string),
    floor: types.maybeNull(types.integer),
    assetNumber: types.maybeNull(types.string),
    lastModified: types.maybeNull(types.number),
    notes: types.optional(types.string, ''),
    isSelected: false,
  })
  .actions((self) => ({
    toggle() {
      // eslint-disable-next-line no-param-reassign
      self.isSelected = !self.isSelected;
    },
  }));

const Sensors = types
  .model('Sensors', {
    list: types.array(Sensor), // the current fetched page of sensors
    pages: Pages,
    sort: Sort,
    total: 0, // total # of sensors configured w/ Picard
  })
  .views((self) => ({
    get currentPageItems() {
      return self.list;
    },
    get currentPageCount() {
      return self.list.length;
    },
    get selected() {
      return self.list.filter((sensor) => sensor.isSelected === true);
    },
    get selectedCount() {
      return self.selected.length;
    },
    get isAllSelected() {
      return self.selectedCount === self.currentPageCount;
    },
    get isIntermediateSelected() {
      return (
        self.selectedCount > 0 && self.selectedCount < self.currentPageCount
      );
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const sensor = self;
      const { sensors, total, pagingInfo, sortInfo } = yield getSensors({
        page: self.pages.currentPage,
        length: self.pages.size,
        sortField: self.sort.field,
        sortDirection: self.sort.direction,
      });
      sensor.total = total;
      sensor.pages.pageCount = pagingInfo.numPages;
      sensor.sort.field = sortInfo.sortField;
      sensor.sort.direction = sortInfo.sortDirection;
      applySnapshot(self.list, sensors);
    }),

    update(newSensors) {
      const cleanedSensors = cleanSensors(newSensors);
      const sensorNames = cleanedSensors.map((sensor) => sensor.sensorName);

      const prevSensors = self.list.filter((sensor) => {
        return !sensorNames.includes(sensor.sensorName);
      });

      applySnapshot(self.list, [...prevSensors, ...cleanedSensors]);
      return self.load();
    },

    delete(deletedSensors) {
      const sensor = self;
      const updatedSensors = self.list.filter(
        (sensor) => !deletedSensors.includes(sensor.sensorName)
      );

      // TODO: Include this data in the response so we're not calculating it by hand
      sensor.total -= deletedSensors.length;
      applySnapshot(self.list, updatedSensors);

      // TODO: Figure out a way to recompute paging without making a full load call
      // Possibly return that info with the delete call
      return self.load();
    },
    selectAll() {
      // eslint-disable-next-line array-callback-return
      self.list.map((sensor) => {
        // eslint-disable-next-line no-param-reassign
        sensor.isSelected = true;
      });
    },
    deselectAll() {
      // eslint-disable-next-line array-callback-return
      self.list.map((sensor) => {
        // eslint-disable-next-line no-param-reassign
        sensor.isSelected = false;
      });
    },
  }));

export default Sensors;
