export const makeSensorFixture = (props) => {
  const baseObject = {
    sensorName: 'Temp,',
    sensorType: 'PIR',
    lat: 1,
    lon: 1,
    geohash: '0000',
    zone: 1,
    facilityID: '1',
    floor: 1,
    assetNumber: 'TempAssetNum',
    lastModified: 1,
    notes: 'default',
    isSelected: false,
  };
  return { ...baseObject, ...props };
};
