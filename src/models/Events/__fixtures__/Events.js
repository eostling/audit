export const makeEventsFixture = (props) => {
  const baseObject = {
    messageID: '1,',
    message: ' BaseEvent',
    received: 'Base User',
    messageType: 'Base Sensor',
  };
  return { ...baseObject, ...props };
};
