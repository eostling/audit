import { applySnapshot, flow, types } from 'mobx-state-tree';
import axios from 'axios';

export const eventsStoreId = 'eventStore';
export const initialEvents = { list: [] };

const Event = types.model({
  messageID: types.string,
  message: types.string,
  received: types.string,
  messageType: types.string,
});

const Events = types
  .model({
    list: types.array(Event),
  })
  .views((self) => ({
    get all() {
      return self.list;
    },
    get countAll() {
      return self.list.length;
    },
    get hasEvents() {
      return self.countAll > 0;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      // self.load()
    },
    load: flow(function* load() {
      const { data } = yield axios.get('/api/messages');
      applySnapshot(self.list, data);
    }),
  }));

export default Events;
