export const makeAuditFixture = (props) => {
  const baseObject = {
    eventID: '1,',
    eventType: ' BaseEvent',
    username: 'Base User',
    sensorName: 'Base Sensor',
    alarmType: 'Active',
    stateInputUpdate: 'Input',
    time: '1/12/1989',
  };
  return { ...baseObject, ...props };
};
