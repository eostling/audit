import { applySnapshot, flow, types } from 'mobx-state-tree';
import axios from 'axios';
import { getAuditRecords } from '../../services/Audit/audit';
import Pages from '../Pages/Pages';
import { formatDateShort, formatTime24 } from '../../util';

export const auditStoreId = 'auditStore';
export const initialAudit = { list: [], pages: { size: 10 } };

const AuditRecord = types
  .model('AuditRecord', {
    eventID: types.string,
    eventType: types.string,
    username: types.string,
    sensorName: types.string,
    alarmType: types.string,
    stateInputUpdate: types.maybeNull(types.string),
    time: types.string,
  })
  .views((self) => ({
    get timestamp() {
      const datetime = Number(self.time);
      const date = formatDateShort(datetime);
      const time = formatTime24(datetime);
      return `${date} ${time}`;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.loadMessage();
    },
    loadMessage: flow(function* loadMessage() {
      const audit = self;
      const {
        data: { message },
      } = yield axios.get(`/api/message/${self.eventID}`);
      audit.message = message;
    }),
  }));

const AuditRecords = types
  .model({
    list: types.array(AuditRecord),
    pages: Pages,
    total: 0,
  })
  .views((self) => ({
    get all() {
      return self.list;
    },
    get countAll() {
      return self.list.length;
    },
    get hasRecords() {
      return self.countAll > 0;
    },
  }))
  .actions((self) => ({
    load: flow(function* load() {
      const audit = self;
      const { records, total, pagingInfo } = yield getAuditRecords({
        page: self.pages.currentPage,
        length: self.pages.size,
      });
      audit.total = total;
      audit.pages.pageCount = pagingInfo.numPages;
      applySnapshot(self.list, records);
    }),
  }));

export default AuditRecords;
