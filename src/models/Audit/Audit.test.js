import AuditRecords from './Audit';

export const testAuditRecords = [
  {
    eventID: '6722560786247450624',
    sensorName: 'TESTSENSOR_PIR001',
    username: 'johndoe123',
    alarmType: 'INTRUSION',
    eventType: 'ALARM',
    time: '1602783390595',
    stateInputUpdate: null,
  },
  {
    eventID: '6722560959266684928',
    sensorName: 'TESTSENSOR_PIR001',
    username: 'johndoe123',
    alarmType: 'INTRUSION',
    eventType: 'ACKNOWLEDGED',
    time: '1602783431838',
    sensorType: 'utc_ap669',
    stateInputUpdate: null,
  },
];

describe('Audit', () => {
  test('empty record list', async () => {
    const instance = AuditRecords.create({ list: [], pages: {} });
    expect(instance).not.toBeNull();
    expect(instance.countAll).toBe(0);
    expect(instance.hasRecords).toBe(false);
    expect(instance.all).toEqual([]);
  });

  test('loaded records', async () => {
    const instance = AuditRecords.create({ list: testAuditRecords, pages: {} });
    expect(instance).not.toBeNull();
    expect(instance.countAll).toBe(testAuditRecords.length);
    expect(instance.hasRecords).toBe(true);
    // model stores a subset of fields from input record
    expect(testAuditRecords[0]).toMatchObject(instance.all[0]);
  });
});
