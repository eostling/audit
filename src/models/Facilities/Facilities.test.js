import { Facilities, Zones } from './Facilities';

const testFacilities = [
  {
    facilityID: 'TESTFACILITY',
    displayName: 'Test Facility',

    lat: 10.123,
    lon: 10.1234,
    numFloors: 2,
    geohash: 'aaa',
    streetAddress: null,
    cadFilenames: { 1: 'one', 2: 'two' },
    zoneID: 1,
  },
  {
    facilityID: 'TESTFACILITY2',
    displayName: 'Test Facility 2',

    lat: 20.123,
    lon: 20.1234,
    numFloors: 3,
    geohash: 'bbb',
    streetAddress: null,
    cadFilenames: { 1: 'one', 2: 'two', 3: 'three' },
    zoneID: 2,
  },
];

const mockFacilities = testFacilities;
jest.mock('../../services/Facilities/facilities', () => ({
  getAllFacilities: async () => {
    return mockFacilities;
  },
  getActiveFacilities: async () => {
    return mockFacilities;
  },
}));

const testZones = [
  {
    displayName: 'Test Zone',
    zoneID: 1,
  },
  {
    displayName: 'Test Zone 2',
    zoneID: 2,
  },
];

const mockZones = testZones;
jest.mock('../../services/Zones/zones', () => ({
  getAllZones: async () => {
    return mockZones;
  },
}));

describe('Facilities Model', () => {
  let facilities;
  beforeEach(() => {
    facilities = Facilities.create({ list: testFacilities });
  });
  test('load', async () => {
    const loadTestFacilities = Facilities.create({ list: [] });
    await loadTestFacilities.load();
    expect(loadTestFacilities.count).toBe(2);
  });

  test('can create a Facilities instance', () => {
    expect(facilities).not.toBeNull();
  });
  test('Facilities.all', () => {
    expect(facilities.all.length).toBe(2);
  });
  test('Facilities.count', () => {
    expect(facilities.count).toBe(2);
  });

  test('inZone', () => {
    const filtered = facilities.inZone('2');
    expect(filtered.length).toBe(1);
    expect(filtered[0].displayName).toBe('Test Facility 2');
  });
  test('inZone no ID given returns all', () => {
    const filtered = facilities.inZone(undefined);
    expect(filtered.length).toBe(2);
  });
  test('Facility.floors', async () => {
    const fac = facilities.all[0];
    const result = fac.floors;
    expect(result).toEqual(['1', '2']);
  });
});

describe('Zones Model', () => {
  let zones;
  beforeEach(() => {
    zones = Zones.create({ list: testZones });
  });
  test('load', async () => {
    const loadTestZones = Zones.create({ list: [] });
    await loadTestZones.load();
    expect(loadTestZones.count).toBe(2);
  });

  test('can create a Zones instance', () => {
    expect(zones).not.toBeNull();
  });
  test('Zones.all', () => {
    expect(zones.all.length).toBe(2);
  });
  test('Zones.count', () => {
    expect(zones.count).toBe(2);
  });
});
