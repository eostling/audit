export const makeFacilityFixture = (props) => {
  const baseObject = {
    facilityID: '1,',
    displayName: ' BaseEvent',
    lat: 1,
    lon: 2,
    numFloors: 1,
    geohash: 'Input',
    streetAddress: '1/12/1989',
    cadFilenames: 'Filename',
    zoneID: 2,
    state: 'Active',
    isSelected: false,
  };
  return { ...baseObject, ...props };
};
