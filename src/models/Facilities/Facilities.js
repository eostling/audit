import { applySnapshot, flow, types } from 'mobx-state-tree';
import {
  getAllFacilities,
  updateFacilities,
  getActiveFacilities,
} from '../../services/Facilities/facilities';
import { getAllZones } from '../../services/Zones/zones';

const Zone = types.model('Zone', {
  zoneID: types.integer,
  displayName: types.string,
});

export const zonesStoreId = 'zonesStore';
export const initialZones = { list: [] };
export const Zones = types
  .model('Zones', {
    list: types.array(Zone),
  })
  .views((self) => ({
    get all() {
      return self.list;
    },
    get count() {
      return self.list.length;
    },
  }))
  .actions((self) => ({
    afterCreate() {
      self.load();
    },
    load: flow(function* load() {
      const zones = yield getAllZones();
      applySnapshot(self.list, zones);
    }),
  }));

const updateFacility = async (facility, newState) => {
  try {
    const { facilityID } = facility;
    const result = await updateFacilities([facilityID], newState);
    if (result) {
      const updatedAlarms = result.filter((resp) => {
        return resp.statusCode === 200;
      });
      if (updatedAlarms.length === result.length) {
        return newState;
      }
    }
  } catch (err) {
    // TODO: add logging service/end point
    throw new Error(err);
  }
  return facility;
};

const Facility = types
  .model('Facility', {
    facilityID: types.string,
    displayName: types.string,
    lat: types.number,
    lon: types.number,
    numFloors: types.integer,
    geohash: types.string,
    streetAddress: types.maybeNull(types.string),
    cadFilenames: types.map(types.maybeNull(types.string)),
    zoneID: types.integer,
    state: types.maybe(types.string),
    isSelected: false,
  })
  .views((self) => ({
    get floors() {
      return [...self.cadFilenames.keys()];
    },
    get canAccess() {
      return self.state !== 'UNKNOWN';
    },
    get canSecure() {
      return self.state !== 'UNKNOWN';
    },
  }))
  .actions((self) => ({
    secure: flow(function* updateState() {
      const facility = self;
      const newState = yield updateFacility(self, 'SECURE');
      if (newState) {
        facility.state = newState;
      } else {
        throw new Error('Alarm could not be updated');
      }
    }),
    access: flow(function* updateState() {
      const facility = self;
      const newState = yield updateFacility(self, 'ACCESS');
      if (newState) {
        facility.state = newState;
      } else {
        throw new Error('Alarm could not be updated');
      }
    }),
  }));

export const facilitiesStoreId = 'facilitiesStore';
export const initialFacilities = { list: [] };
export const Facilities = types
  .model('Facilities', {
    list: types.array(Facility),
    active: types.array(Facility),
  })
  .views((self) => ({
    get all() {
      return self.list;
    },
    get count() {
      return self.list.length;
    },
    get selected() {
      return self.list.filter((facilities) => facilities.isSelected === true);
    },
    get selectedCount() {
      return self.selected.length;
    },
    get isAllSelected() {
      return self.selectedCount === self.count;
    },
    get isIntermediateSelected() {
      return self.selectedCount > 0 && self.selectedCount < self.count;
    },
    get sortedById() {
      return self.list.slice().sort((a, b) => a.facilityID - b.facilityID);
    },
    get activeSortedById() {
      return self.active.slice().sort((a, b) => a.facilityID - b.facilityID);
    },
    byID(facilityID) {
      return self.list.filter((fac) => fac.facilityID === facilityID)[0];
    },
    details(facilityID) {
      // TODO use different endpoint to get additional facility details?
      return self.list.filter((fac) => fac.facilityID === facilityID)[0];
    },
    inZone(zoneID) {
      if (!zoneID) {
        return self.all;
      }
      if (typeof zoneID === 'string') {
        // eslint-disable-next-line no-param-reassign
        zoneID = parseInt(zoneID);
      }
      return self.list.filter((fac) => fac.zoneID === zoneID);
    },
  }))
  .actions((self) => ({
    load: flow(function* load() {
      const facilities = yield getAllFacilities();
      const activeFacilities = yield getActiveFacilities();
      applySnapshot(self.list, facilities);
      applySnapshot(self.active, activeFacilities);
    }),
    update(newFacility) {
      const updatedFacilities = self.list.map((facility) => {
        if (facility.facilityID === newFacility.facilityID)
          // eslint-disable-next-line no-param-reassign
          facility.state = newFacility.state;
        return facility;
      });
      applySnapshot(self.list, updatedFacilities);
    },
  }));

export default Facilities;
