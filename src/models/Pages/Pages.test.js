import Pages from './Pages';

const mockLoad = jest.fn(() => {});
jest.mock('mobx-state-tree', () => ({
  ...jest.requireActual('mobx-state-tree'),
  getParent: () => ({
    load: mockLoad,
  }),
}));

describe('Pages model', () => {
  test('Pages initializes properly', () => {
    const pages = Pages.create({});
    expect(pages.currentPage).toBe(1);
    expect(pages.size).toBe(25);
    expect(pages.pageCount).toBe(0);
    expect(pages.hasPrev).toBe(false);
    expect(pages.hasNext).toBe(false);
  });

  test('next', () => {
    const pages = Pages.create({ pageCount: 2 });
    expect(pages.hasNext).toBe(true);
    const mockCalls = mockLoad.mock.calls.length;
    pages.next();
    expect(mockLoad.mock.calls.length).toBe(mockCalls + 1);
  });

  test('prev', () => {
    const pages = Pages.create({ pageCount: 2, currentPage: 2 });
    expect(pages.hasPrev).toBe(true);
    const mockCalls = mockLoad.mock.calls.length;
    pages.prev();
    expect(mockLoad.mock.calls.length).toBe(mockCalls + 1);
  });
  test('changeSize', () => {
    const pages = Pages.create({ pageCount: 2, currentPage: 2 });
    const mockCalls = mockLoad.mock.calls.length;
    pages.changeSize(11);
    expect(mockLoad.mock.calls.length).toBe(mockCalls + 1);
    expect(pages.size).toBe(11);
    expect(pages.currentPage).toBe(1);
  });
});
