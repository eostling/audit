import { getParent, types } from 'mobx-state-tree';

/* Include this as a child of another model to make it paginated
 * next/prev will call the load() action on the parent model.
 * When data is loaded, update the pageCount attribute of this model
 */
const Pages = types
  .model('Pages', {
    currentPage: 1,
    size: 25,
    pageCount: 0,
  })
  .views((self) => ({
    get hasNext() {
      return self.currentPage < self.pageCount;
    },
    get hasPrev() {
      return self.currentPage !== 1;
    },
  }))
  .actions((self) => ({
    next() {
      const pages = self;
      pages.currentPage += 1;
      getParent(pages).load();
    },
    prev() {
      const pages = self;
      pages.currentPage -= 1;
      getParent(pages).load();
    },
    changeSize(newSize) {
      const pages = self;
      pages.size = newSize;
      pages.currentPage = 1;
      getParent(pages).load();
    },
  }));

export default Pages;
