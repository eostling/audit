export const makePagesFixture = (props) => {
  const baseObject = {
    currentPage: 4,
    size: 25,
    pageCount: 20,
  };
  return { ...baseObject, ...props };
};
