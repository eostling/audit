import { types, applySnapshot } from 'mobx-state-tree';
import { getFloor } from '../../services/Map/map';

export const DEFAULT_MAP_STYLE_KEY = 'satellite';
const DEFAULT_FLYTO_ZOOM = 12;

export const maxZoom = 20;
export const minZoom = 13;

export const mapStoreId = 'mapStore';
export const initialMap = {
  viewport: {},
};

/* Viewport used to contain a transitionDuration property that caused transitions to be animated.
 * Unfortunately this resulted in an exception crashing the map when you clicked a map navigation button
 * and switched to a different page and back (without moving the map via mouse after the button click).
 * For now, transitionDuration has been removed to avoid this bug.
 */
const Viewport = types
  .model({
    latitude: types.number,
    longitude: types.number,
    zoom: 16,
    bearing: 0,
    pitch: 0,
  })
  .actions((self) => ({
    flyTo({ lat, lon, zoom = DEFAULT_FLYTO_ZOOM }) {
      const fly = {
        ...self,
        latitude: lat,
        longitude: lon,
        zoom,
      };
      applySnapshot(self, fly);
    },
    resetBearing() {
      const viewport = self;
      const isDefault = self.pitch === 0 && self.bearing === 0;
      viewport.pitch = isDefault ? 60 : 0;
      viewport.bearing = 0;
    },
    update(newViewport) {
      applySnapshot(self, newViewport);
    },
    zoomIn() {
      const viewport = self;
      if (viewport.zoom < maxZoom && viewport.zoom >= minZoom) {
        viewport.zoom += 1;
      }
    },
    zoomOut() {
      const viewport = self;
      if (viewport.zoom > minZoom && viewport.zoom <= maxZoom) {
        viewport.zoom -= 1;
      }
    },
  }));

const emptyFloor = { type: 'FeatureCollection', features: [] };
const Map = types
  .model({
    viewport: Viewport,
    activeBuilding: types.maybe(types.string),
    activeFloor: types.maybe(types.string),
    floorplan: types.frozen(emptyFloor),
  })
  .views((self) => ({
    get floorFilter() {
      return {
        building: self.activeBuilding,
        floor: self.activeFloor,
      };
    },
  }))
  .actions((self) => ({
    setFloorplan(floorplan) {
      const viewport = self;
      viewport.floorplan = floorplan;
    },
    setFloorFilter({ building, floor }) {
      const viewport = self;

      viewport.activeBuilding = building;
      viewport.activeFloor = floor;
      if (floor) {
        getFloor(building, floor).then((floorplan) => {
          self.setFloorplan(floorplan);
        });
      } else {
        self.setFloorplan(emptyFloor);
      }
    },
  }));

export default Map;
