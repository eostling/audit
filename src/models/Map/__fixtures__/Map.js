export const makeMapFixture = (props) => {
  const baseObject = {
    viewport: {},
    activeBuilding: ' BaseEvent',
    activeFloor: 'Base User',
    floorplan: {},
  };
  return { ...baseObject, ...props };
};
