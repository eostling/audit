import Map from './Map';

const testMap = {
  viewport: {
    latitude: 10,
    longitude: -10,
  },
};

it('can create a Map instance', () => {
  const map = Map.create(testMap);
  expect(map).not.toBeNull();
});

it('has default values', () => {
  const map = Map.create(testMap);
  expect(map.viewport.zoom).toBe(16);
  expect(map.viewport.bearing).toBe(0);
  expect(map.viewport.pitch).toBe(0);
});

it('can update', () => {
  const map = Map.create(testMap);
  map.viewport.update({
    latitude: 0,
    longitude: 0,
  });
  expect(map.viewport.latitude).toBe(0);
  expect(map.viewport.longitude).toBe(0);
});

it('can fly to coords', () => {
  const map = Map.create(testMap);
  expect(map.viewport.latitude).toBe(10);
  expect(map.viewport.longitude).toBe(-10);
  expect(map.viewport.zoom).toBe(16);
  map.viewport.flyTo({ lat: 0, lon: 0 });
  expect(map.viewport.latitude).toBe(0);
  expect(map.viewport.longitude).toBe(0);
  expect(map.viewport.zoom).toBe(12);
});

it('can zoom in & out', () => {
  const map = Map.create(testMap);
  expect(map.viewport.zoom).toBe(16);
  map.viewport.zoomIn();
  expect(map.viewport.zoom).toBe(17);
  map.viewport.zoomOut();
  expect(map.viewport.zoom).toBe(16);
});

it('can reset bearing and toggle pitch', () => {
  const map = Map.create(testMap);
  expect(map.viewport.bearing).toBe(0);
  map.viewport.update({
    latitude: 10,
    longitude: -10,
    bearing: 10,
  });
  expect(map.viewport.bearing).toBe(10);
  map.viewport.resetBearing();
  expect(map.viewport.bearing).toBe(0);
  expect(map.viewport.pitch).toBe(0);
  map.viewport.resetBearing();
  expect(map.viewport.pitch).toBe(60);
});
