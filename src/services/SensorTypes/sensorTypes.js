import axios from 'axios';

// Get all sensor types (paginated)
export const getSensorTypes = async ({ page, length }) => {
  if (!page) {
    throw new Error('Page number not provided!');
  }

  if (!length) {
    throw new Error('Page length not provided!');
  }

  const { data } = await axios.get(`/api/sensorTypes`);
  return data;
};

// Add sensors
export const addSensorTypes = async (sensorType, sensorId) => {
  try {
    const response = await fetch(`/api/sensorType/${sensorId}`, {
      method: 'PUT', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(sensorType), // body data type must match "Content-Type" header
    });
    return response.json();
  } catch (err) {
    throw new Error(err);
  }
};

const getAllSensorTypes = async () => {
  const { data } = await axios.get(`/api/sensorTypes`);
  return data.sensorTypes;
};

export const getMakeLookup = async () => {
  const types = await getAllSensorTypes();
  const lookup = {};
  types.forEach((type) => {
    if (lookup[type.make] === undefined) {
      lookup[type.make] = [type];
    } else {
      lookup[type.make].push(type);
    }
  });
  return lookup;
};
