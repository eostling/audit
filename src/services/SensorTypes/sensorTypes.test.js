import mockAxios from 'axios';
import { getMakeLookup, getSensorTypes } from './sensorTypes';
import { testSensorTypes } from '../../models/SensorTypes/SensorTypes.test';

const mockResult = testSensorTypes;

describe('getSensorTypes', () => {
  test('getSensorTypes success', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getSensorTypes({ page: 1, length: 10 });
    expect(result).toBe(mockResult);
    expect(mockAxios.get).toHaveBeenCalledWith('/api/sensorTypes');
  });

  test('error if no page number', async () => {
    await expect(getSensorTypes({ length: 10 })).rejects.toThrow();
  });

  test('error if no page size', async () => {
    await expect(getSensorTypes({ page: 1 })).rejects.toThrow();
  });
});

describe('getMakeLookup', () => {
  test('getMakeLookup', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getMakeLookup();
    expect(result).toEqual({
      a: [mockResult.sensorTypes[0]],
      b: [mockResult.sensorTypes[1]],
    });
  });
});
