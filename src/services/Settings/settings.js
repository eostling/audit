import axios from 'axios';

const URL = '/api/settings/prime';

/**
 * getSettings
 * Retrieve object containing PRIME settings
 * @returns {Promise<Object[]>}
 */
export const getSettings = async () => {
  const response = await axios.get(URL);
  return response.data;
};
