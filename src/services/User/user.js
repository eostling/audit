import axios from 'axios';

const URL = '/api/user';

/**
 * addUser
 * Add user to system
 * @param {Object} user
 * @param {string} user.username
 * @param {string} user.password
 * @param {string} user.role
 * @returns {Promise<void>}
 */
export const addUser = (user) => {
  return axios.post(URL, user);
};

/**
 * getUsers
 * Retrieve list of users
 * @returns {Promise<Object[]>}
 */
export const getUsers = async () => {
  const response = await axios.get(URL);
  return response.data.sort((a, b) => {
    if (a.username < b.username) {
      return -1;
    }
    if (a.username > b.username) {
      return 1;
    }
    return 0;
  });
};

/**
 * removeUser
 * Remove user from system
 * @param {Object} user
 * @param {string} user.username
 * @returns {Promise<void>}
 */
export const removeUser = (user) => {
  return axios.delete(`${URL}/${user.username}`);
};
