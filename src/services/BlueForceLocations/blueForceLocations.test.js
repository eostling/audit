import mockAxios from 'axios';
import {
  getBlueForceDevices,
  getBlueForceLocation,
} from './blueForceLocations';

const blueForceDevice1 = 'device1';
const blueForceDevice2 = 'device2';
const testBlueForceDeviceList = [blueForceDevice1, blueForceDevice2];
const testLocation1 = {
  sensorName: 'device1',
  latitude: 1,
  longitude: 1,
};

const mockGetBlueForceLocation = (url) => {
  let result;
  if (url.endsWith('/device')) {
    result = testBlueForceDeviceList;
  } else {
    result = testLocation1;
  }
  return Promise.resolve({ data: result });
};

describe('BlueForceLocations service', () => {
  beforeAll(() => {
    mockAxios.get.mockImplementation(mockGetBlueForceLocation);
  });

  test('get blue force locations', async () => {
    const result = await getBlueForceDevices();
    expect(result).toBeTruthy();
  });
  test('get blue force location', async () => {
    const result = await getBlueForceLocation(blueForceDevice1);
    expect(result).toBeTruthy();
  });
});
