import axios from 'axios';

export const getBlueForceDevices = async () => {
  const data = await axios.get('/api/blueforce/device');
  return data;
};

export const getBlueForceLocations = async () => {
  const { data } = await axios.get('/api/blueforce/location');
  return data;
};

export const getBlueForceLocation = async (sensorName) => {
  const { data } = await axios.get(`/api/blueforce/location/${sensorName}`);
  return data;
};
