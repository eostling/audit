import mockAxios from 'axios';
import { getAlarms, updateAlarmState, updateMultiAlarmState } from './alarms';

const testAlarms = [
  {
    sensorName: 'Test_01',
    lat: 10,
    lon: -10,
    geohash: 'aaaaaaaaa',
    zone: null,
    facilityID: 'facility1',
    floor: 1,
    alarmID: '1',
    alarmType: 'BADGE_READER',
    priority: 1,
    groupAccess: 'false',
    active: false,
    state: 'SECURE',
    alarmTime: 0,
  },
  {
    sensorName: 'Test_02',
    lat: 12,
    lon: -12,
    geohash: 'bbbbbbbbb',
    zone: null,
    facilityID: 'facility1',
    floor: 2,
    alarmID: '2',
    alarmType: 'MOTION',
    priority: 1,
    groupAccess: 'false',
    active: false,
    state: 'ALARM',
    alarmTime: 0,
  },
];
const mockAxiosError = () =>
  Promise.reject(new Error('Test Error: Update rejected'));

it('should return alarms', async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.resolve({ data: testAlarms })
  );

  const alarms = await getAlarms();
  expect(alarms.length).toBe(2);
});

it('should generate labels', async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.resolve({ data: testAlarms })
  );

  const alarms = await getAlarms();
  expect(alarms[0].label).not.toBeNull();
  expect(alarms[0].label).toBe('Test 01 - BADGE READER');
  expect(alarms[1].label).not.toBeNull();
  expect(alarms[1].label).toBe('Test 02 - MOTION');
});

it('preserve props', async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.resolve({ data: testAlarms })
  );

  const alarms = await getAlarms();
  const alarm = alarms[0];
  expect(alarm.sensorName).toBe('Test_01');
  expect(alarm.lat).toBe(10);
  expect(alarm.lon).toBe(-10);
  expect(alarm.geohash).toBe('aaaaaaaaa');
  expect(alarm.zone).toBeNull();
  expect(alarm.facilityID).toBe('facility1');
  expect(alarm.floor).toBe(1);
  expect(alarm.alarmID).toBe('1');
  expect(alarm.alarmType).toBe('BADGE_READER');
  expect(alarm.priority).toBe(1);
  expect(alarm.groupAccess).toBe('false');
  expect(alarm.active).toBeFalsy();
  expect(alarm.state).toBe('SECURE');
  expect(alarm.alarmTime).toBe(0);
});

it('should handle no alarms', async () => {
  mockAxios.get.mockImplementationOnce(() => Promise.resolve({ data: [] }));

  const alarms = await getAlarms();
  expect(alarms).toEqual([]);
});

it('should return an updated alarm', async () => {
  mockAxios.put.mockImplementationOnce(() =>
    Promise.resolve({
      data: {
        alarmStateInfo: { state: 'ACCESS' },
      },
    })
  );

  const testAlarm = testAlarms[0];
  const newState = await updateAlarmState(testAlarm, 'ACCESS');
  expect(newState).toBe('ACCESS');
});

it('should return multiple updated alarms', async () => {
  const response = {
    data: {
      alarmStateInfo: { state: 'ACCESS' },
    },
  };
  mockAxios.put.mockImplementationOnce(() => Promise.resolve(response));

  const result = await updateMultiAlarmState(testAlarms, 'ACCESS');
  expect(result).toBe(response.data);
});

it('should handle errors for updateAlarmState', async () => {
  mockAxios.put.mockImplementationOnce(mockAxiosError);

  const testAlarm = testAlarms[0];
  const updatedAlarm = await updateAlarmState(testAlarm, 'ACCESS');
  expect(updatedAlarm).toStrictEqual(new Error('Test Error: Update rejected'));
});

it('should handle errors for updateMultiAlarmState', async () => {
  mockAxios.put.mockImplementationOnce(mockAxiosError);

  const updatedAlarm = await updateMultiAlarmState(testAlarms, 'ACCESS');
  expect(updatedAlarm).toStrictEqual(new Error('Test Error: Update rejected'));
});
