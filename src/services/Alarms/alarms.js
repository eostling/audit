import axios from 'axios';
import { getAlarmLabel } from '../../util';

// Get all nominations
export const getAlarms = async () => {
  const { data } = await axios.get('/api/alarms');
  // Convert snake_case props to camelCase
  const alarms = data.map(({ sensorName, alarmType, ...rest }) => ({
    label: getAlarmLabel({ sensorName, alarmType }),
    sensorName,
    alarmType,
    ...rest,
  }));

  return alarms;
};

export const updateAlarmState = async ({ sensorName, alarmType }, newState) => {
  let urlState = newState.toLowerCase();
  // API path is different for one desired state
  if (urlState === 'acknowledged') {
    urlState = 'acknowledge';
  }
  const updateURL = `/api/alarms/actions/${urlState}/${sensorName}-${alarmType}`;
  try {
    const { data } = await axios.put(updateURL);
    return data.alarmStateInfo.state;
  } catch (err) {
    return err;
  }
};

export const updateMultiAlarmState = async (alarms, newState) => {
  let urlState = newState.toLowerCase();
  // API path is different for one desired state
  if (urlState === 'acknowledged') {
    urlState = 'acknowledge';
  }

  const updateURL = `/api/alarms/actions/${urlState}/`;

  try {
    const { data } = await axios.put(updateURL, {
      alarms,
    });
    return data;
  } catch (err) {
    return err;
  }
};

export const updateMultiAlarmStateSecureDialog = async (
  alarms,
  userRemark,
  assessment,
  newState
) => {
  let urlState = newState.toLowerCase();
  // API path is different for one desired state
  if (urlState === 'acknowledged') {
    urlState = 'acknowledge';
  }

  const updateURL = `/api/alarms/actions/${urlState}/`;

  try {
    const { data } = await axios.put(updateURL, {
      alarms,
      userRemark,
      assessment,
    });
    return data;
  } catch (err) {
    return err;
  }
};

export const startAlarmPlayback = async (eventID) => {
  const res = await axios.get(`/api/replay/event/${eventID}`);
  return res;
};
