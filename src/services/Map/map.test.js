import mockAxios from 'axios';
import { getFloors } from './map';

const testViewport = { nLat: 10, eLon: 10, sLat: -10, wLon: -10 };
const testFloors = {};

it('should expect viewport and return floors', async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.resolve({ data: testFloors })
  );

  const floors = await getFloors(testViewport);
  expect(floors).not.toBeNull();
});

it('should handle errors', async () => {
  mockAxios.get.mockImplementationOnce(() =>
    Promise.reject(new Error('Test Error: Failed to get floors'))
  );

  const failedFloors = await getFloors(testViewport);
  expect(failedFloors).toStrictEqual(
    new Error('Test Error: Failed to get floors')
  );
});
