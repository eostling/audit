import axios from 'axios';

// Get floor geoJSON based on viewport coordinates
export const getFloors = async ({ nLat, eLon, sLat, wLon }) => {
  const geoJsonUrl = `/api/geojson/viewport?nLat=${nLat}&eLon=${eLon}&sLat=${sLat}&wLon=${wLon}`;
  try {
    const { data } = await axios.get(geoJsonUrl);
    return data;
  } catch (err) {
    return err;
  }
};

export const getFloor = async (facilityID, floor) => {
  const geoJsonUrl = `/api/floorplan/?facility=${facilityID}&floor=${floor}`;
  try {
    const { data } = await axios.get(geoJsonUrl);
    return data;
  } catch (err) {
    return err;
  }
};
