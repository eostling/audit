import axios from 'axios';

// Get all audit records (paginated)
export const getAuditRecords = async ({ page, length }) => {
  if (!page) {
    throw new Error('Page number not provided!');
  }
  if (!length) {
    throw new Error('Page length not provided!');
  }
  const { data } = await axios.get(
    `/api/audit?currentPage=${page}&pageLen=${length}`
  );
  return data;
};

// get all audit records within date range
export const getAuditRecordsByDate = async (start, end) => {
  const { data } = await axios.get(
    `/api/audit?startTime=${start}&endTime=${end}`
  );
  return data;
};
