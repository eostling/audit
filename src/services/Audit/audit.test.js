import mockAxios from 'axios';
import { getAuditRecords } from './audit';
import { testAuditRecords } from '../../models/Audit/Audit.test';

const mockResult = testAuditRecords;

describe('getAuditRecords', () => {
  test('getSensorTypes success', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getAuditRecords({ page: 1, length: 10 });
    expect(result).toBe(mockResult);
    expect(mockAxios.get).toHaveBeenCalledWith(
      '/api/audit?currentPage=1&pageLen=10'
    );
  });

  test('error if no page number', async () => {
    await expect(getAuditRecords({ length: 10 })).rejects.toThrow();
  });

  test('error if no page size', async () => {
    await expect(getAuditRecords({ page: 1 })).rejects.toThrow();
  });
});
