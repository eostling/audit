import axios from 'axios';

export const sensorAlarmState = async (
  sensorName,
  alarmType,
  state,
  additionalAlarmDetails
) => {
  let sensorReported = null;

  switch (state.toLowerCase()) {
    case 'reset':
      sensorReported = 'SECURE';
      break;
    case 'trigger':
      sensorReported = 'ALARM';
      break;
    case 'unknown':
      sensorReported = 'UNKNOWN';
      break;
    default:
      return null;
  }

  const stateUpdate = {
    sensorName,
    alarmType,
    update: { state: { sensorReported } },
    additionalAlarmDetails,
  };
  try {
    return await axios.put(`/api/alarms/actions/evaluate`, stateUpdate);
  } catch (err) {
    const sensorAlarmStateError = new Error();
    sensorAlarmStateError.message =
      'An error occured while calling /alarms/actions/evaluate';
    sensorAlarmStateError.originalError = err;
    throw sensorAlarmStateError;
  }
};

export const getAlarmTwin = async (sensorName, alarmType) => {
  const alarmName = `${sensorName}-${alarmType}`;
  const response = await axios.get(`/api/alarmState/${alarmName}`);
  return response.data;
};

const bdocOperation = async (sensorName, alarmType, operation) => {
  const validOperations = ['access', 'acknowledge', 'ack_unknown', 'secure'];

  if (!validOperations.includes(operation)) {
    throw new Error(
      `Invalid Operation ${operation}, valid operations are ${validOperations.join()}`
    );
  }

  const requestBody = { alarms: [{ sensorName, alarmType }] };
  const response = await axios.put(
    `/api/alarms/actions/${operation}`,
    requestBody
  );
  return response.data;
};

export const clearAlarm = async (sensorName, alarmType) => {
  const twinData = await getAlarmTwin(sensorName, alarmType);
  if (twinData.stateInfo.state === 'ALARM') {
    await bdocOperation(sensorName, alarmType, 'acknowledge');
  }

  if (twinData.stateInfo.state === 'UNKNOWN') {
    await bdocOperation(sensorName, alarmType, 'ack_unknown');
  }

  await sensorAlarmState(sensorName, alarmType, 'reset');
  await axios.put(`/api/alarms/actions/evaluate/`, {
    sensorName,
    alarmType,
    update: { workflowRequested: 'SECURE', state: true },
  });

  const secureBdocOperation = await bdocOperation(
    sensorName,
    alarmType,
    'secure'
  );

  return secureBdocOperation;
};

const shuffleArray = (arr) => {
  for (let i = arr.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = arr[i];
    // eslint-disable-next-line no-param-reassign
    arr[i] = arr[j];
    // eslint-disable-next-line no-param-reassign
    arr[j] = temp;
  }
  return arr;
};

export const triggerMulti = async (alarms, userInput) => {
  const stateUpdate = [];
  const randomAlarms = shuffleArray(
    alarms.filterByState('SECURE').slice()
  ).slice(0, userInput);

  randomAlarms.forEach((alarm) => {
    const { sensorName, alarmType } = alarm;
    stateUpdate.push({
      sensorName,
      alarmType,
      update: { state: { sensorReported: 'ALARM' } },
    });
  });

  const response = await axios.put(`/api/alarms/actions/evaluate`, stateUpdate);
  return response;
};

const clearAlarmFlow = async (sensorName, alarmType) => {
  const twinData = await getAlarmTwin(sensorName, alarmType);
  if (twinData.stateInfo.state === 'ALARM') {
    await bdocOperation(sensorName, alarmType, 'acknowledge');
  }

  if (twinData.stateInfo.state === 'UNKNOWN') {
    await bdocOperation(sensorName, alarmType, 'ack_unknown');
  }
  await sensorAlarmState(sensorName, alarmType, 'reset');
};

export const clearAlarms = async (alarms, state) => {
  const stateUpdate = [];
  if (state === 'All') {
    alarms.active.forEach(async (alarm) => {
      const { sensorName, alarmType } = alarm;
      await clearAlarmFlow(sensorName, alarmType);
      stateUpdate.push({
        sensorName,
        alarmType,
        update: { workflowRequested: 'SECURE' },
      });
      await bdocOperation(sensorName, alarmType, 'secure');
    });
    const response = await axios.put(
      '/api/alarms/actions/evaluate',
      stateUpdate
    );
    return response;
  }
  const alarmsToClear = alarms.filterByState(state.toUpperCase());
  alarmsToClear.forEach(async (alarm) => {
    const { sensorName, alarmType } = alarm;
    await clearAlarmFlow(sensorName, alarmType);
    stateUpdate.push({
      sensorName,
      alarmType,
      update: { workflowRequested: 'SECURE' },
    });
    await bdocOperation(sensorName, alarmType, 'secure');
  });
  const response = await axios.put('/api/alarms/actions/evaluate', stateUpdate);
  return response;
};
