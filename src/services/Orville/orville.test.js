import {
  clearAlarm,
  getAlarmTwin,
  sensorAlarmState,
  triggerMulti,
} from './orville';

describe('Orville Service Tests', () => {
  sensorAlarmState();
  getAlarmTwin();
  clearAlarm();
  triggerMulti();
  test('Should call utils without side effects', () => {});
});
