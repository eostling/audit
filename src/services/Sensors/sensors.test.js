import mockAxios from 'axios';
import { getSensors, updateSensors } from './sensors';

const mockResult = [];

describe('getSensors', () => {
  test('getSensors success', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getSensors({ page: 1, length: 10 });
    expect(result).toBe(mockResult);
    expect(mockAxios.get).toHaveBeenCalledWith(
      '/api/sensors?currentPage=1&pageLen=10'
    );
  });

  test('getSensors with sorting', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getSensors({
      page: 1,
      length: 10,
      sortField: 'sensorName',
      sortDirection: 'asc',
    });
    expect(result).toBe(mockResult);
    expect(mockAxios.get).toHaveBeenCalledWith(
      '/api/sensors?currentPage=1&pageLen=10&sortField=sensorName&sortDirection=asc'
    );
  });

  test('error if no page number', async () => {
    await expect(getSensors({ length: 10 })).rejects.toThrow();
  });

  test('error if no page size', async () => {
    await expect(getSensors({ page: 1 })).rejects.toThrow();
  });
});

describe('updateSensors', () => {
  test('updateSensors success', async () => {
    const mockPost = jest.fn(() => Promise.resolve(mockResult));
    mockAxios.post.mockImplementationOnce(mockPost);
    const sensors = [];
    const result = await updateSensors(sensors);
    expect(result).toBe(mockResult);
    expect(mockAxios.post).toHaveBeenCalledWith('/api/sensors', sensors);
  });
});
