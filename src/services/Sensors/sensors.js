import axios from 'axios';

// Get all sensors
export const getSensors = async ({
  page,
  length,
  sortField,
  sortDirection,
}) => {
  if (!page) {
    throw new Error('Page number not provided!');
  }

  if (!length) {
    throw new Error('Page length not provided!');
  }

  const { data } = await axios.get(
    `/api/sensors?currentPage=${page}&pageLen=${length}${
      sortField ? `&sortField=${sortField}` : ''
    }${sortDirection ? `&sortDirection=${sortDirection}` : ''}`
  );
  return data;
};

// Add sensors
export const addSensors = async (sensors) => {
  try {
    const response = await fetch('/api/sensors', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: sensors, // body data type must match "Content-Type" header
    });
    return response.json();
  } catch (err) {
    throw new Error(err);
  }
};

export const updateSensors = async (sensors) => {
  const result = await axios.post('/api/sensors', sensors);
  return result;
};
export const updateSensor = async (sensor) => {
  const result = await axios.put('/api/sensor', sensor[0]);
  return result;
};

export const deleteSensor = async (sensors) => {
  const result = await axios.delete('/api/sensors', {
    data: { sensors },
  });
  return result;
};
