import mockAxios from 'axios';
import { getAllFacilities } from './facilities';

const mockResult = [];

describe('Facilities Service', () => {
  test('can get all facilities', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: { facilities: mockResult } })
    );
    const result = await getAllFacilities();
    expect(result).toBe(mockResult);
  });
});
