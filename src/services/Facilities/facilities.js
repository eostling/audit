import axios from 'axios';

export const getAllFacilities = async () => {
  const { data } = await axios.get(`/api/facilities`);
  return data.facilities;
};

export const getActiveFacilities = async () => {
  const { data } = await axios.get(`/api/facilities?onlyWithSensors=true`);
  return data.facilities;
};

export const updateFacilities = async (facilities, newState) => {
  const urlState = newState.toLowerCase();

  const updateURL = `/api/${urlState}/`;

  try {
    const { data } = await axios.put(updateURL, {
      facilities,
    });
    return data;
  } catch (err) {
    throw new Error(err);
  }
};
