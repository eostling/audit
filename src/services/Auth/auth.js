import axios from 'axios';

const LOCALSTORAGE_KEY = 'BDOC_user';

export const getLocalUser = () => {
  return JSON.parse(window.localStorage.getItem(LOCALSTORAGE_KEY));
};

// Authenticate the user
export const loginUser = async (credentials) => {
  const {
    data: { user },
  } = await axios.post('/api/authenticate', credentials);
  const token = JSON.stringify(user);
  window.localStorage.setItem(LOCALSTORAGE_KEY, token);
  window.localStorage.setItem('modalSeen', true);
  return user;
};

// Log the user out
export const logoutUser = async () => {
  await axios.post('/api/logout');
  window.localStorage.removeItem(LOCALSTORAGE_KEY);
  window.localStorage.removeItem('modalSeen');
};

// Get refresh token
export const refreshUser = async () => {
  const { data } = await axios.post('/api/authenticate/refresh');
  return data;
};
