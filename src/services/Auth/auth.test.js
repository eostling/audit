import mockAxios from 'axios';

import { loginUser, logoutUser, refreshUser } from './auth';

const testUser = { user: 'pinky', role: 'operator' };

describe('Authentication Service', () => {
  test('should login user', async () => {
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({ data: testUser })
    );
    const result = await loginUser();
    expect(mockAxios.post).toHaveBeenCalled();
    expect(result).toEqual('pinky');
  });

  test('should logout user', async () => {
    mockAxios.post.mockImplementationOnce(() =>
      Promise.resolve({ data: null })
    );
    await logoutUser();
    expect(mockAxios.post).toHaveBeenCalled();
  });

  test('should refresh the user', async () => {
    await refreshUser();
    expect(mockAxios.post).toHaveBeenCalled();
  });
});
