import axios from 'axios';

const LOGIN_PATH = '/login';

export const initAxios = () => {
  axios.interceptors.response.use(
    function axiosRes(response) {
      return response;
    },
    function axiosErr(error) {
      const isLogin = window.location.pathname === LOGIN_PATH;
      if (error.response.status === 401 && !isLogin) {
        window.location = LOGIN_PATH;
      }
      return Promise.reject(error);
    }
  );
};
