import mockAxios from 'axios';
import { getAllZones } from './zones';

const mockResult = [];

describe('getAllZones', () => {
  test('getAllZones success', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: mockResult })
    );
    const result = await getAllZones();
    expect(result).toBe(mockResult);
    expect(mockAxios.get).toHaveBeenCalledWith('/api/zones');
  });
});
