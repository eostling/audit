import axios from 'axios';

export const getAllZones = async () => {
  const { data } = await axios.get(`/api/zones`);
  return data;
};
