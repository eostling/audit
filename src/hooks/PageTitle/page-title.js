/*
 * Borrowed from https://github.com/rehooks/document-title
 *
 * Allows title to be set via hook and if a component unmounts it will
 * fall back to the previous title
 *
 * Modified to use custom title formatting, default retain,
 * and strict hook deps
 * */

import React from 'react';

import * as packageJson from '../../../package.json';

export default function usePageTitle(pageTitle, defaultRetainOnUnmount = true) {
  const { name: appName } = packageJson.default;
  const formattedPageTitle = `${appName.toUpperCase()} - ${pageTitle}`;
  const retainOnUnmount = defaultRetainOnUnmount;

  const defaultTitle = React.useRef(document.title);

  React.useEffect(() => {
    document.title = formattedPageTitle;
  }, [formattedPageTitle]);

  React.useEffect(() => {
    const cleanupTitle = defaultTitle.current;
    return () => {
      if (!retainOnUnmount) {
        document.title = cleanupTitle;
      }
    };
  }, [retainOnUnmount]);
}
