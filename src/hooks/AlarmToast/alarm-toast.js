import { useSocket } from 'use-socketio';
// import useToast from './toast';

export default function useAlarmToast() {
  // export default function useAlarmToast(states = ['ALARM']) {
  // const toast = useToast();
  useSocket('alarm', () => {
    // useSocket('alarm', (newAlarms) => {
    /*
     * The alarm object over the websocket comes in with
     * a list of updated alarms.
     */
    // TEMP: Disable alarm toasts
    /* const alarms = newAlarms.filter((alarm) => states.includes(alarm.state));
    if (alarms.length) {
      let description = '';
      alarms.forEach((alarm) => {
        description = description.concat(
          `${alarm.sensorName} - ${alarm.alarmType} ${alarm.state}\n`
        );
      });
      toast({
        title: 'New Alarms',
        description,
        status: 'error',
      });
      const options = {
        body: description,
        icon: '',
        dir: 'ltr',
      };
      if (Notification.permission === 'granted') {
        const notification = new Notification('New Alarms', options);
        notification.close();
      }
    } */
    return true;
  });
}
