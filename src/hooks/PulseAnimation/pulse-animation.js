import { useState, useEffect } from 'react';

const FRAME_RATE_MS = 120;
const RADIUS_MIN = 5;
const RADIUS_MAX = 20;
const RADIUS_STEP = 1;
const OPACITY_MAX = 0.8;
const OPACITY_MIN = 0;

export default function usePulseAnimation() {
  const [radius, setRadius] = useState(RADIUS_MIN);
  const [opacity, setOpacity] = useState(OPACITY_MAX);

  useEffect(() => {
    const timer = setTimeout(() => {
      // Compute next radius frame
      const nextRadius = radius + RADIUS_STEP;
      setRadius(nextRadius > RADIUS_MAX ? RADIUS_MIN : nextRadius);

      // Compute next opacity frame; as radius grows, opacity fades
      const radiusPercent =
        (nextRadius - RADIUS_MIN) / (RADIUS_MAX - RADIUS_MIN);
      const nextOpacity = OPACITY_MAX * (1 - radiusPercent);
      setOpacity(nextOpacity < OPACITY_MIN ? OPACITY_MIN : nextOpacity);
    }, FRAME_RATE_MS);

    return () => clearTimeout(timer);
  }, [radius, opacity]);

  return {
    radius,
    opacity,
  };
}
