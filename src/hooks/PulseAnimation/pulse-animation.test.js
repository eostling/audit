import { renderHook, act } from '@testing-library/react-hooks';
import usePulseAnimation from './pulse-animation';

const RADIUS_MIN = 5;
const RADIUS_MAX = 20;
const RADIUS_STEP = 1;
const OPACITY_MAX = 0.8;

describe('Pulse Animation hook', () => {
  let pulse;
  beforeEach(() => {
    pulse = renderHook(() => {
      return usePulseAnimation();
    }).result;
    jest.useFakeTimers();
  });

  it('initializes radius/opacity', () => {
    expect(pulse.current.radius).toEqual(RADIUS_MIN);
    expect(pulse.current.opacity).toEqual(OPACITY_MAX);
  });

  it('changes radius', () => {
    act(() => {
      jest.runOnlyPendingTimers();
    });
    jest.clearAllTimers();
    expect(pulse.current.radius).toEqual(RADIUS_MIN + RADIUS_STEP);
  });

  it('has the right min/max values', () => {
    // run through a few cycles of the animation to confirm that the minimum/maximum values are correct
    const radii = [];
    const opacities = [];
    const numSteps = Math.ceil((RADIUS_MAX - RADIUS_MIN) / RADIUS_STEP) * 3;
    for (let i = 0; i < numSteps; i += 1) {
      act(() => {
        jest.runOnlyPendingTimers();
      });
      radii.push(pulse.current.radius);
      opacities.push(pulse.current.opacity);
    }
    jest.clearAllTimers();
    expect(Math.min(...radii)).toBeGreaterThanOrEqual(RADIUS_MIN);
    expect(Math.max(...radii)).toBeLessThanOrEqual(RADIUS_MAX);
    expect(Math.min(...opacities)).toBeGreaterThanOrEqual(0);
    expect(Math.max(...opacities)).toBeLessThanOrEqual(OPACITY_MAX);
  });
});
