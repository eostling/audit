import { useState, useEffect } from 'react';

export default function useCountdown(time) {
  const [value, setValue] = useState(time);

  useEffect(() => {
    const tick = () => {
      if (value > 0) {
        setValue(value - 1);
      }
    };
    const handle = setInterval(tick, 1000);
    // remove the timer when the hook is unmounted
    return () => {
      clearInterval(handle);
    };
  });
  return {
    value,
    setValue, // reset the timer by calling setValue
  };
}
