import { renderHook, act } from '@testing-library/react-hooks';
import useCountdown from './countdown';

describe('Countdown hook', () => {
  let countdownTest;
  beforeEach(() => {
    jest.useFakeTimers();
    countdownTest = renderHook(() => {
      return useCountdown(5);
    }).result;
  });
  test('changes values', async () => {
    expect(countdownTest.current.value).toBe(5);
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(countdownTest.current.value).toBe(4);
    act(() => {
      jest.advanceTimersByTime(3000);
    });
    expect(countdownTest.current.value).toBe(1);
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(countdownTest.current.value).toBe(0);
    // after it hits 0, it stops counting down
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(countdownTest.current.value).toBe(0);
    // can reset the countdown with setValue
    act(() => {
      countdownTest.current.setValue(3);
    });
    expect(countdownTest.current.value).toBe(3);
    // countdown still works after being reset
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(countdownTest.current.value).toBe(2);
  });
});
