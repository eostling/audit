import { useEffect, useRef, useState } from 'react';
import { useStore } from 'mobx-store-provider';
import { alarmsStoreId } from '../../models/Alarms/Alarms';
import { settingsStoreId } from '../../models/Settings/Settings';
import alarmSrc from '../../assets/alarm.wav';
import timeoutSrc from '../../assets/alarmTimeout.wav';

// Return functions to play an alarm, play a timeout, or stop all sounds
export default function useAlarmAudio(acknowledged) {
  const { hasALARM: isAlarm, hasTimeout: isTimeout } = useStore(alarmsStoreId);
  const alarms = useStore(alarmsStoreId);
  const settings = useStore(settingsStoreId);
  const [type, setType] = useState(null);
  const alarmInterval = useRef();
  const timeoutInterval = useRef();

  useEffect(() => {
    if (isTimeout) {
      setType('timeout');
      alarms.timedOutAlarms();
    } else if (isAlarm) {
      setType('alarm');
    } else {
      setType(null);
    }
  }, [isTimeout, isAlarm, alarms]);

  useEffect(() => {
    const ALARM_INTERVAL = settings.REACT_APP_ALARM_INTERVAL || 1;

    const alarmSound = new Audio(alarmSrc);
    const timeoutSound = new Audio(timeoutSrc);

    function stopAll() {
      alarmSound.pause();
      timeoutSound.pause();
      clearInterval(alarmInterval.current);
      clearInterval(timeoutInterval.current);
      alarmInterval.current = null;
      timeoutInterval.current = null;
    }

    function play(sound) {
      try {
        sound.play();
      } catch (error) {
        // Suppress error caused from sound playing before the user interacts with the DOM
      }
    }

    const playAlarm = () => {
      alarmInterval.current = setTimeout(() => {
        play(alarmSound);
        alarms.timedOutAlarms();
      }, ALARM_INTERVAL);
      play(alarmSound);
    };

    const playTimeout = () => {
      alarmSound.pause();
      clearInterval(alarmInterval.current);

      timeoutInterval.current = setInterval(() => {
        play(timeoutSound);
      }, ALARM_INTERVAL);
      play(timeoutSound);
    };

    if (type === 'alarm') {
      playAlarm();
    }
    if (type === 'timeout') {
      playTimeout();
    }
    if (!type) {
      stopAll();
    }

    // On teardown, stop all the sounds
    return () => {
      stopAll();
    };
  }, [type, settings.REACT_APP_ALARM_INTERVAL, alarms]);

  useEffect(() => {
    function play(sound) {
      try {
        sound.play();
      } catch (error) {
        // Suppress error caused from sound playing before the user interacts with the DOM
      }
    }

    const interval = setInterval(() => {
      const timeoutSound = new Audio(timeoutSrc);
      if (isAlarm && acknowledged) {
        alarmInterval.current = setTimeout(() => {
          play(timeoutSound);
          alarms.timedOutAlarms();
        }, settings.REACT_APP_ALARM_INTERVAL);
        play(timeoutSound);
      }
    }, settings.REACT_APP_ALARM_ESCALATION);
    return () => clearInterval(interval);
  }, [
    alarms,
    isAlarm,
    acknowledged,
    settings.REACT_APP_ALARM_ESCALATION,
    settings.REACT_APP_ALARM_INTERVAL,
  ]);
}
