import { useToast as chakraToast } from '@chakra-ui/react';
import titleCase from '../../util/Titlecase/titleCase';

const defaultProps = {
  title: '',
  description: '',
  status: 'success',
  duration: 9000,
  isClosable: true,
  position: 'top-left',
};

export default function useToast() {
  const toast = chakraToast();
  return ({ title, ...rest }) => {
    const fixedTitle = titleCase(title);
    return toast({ ...defaultProps, title: fixedTitle, ...rest });
  };
}
