import React from 'react';
import { Box, Heading, Stack } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import Layout from '../Layout';

export default function DefaultPageLayout({ title, children }) {

  return (
    <Layout>
      <Stack w='100vw' p={4} spacing={4}>
        <Heading>title</Heading>
        <Box flexShrink={1} overflow='auto' height='100vh'>
          {children}
        </Box>
      </Stack>
    </Layout>
  );
}

DefaultPageLayout.defaultProps = {
  title: '',
  children: {},
};

DefaultPageLayout.propTypes = {
  title: PropTypes.string,
  children: PropTypes.instanceOf(Object),
};
