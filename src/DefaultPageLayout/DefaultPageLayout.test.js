import { render, screen } from '@testing-library/react';
import DefaultPageLayout from './DefaultPageLayout';

jest.mock('../Layout', () => () => (
  <div data-testid='mocklayout'>MOCK Layout</div>
));

describe('<DefaultPageLayout />', () => {
  const mockTitle = 'Mock Title';
  const mockChildren = () => {
    return <>MOCK CHILDREN</>;
  };

  const setup = () => {
    // eslint-disable-next-line react/no-children-prop
    render(<DefaultPageLayout title={mockTitle} children={mockChildren} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('MOCK Layout')).toBeDefined();
  });
});
