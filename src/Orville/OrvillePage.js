/* istanbul ignore file */
import React from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';
import { Flex, Box, Heading, Text } from '@chakra-ui/react';
import { alarmsStoreId } from '../../../models/Alarms/Alarms';

import DefaultPageLayout from '../DefaultPageLayout/DefaultPageLayout';
import ClearAlarmsTool from './ClearAlarmsTool/ClearAlarmsTool';
import CurrentAlarmList from './CurrentAlarmList/CurrentAlarmList';
import InactiveAlarmsTool from './InactiveAlarmsTool/InactiveAlarmsTool';
import MultiAlarmTool from './MultiAlarmTool/MultiAlarmTool';

function Orville() {
  const alarms = useStore(alarmsStoreId);
  React.useLayoutEffect(() => {
    alarms.load();
  }, [alarms]);

  return (
    <DefaultPageLayout title='Orville Tools' minH='0'>
      <CurrentAlarmList alarms={alarms} />
      <InactiveAlarmsTool alarms={alarms} />
      <Box my={4}>
        <Heading size='md'>Multiple Alarm Tools</Heading>
        <Text>Trigger and Clear multiple Alarms at once</Text>
      </Box>
      <Flex justifyContent='flex-start'>
        <MultiAlarmTool alarms={alarms} maxAlarms={alarms.secure.length} />
        <ClearAlarmsTool alarms={alarms} />
      </Flex>
    </DefaultPageLayout>
  );
}

export default observer(Orville);
