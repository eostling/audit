import React from 'react';
import {
  Modal,
  ModalBody,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalHeader,
  Center,
  Text,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

export default function TwinDataModal({
  twinData,
  isOpen,
  onClose,
  sensorData,
  sensorString,
}) {
  const sensorName = sensorString
    ? sensorString.split(' ')[0]
    : sensorData.sensorName;

  return (
    <Center>
      <Modal isOpen={isOpen} onClose={onClose} size='xl'>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{sensorName} TWIN data</ModalHeader>
          <ModalCloseButton />
          <ModalBody mb={5}>
            <Text fontSize='xl' mb={5}>
              Current as of
              {new Date().toLocaleString('en-US', { hour12: false })}
            </Text>
            <Text as='pre' bgColor='bgColor' borderRadius='md' p={1}>
              {JSON.stringify(twinData, null, '\t')}
            </Text>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Center>
  );
}

TwinDataModal.defaultProps = {
  twinData: {},
  isOpen: false,
  onClose: () => undefined,
  sensorData: { sensorName: '' },
  sensorString: '',
};

TwinDataModal.propTypes = {
  twinData: PropTypes.instanceOf(Object),
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  sensorData: PropTypes.instanceOf(Object),
  sensorString: PropTypes.string,
};
