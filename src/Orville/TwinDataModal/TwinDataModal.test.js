import { render, screen } from '@testing-library/react';
import TwinDataModal from './TwinDataModal';

describe('<TwinDataModal />', () => {
  const setup = () => {
    render(<TwinDataModal isOpen onClose={jest.fn()} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText(/TWIN data/i)).toBeDefined();
  });
});
