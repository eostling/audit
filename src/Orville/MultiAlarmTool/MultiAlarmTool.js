import React from 'react';

import {
  Button,
  Flex,
  Heading,
  Box,
  HStack,
  FormControl,
  NumberInput,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  NumberInputField,
  FormLabel,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import useToast from '../../../../hooks/Toast/toast';

import { triggerMulti } from '../../../../services/Orville/orville';

function MultiAlarmTool({ alarms, maxAlarms }) {
  const toast = useToast();

  const [numAlarmsToTrigger, setNumAlarmsToTrigger] = React.useState('5');

  const handleNumAlarms = (valueString) => {
    setNumAlarmsToTrigger(parseInt(valueString));
  };

  const runMultiAlarms = (e) => {
    e.preventDefault();
    triggerMulti(alarms, numAlarmsToTrigger).then((res) => {
      if (res.status === 200) {
        alarms.load();
        return toast({
          title: 'Alarms Triggered',
          description: `Triggered ${numAlarmsToTrigger} random alarms`,
          status: 'info',
        });
      }
      return toast({
        title: 'Trigger Failed',
        description: `Failed to trigger ${numAlarmsToTrigger} random alarms`,
        status: 'error',
      });
    });
  };

  return (
    <Box mr='10em'>
      <Box my={4}>
        <Heading size='md'>Trigger Multiple Alarms</Heading>
      </Box>
      <Flex justifyContent='flex-start' mt='3em' mb={4}>
        <HStack alignItems='flex-end'>
          <FormControl>
            <FormLabel>Number of Alarms</FormLabel>
            <NumberInput
              value={numAlarmsToTrigger}
              onChange={handleNumAlarms}
              min={1}
              max={maxAlarms}
              w='20vw'
              bg='bgColor'
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </FormControl>
          <Button
            onClick={runMultiAlarms}
            disabled={Number.isNaN(numAlarmsToTrigger)}
            type='submit'
          >
            Trigger
          </Button>
        </HStack>
      </Flex>
    </Box>
  );
}

MultiAlarmTool.defaultProps = {
  alarms: {
    list: [],
    load: () => undefined,
  },
  maxAlarms: 5,
};

MultiAlarmTool.propTypes = {
  alarms: PropTypes.instanceOf(Object),
  maxAlarms: PropTypes.number,
};

export default observer(MultiAlarmTool);
