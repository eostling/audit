import { render, screen } from '@testing-library/react';
import MultiAlarmTool from './MultiAlarmTool';

describe('<MultiAlarmTool />', () => {
  const setup = () => {
    render(<MultiAlarmTool />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByLabelText('Number of Alarms')).toHaveValue('5');
  });
});
