import { render, screen } from '@testing-library/react';
import InactiveAlarmsTool from './InactiveAlarmsTool';

describe('<InactiveAlarmsTool />', () => {
  const setup = () => {
    render(<InactiveAlarmsTool />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByLabelText('Sensor')).toBeDefined();
  });
});
