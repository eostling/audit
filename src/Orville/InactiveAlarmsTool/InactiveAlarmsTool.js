import React from 'react';

import {
  Button,
  Flex,
  Select,
  Heading,
  Text,
  Box,
  HStack,
  useDisclosure,
  FormControl,
  FormLabel,
  Divider,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import useToast from '../../../../hooks/Toast/toast';
import TwinDataModal from '../TwinDataModal/TwinDataModal';

import {
  sensorAlarmState,
  getAlarmTwin,
} from '../../../../services/Orville/orville';

function InactiveAlarmsTool({ alarms }) {
  const toast = useToast();

  const [twinData, setTwinData] = React.useState();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [sensor, setSensor] = React.useState('');

  const handleSensor = (e) => {
    setSensor(e.target.value);
  };

  const makeToast = (toastAction, status, sensorName) => {
    const success = status === 'info' ? 'succeeded' : 'failed';
    return toast({
      title: `${toastAction} ${success}`,
      description: `${toastAction} on ${sensorName} ${success}`,
      status,
    });
  };

  const runSingleAlarm = (action) => {
    const sensorData = sensor.split(' ');
    const sensorName = sensorData
      .slice(0, sensorData.length - 1)
      .join(' ')
      .trim();
    const alarmType = sensorData[sensorData.length - 1];
    switch (action) {
      case 'Trigger':
      case 'Unknown':
        sensorAlarmState(sensorName, alarmType, action).then((res) => {
          if (res.status === 200) {
            alarms.load();
            setSensor('');
            makeToast(action, 'info', sensorName);
          } else {
            makeToast(action, 'error', sensorName);
          }
        });
        break;
      case 'Twin':
        getAlarmTwin(sensorName, alarmType).then((res) => {
          setTwinData(res);
          onOpen();
        });
        break;
      default:
        break;
    }
  };

  return (
    <>
      <Box my={4}>
        <Heading size='md'>Single Alarm Tools</Heading>
        <Text>Trigger, Set Unknown, or view Twin data for Inactive Alarms</Text>
      </Box>
      <HStack alignItems='flex-end' mt='3em' mb={4}>
        <Flex>
          <FormControl>
            <FormLabel>Sensor</FormLabel>
            <Select
              bg='bgColor'
              w='25vw'
              placeholder='Select Sensor'
              value={sensor}
              onChange={handleSensor}
              mr={5}
            >
              {alarms.inactive.map((alarm) => (
                <option key={alarm.alarmID}>
                  {alarm.sensorName} {alarm.alarmType}
                </option>
              ))}
            </Select>
          </FormControl>
        </Flex>
        <Box>
          <Button
            onClick={() => runSingleAlarm('Trigger')}
            mr={4}
            disabled={!sensor}
          >
            Trigger
          </Button>
          <Button
            onClick={() => runSingleAlarm('Unknown')}
            mr={4}
            disabled={!sensor}
          >
            Unknown
          </Button>
          <Button onClick={() => runSingleAlarm('Twin')} disabled={!sensor}>
            View Twin
          </Button>
        </Box>
      </HStack>
      <TwinDataModal
        isOpen={isOpen}
        onClose={onClose}
        twinData={twinData}
        sensorString={sensor}
      />
      <Divider />
    </>
  );
}

InactiveAlarmsTool.defaultProps = {
  alarms: {
    list: [],
    inactive: [],
  },
};

InactiveAlarmsTool.propTypes = {
  alarms: PropTypes.instanceOf(Object),
};

export default observer(InactiveAlarmsTool);
