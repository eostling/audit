import React from 'react';

import {
  Button,
  Flex,
  Select,
  Heading,
  Box,
  FormControl,
  HStack,
  FormLabel,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { clearAlarms } from '../../../../services/Orville/orville';
import useToast from '../../../../hooks/Toast/toast';

export default function ClearAlarmsTool({ alarms }) {
  const [alarmState, setAlarmState] = React.useState('');
  const toast = useToast();

  const handleAlarmState = (e) => {
    setAlarmState(e.target.value);
  };

  const clearAlarmsByState = (e) => {
    e.preventDefault();
    clearAlarms(alarms, alarmState).then((res) => {
      const description =
        alarmState === 'All'
          ? `Clearing ALL Alarms`
          : `Clearing Alarms in ${alarmState.toUpperCase()} state`;

      if (res.status === 200) {
        return toast({
          title: 'Clearing Alarms',
          description,
          status: 'info',
        });
      }
      return toast({
        title: 'No Alarms Cleared',
        description: `No alarms in ${alarmState.toUpperCase()} state`,
        status: 'info',
      });
    });
  };

  return (
    <Box>
      <Box my={4}>
        <Heading size='md'>Clear Alarms By State</Heading>
      </Box>
      <Flex justifyContent='flex-start' alignItems='flex-end' mt='3em' mb={4}>
        <HStack>
          <FormControl>
            <FormLabel>State</FormLabel>
            <Select
              bg='bgColor'
              w='20vw'
              placeholder='Select State'
              value={alarmState}
              onChange={handleAlarmState}
            >
              <option>All</option>
              <option>Alarm</option>
              <option>Unknown</option>
              <option>Acknowledged</option>
              <option>Ack_Unknown</option>
            </Select>
          </FormControl>
          <Button
            onClick={clearAlarmsByState}
            type='submit'
            disabled={!alarmState}
            alignSelf='flex-end'
          >
            Clear All
          </Button>
        </HStack>
      </Flex>
    </Box>
  );
}

ClearAlarmsTool.propTypes = {
  alarms: PropTypes.instanceOf(Object),
};

ClearAlarmsTool.defaultProps = {
  alarms: {},
};
