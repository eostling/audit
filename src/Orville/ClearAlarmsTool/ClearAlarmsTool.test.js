import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ClearAlarmsTool from './ClearAlarmsTool';

describe('<ClearAlarmsTool />', () => {
  const setup = () => {
    render(<ClearAlarmsTool />);
  };
  test('Should render without error', () => {
    setup();
    expect(screen.getByText('Clear All')).toBeDefined();
  });

  test('simulate selection', () => {
    setup();
    expect(screen.getByText('Clear All')).toBeDisabled();
    userEvent.selectOptions(screen.getByLabelText('State'), 'Alarm');
    expect(screen.getByText('Clear All')).toBeEnabled();
    expect(screen.getByText('Alarm').selected).toBeTruthy();
    expect(screen.getByText('Unknown').selected).toBeFalsy();
  });
});
