import React from 'react';

import { Box, Stack, Heading, Text, Flex } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import Table from '../../../Table/Table';
import CurrentAlarmListActionButtons from './CurrentAlarmListActionButtons/CurrentAlarmListActionButtons';
import EmptyQueueMessage from '../../Sidebar/Queue/EmptyQueueMessage/EmptyQueueMessage';

function CurrentAlarmList({ alarms }) {
  const columns = [
    {
      label: 'Sensor',
      accessor: 'sensorName',
    },
    {
      label: 'Alarm Type',
      accessor: 'alarmType',
    },
    {
      label: 'Current State',
      accessor: 'state',
    },
  ];

  return (
    <>
      <Flex justifyContent='space-between' mb={4}>
        <Box>
          <Heading size='md'>Current Alarms</Heading>
          <Text>
            Reset, Clear, Set Unknown, and view Twin data for active Alarms
          </Text>
        </Box>
        <Text pr='15px'>
          {alarms.notSecure.length} current{' '}
          {alarms.notSecure.length !== 1 ? 'alarms' : 'alarm'}
        </Text>
      </Flex>
      {alarms.notSecure.length > 0 ? (
        <Stack maxHeight='400px' overflowY='auto' pr='15px'>
          <Box>
            <Table
              items={alarms.notSecure}
              columns={columns}
              idAccessor='alarmID'
              rowActions={(rowData) => (
                <CurrentAlarmListActionButtons
                  alarms={alarms}
                  sensorData={rowData}
                />
              )}
              isOrville
            />
          </Box>
        </Stack>
      ) : (
        <EmptyQueueMessage
          queueType='alarm'
          itemSubtype='current alarms'
          pr='15px'
        />
      )}
    </>
  );
}

CurrentAlarmList.defaultProps = {
  alarms: {
    list: [],
    notSecure: [],
  },
};

CurrentAlarmList.propTypes = {
  alarms: PropTypes.instanceOf(Object),
};

export default observer(CurrentAlarmList);
