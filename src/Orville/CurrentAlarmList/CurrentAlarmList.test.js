import { render, screen } from '@testing-library/react';
import CurrentAlarmList from './CurrentAlarmList';

jest.mock(
  './CurrentAlarmListActionButtons/CurrentAlarmListActionButtons',
  () => () =>
    (
      <div data-testid='mockCurrentAlarmActionButtons'>
        MOCK CURRENT ALARM ACTION BUTTONS
      </div>
    )
);

describe('<CurrentAlarmList />', () => {
  const alarmALARM = {
    label: 'Test Alarm - INTRUSION',
    lat: 0,
    lon: 10,
    state: 'ALARM',
    alarmTime: 3600000,
    priority: 1,
    facilityID: 'Test Bldg',
    floor: 1,
    validNextStates: ['SECURE'],
    additionalAlarmDetails: {},
  };

  const mockAlarms = {
    notSecure: [alarmALARM],
  };

  const setup = () => {
    render(<CurrentAlarmList alarms={mockAlarms} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getAllByText(/Current Alarms/i)).toBeDefined();
    expect(screen.getByText('MOCK CURRENT ALARM ACTION BUTTONS')).toBeDefined();
  });
});
