import { render, screen } from '@testing-library/react';
import CurrentAlarmListActionButtons from './CurrentAlarmListActionButtons';

describe('<CurrentAlarmListActionButtons />', () => {
  const setup = () => {
    render(<CurrentAlarmListActionButtons />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText(/Twin/i)).toBeDefined();
  });
});
