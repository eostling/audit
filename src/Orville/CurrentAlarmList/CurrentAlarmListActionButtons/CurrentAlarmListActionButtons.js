import React from 'react';
import { Button, useDisclosure, HStack } from '@chakra-ui/react';
import PropTypes from 'prop-types';

import {
  sensorAlarmState,
  clearAlarm,
  getAlarmTwin,
} from '../../../../../services/Orville/orville';
import useToast from '../../../../../hooks/Toast/toast';
import TwinDataModal from '../../TwinDataModal/TwinDataModal';

export default function CurrentAlarmListActionButtons({ sensorData, alarms }) {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [twinData, setTwinData] = React.useState();

  const makeToast = (toastAction, status, sensorName) => {
    const success = status === 'info' ? 'succeeded' : 'failed';
    return toast({
      title: `${toastAction} ${success}`,
      description: `${toastAction} on ${sensorName} ${success}`,
      status,
    });
  };

  const runSingleAlarm = (action) => {
    const { sensorName } = sensorData;
    const { alarmType } = sensorData;
    switch (action) {
      case 'Reset':
      case 'Unknown':
        sensorAlarmState(sensorName, alarmType, action).then((res) => {
          if (res.status === 200) {
            if (action === 'Unknown') alarms.load();
            makeToast(action, 'info', sensorName);
          } else {
            makeToast(action, 'error', sensorName);
          }
        });
        break;
      case 'Clear':
        clearAlarm(sensorName, alarmType).then((res) => {
          if (res[0].statusCode === 200) {
            alarms.load();
            makeToast(action, 'info', sensorName);
          } else {
            makeToast(action, 'error', sensorName);
          }
        });
        break;
      case 'Twin':
        getAlarmTwin(sensorName, alarmType).then((res) => {
          setTwinData(res);
          onOpen();
        });
        break;
      default:
        break;
    }
  };

  return (
    <HStack>
      <Button onClick={() => runSingleAlarm('Reset')} type='submit'>
        Reset
      </Button>
      <Button onClick={() => runSingleAlarm('Clear')} type='submit'>
        Clear
      </Button>
      <Button
        onClick={() => runSingleAlarm('Unknown')}
        type='submit'
        disabled={sensorData.state !== 'ACCESS'}
      >
        Unknown
      </Button>
      <Button onClick={() => runSingleAlarm('Twin')} type='submit'>
        Twin
      </Button>
      <TwinDataModal
        isOpen={isOpen}
        onClose={onClose}
        twinData={twinData}
        sensorData={sensorData}
      />
    </HStack>
  );
}

CurrentAlarmListActionButtons.defaultProps = {
  sensorData: { state: '' },
  alarms: {
    load: () => undefined,
  },
};

CurrentAlarmListActionButtons.propTypes = {
  sensorData: PropTypes.instanceOf(Object),
  alarms: PropTypes.instanceOf(Object),
};
