import React from 'react';
import { Box, Tooltip } from '@chakra-ui/react';

export default function IconButtonWithTooltip() {
  return (
    <Tooltip placement='center'>
      <Box cursor='not-allowed'>MOCKED ICON BUTTON</Box>
    </Tooltip>
  );
}
