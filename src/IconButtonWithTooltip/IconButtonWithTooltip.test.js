import { render, screen } from '@testing-library/react';
import IconButtonWithTooltip from './IconButtonWithTooltip';

describe('<ButtonWithTooltip />', () => {
  const mockLabel = 'MockLabel';
  const mockPlacement = 'MockPlacement';
  const mockDisabled = false;
  const mockLoading = false;
  const mockTooltipProps = {};
  const mockRest = {};
  const setup = () => {
    render(
      <IconButtonWithTooltip
        label={mockLabel}
        placement={mockPlacement}
        disabled={mockDisabled}
        tooltipProps={mockTooltipProps}
        loading={mockLoading}
        rest={mockRest}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByRole('button')).toBeDefined();
  });
});
