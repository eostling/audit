import React from 'react';
import { Box, IconButton, Tooltip } from '@chakra-ui/react';
import PropTypes from 'prop-types';

export default function IconButtonWithTooltip({
  label,
  placement,
  disabled,
  tooltipProp,
  ariaLabel,
  colorScheme,
  color,
  icon,
  mr,
  onClick,
  p,
  rest,
  size,
  style,
  variant,
  loading,
}) {
  if (color) {
    return (
      <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
        <Box cursor={disabled ? 'not-allowed' : 'inherit'}>
          <IconButton
            disabled={disabled}
            pointerEvents={disabled ? 'none' : 'inherit'}
            icon={icon}
            aria-label={ariaLabel}
            colorScheme={colorScheme}
            color={color}
            mr={mr}
            onClick={onClick}
            p={p}
            rest={rest}
            size={size}
            style={style}
            variant={variant}
            isLoading={loading}
          />
        </Box>
      </Tooltip>
    );
  }
  return (
    <Tooltip label={label} placement={placement} tooltipProp={tooltipProp}>
      <Box d='inline' cursor={disabled ? 'not-allowed' : 'inherit'}>
        <IconButton
          disabled={disabled}
          pointerEvents={disabled ? 'none' : 'inherit'}
          icon={icon}
          aria-label={ariaLabel}
          mr={mr}
          onClick={onClick}
          p={p}
          rest={rest}
          size={size}
          style={style}
          variant={variant}
          isLoading={loading}
        />
      </Box>
    </Tooltip>
  );
}

IconButtonWithTooltip.defaultProps = {
  label: '',
  placement: '',
  disabled: false,
  tooltipProp: {},
  ariaLabel: '',
  colorScheme: null,
  color: null,
  icon: {},
  mr: null,
  onClick: () => {},
  p: null,
  rest: null,
  size: null,
  style: null,
  variant: null,
  loading: false,
};

IconButtonWithTooltip.propTypes = {
  label: PropTypes.string,
  placement: PropTypes.string,
  disabled: PropTypes.bool,
  tooltipProp: PropTypes.instanceOf(Object),
  ariaLabel: PropTypes.string,
  colorScheme: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.element,
  mr: PropTypes.number,
  onClick: PropTypes.func,
  p: PropTypes.number,
  rest: PropTypes.instanceOf(Object),
  size: PropTypes.string,
  style: PropTypes.instanceOf(Object),
  variant: PropTypes.string,
  loading: PropTypes.bool,
};
