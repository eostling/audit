import React from 'react';
import { Table as ChakraTable, Thead, Tbody } from '@chakra-ui/react';

function Table() {
  return (
    <ChakraTable data-testid='table'>
      <Thead>MOCKED TABLE HEADER</Thead>
      <Tbody>MOCKED TABLE</Tbody>
    </ChakraTable>
  );
}

export default Table;
