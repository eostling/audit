import React from 'react';
import { observer } from 'mobx-react';
import {
  Box,
  Table as ChakraTable,
  Thead,
  Tbody,
  Text,
  Tr,
  Th,
  Td,
  Tooltip,
  Checkbox,
  Badge,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { CaretDownIcon, CaretUpIcon } from '../Icons';

function Table({
  items,
  columns,
  sorting = {
    field: undefined,
    direction: undefined,
    changeSortField: undefined,
    toggleSortDirection: undefined,
  },
  idAccessor,
  select = false,
  onSelectAll = () => undefined,
  onDeselectAll = () => undefined,
  isAllSelected = false,
  isIntermediateSelected = false,
  rowActions,
  isOrville,
}) {
  const rowPad = '0.5em';
  const oddRowBg = 'rgba(255,255,255,0.01)';
  const evenRowBg = 'rgba(255,255,255,0.05)';

  const {
    field: sortField,
    direction: sortDirection,
    changeSortField,
    toggleSortDirection,
  } = sorting;

  const selectedCheckBox = [];

  const isChecked =
    items.length > 0 && selectedCheckBox.length === items.length;

  const handleHeadingClick = (isSortable, field) => {
    if (!isSortable) return false;
    if (sortField === field) {
      return toggleSortDirection();
    }
    return changeSortField(field);
  };

  const selectAllHandler = (event) => {
    if (event.target.checked) {
      onSelectAll();
    } else {
      onDeselectAll();
    }
  };

  const getBadgeColor = (state) => {
    switch (state) {
      case 'ALARM':
        return 'red';
      case 'ACCESS':
        return 'yellow';
      case 'UNKNOWN':
      case 'ACK_UNKNOWN':
        return 'gray';
      default:
        return null;
    }
  };

  return (
    <ChakraTable data-testid='table'>
      <Thead>
        <Tr backgroundColor='bgColor'>
          {select && (
            <Th width='2em'>
              <Tooltip
                placement='top'
                label={`${isChecked ? `'Unselect All'` : 'Selected all'}`}
              >
                <Box>
                  <Checkbox
                    isChecked={isAllSelected}
                    isIndeterminate={isIntermediateSelected}
                    onChange={selectAllHandler}
                  />
                </Box>
              </Tooltip>
            </Th>
          )}
          {columns.map((column, idx) => {
            const isFirstCol = idx === 0;
            const isLastCol = idx === columns.length - 1;
            const isCurrentSortField = column.accessor === sorting.field;
            const isSortable = column.sortable || false;
            const isAscending = sortDirection === 'asc';

            return (
              <Th
                key={`col-head-${column.label}`}
                paddingLeft={isFirstCol ? rowPad : null}
                paddingRight={isLastCol ? rowPad : null}
                paddingBottom={rowPad}
                textAlign={column.alignment || 'left'}
                cursor={isSortable ? 'pointer' : 'default'}
                top='0'
                width={column.width}
                position='sticky'
                backgroundColor='bgColor'
                shadow='md'
                zIndex='1'
                onClick={() => handleHeadingClick(isSortable, column.accessor)}
              >
                {isSortable ? (
                  <Tooltip
                    label={`Sort by ${column.label} ${
                      isCurrentSortField
                        ? `(${isAscending ? 'Descending' : 'Ascending'})`
                        : ''
                    }`}
                    placement='top'
                  >
                    <div>
                      {column.label}
                      {isCurrentSortField && (
                        <>
                          {sortDirection === 'asc' ? (
                            <CaretUpIcon />
                          ) : (
                            <CaretDownIcon />
                          )}
                        </>
                      )}
                    </div>
                  </Tooltip>
                ) : (
                  <>{column.label}</>
                )}
              </Th>
            );
          })}
          {!!rowActions && (
            <Th
              paddingBottom={rowPad}
              textAlign={isOrville ? 'center' : 'right'}
              width='12em'
              top='0'
              position='sticky'
              backgroundColor='bgColor'
              shadow='md'
              zIndex='1'
            >
              Actions
            </Th>
          )}
        </Tr>
      </Thead>
      <Tbody>
        {items.map((item, rIdx) => {
          const rowBg = rIdx % 2 === 0 ? evenRowBg : oddRowBg;
          return (
            <Tr
              key={item[idAccessor]}
              backgroundColor={rowBg}
              _hover={{ backgroundColor: 'blue.800' }}
            >
              {select && (
                <Td>
                  <Checkbox
                    isChecked={item.isSelected}
                    onChange={() => item.toggle()}
                  />
                </Td>
              )}
              {columns.map((column, cIdx) => {
                const isFirstCol = cIdx === 0;
                const isLastCol = cIdx === columns.length - 1;
                return (
                  <Td
                    key={`data-${item[idAccessor]}-${column.label}`}
                    title={item[column.titleAccessor] || ''}
                    style={{
                      paddingLeft: isFirstCol ? rowPad : null,
                      paddingRight: isLastCol ? rowPad : null,
                      paddingTop: rowPad,
                      paddingBottom: rowPad,
                      textAlign: column.alignment || 'left',
                    }}
                  >
                    {column.label === 'Current State' ? (
                      <Badge colorScheme={getBadgeColor(item[column.accessor])}>
                        {item[column.accessor]}
                      </Badge>
                    ) : (
                      <Text>{item[column.accessor]}</Text>
                    )}
                  </Td>
                );
              })}
              {!!rowActions && (
                <Td title='actions' textAlign='right'>
                  {rowActions(item)}
                </Td>
              )}
            </Tr>
          );
        })}
      </Tbody>
    </ChakraTable>
  );
}

Table.defaultProps = {
  items: [],
  columns: [
    {
      label: 'Facility No.',
      accessor: 'facilityID',
      sort: true,
    },
    {
      label: 'Name',
      accessor: 'displayName',
      sort: true,
    },
    { label: 'Zone', accessor: 'zoneID', alignment: 'center', sort: true },
    { label: 'State', accessor: 'state', sort: true },
  ],
  sorting: {},
  idAccessor: 'default',
  select: false,
  onSelectAll: () => undefined,
  onDeselectAll: () => undefined,
  isAllSelected: false,
  isIntermediateSelected: false,
  rowActions: () => undefined,
  isOrville: false,
};

Table.propTypes = {
  items: PropTypes.instanceOf(Array),
  columns: PropTypes.instanceOf(Array),
  sorting: PropTypes.instanceOf(Object),
  idAccessor: PropTypes.string,
  select: PropTypes.bool,
  onSelectAll: PropTypes.func,
  onDeselectAll: PropTypes.func,
  isAllSelected: PropTypes.bool,
  isIntermediateSelected: PropTypes.bool,
  rowActions: PropTypes.func,
  isOrville: PropTypes.bool,
};

export default observer(Table);
