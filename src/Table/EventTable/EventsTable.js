import React from 'react';

import {
  Box,
  Button,
  Code,
  IconButton,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalFooter,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import DataTable, { createTheme } from 'react-data-table-component';
import PropTypes from 'prop-types';

createTheme('bdoc', {
  text: {
    primary: '#FFFFFF',
    secondary: 'rgba(255, 255, 255, 0.7)',
    disabled: 'rgba(0,0,0,.12)',
  },
  background: {
    default: '#1A202C',
  },
  context: {
    background: '#E91E63',
    text: '#FFFFFF',
  },
  divider: {
    default: 'rgba(81, 81, 81, 1)',
  },
  button: {
    default: '#FFFFFF',
    focus: 'rgba(255, 255, 255, .54)',
    hover: 'rgba(255, 255, 255, .12)',
    disabled: 'rgba(255, 255, 255, .18)',
  },
  sortFocus: {
    default: 'rgba(255, 255, 255, .54)',
  },
  selected: {
    default: 'rgba(0, 0, 0, .7)',
    text: '#FFFFFF',
  },
  highlightOnHover: {
    default: 'rgba(0, 0, 0, .7)',
    text: '#FFFFFF',
  },
  striped: {
    default: 'rgba(0, 0, 0, .87)',
    text: '#FFFFFF',
  },
});

const EventsTable = ({ events }) => {
  // eslint-disable-next-line react/prop-types
  function ModalButton({ event }) {
    const { isOpen, onOpen, onClose } = useDisclosure();

    const handleOpen = () => {
      onOpen();
    };

    return (
      <>
        <IconButton icon='json' onClick={handleOpen} />
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            {/* eslint-disable-next-line react/prop-types */}
            <ModalHeader>{`Event Id: ${event.messageID}`}</ModalHeader>
            <ModalBody>
              <Text as='h4' mb={2}>
                Message:
              </Text>
              {/* eslint-disable-next-line react/prop-types */}
              <Code wordBreak='break-word' p={2}>{`${event.message}`}</Code>
            </ModalBody>
            <ModalFooter>
              <Button onClick={onClose}>Done</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  }

  const columns = [
    {
      name: 'Message Received At',
      selector: (data) => Number.parseFloat(data.received),
      sortable: 'true',
    },
    {
      name: 'Message Type',
      selector: 'messageType',
    },
    {
      name: 'View Message',
      selector: (data) => <ModalButton event={data} />,
    },
  ];

  return (
    <Box>
      <DataTable
        height='10em'
        title={`Showing ${events.countAll} events`}
        columns={columns}
        data={events.all}
        theme='bdoc'
        defaultSortField='received'
        fixedHeader
        pagination
      />
    </Box>
  );
};

EventsTable.defaultProps = {
  events: {
    countAll: 0,
    all: 0,
  },
};

EventsTable.propTypes = {
  events: PropTypes.instanceOf(Object),
};

export default EventsTable;
