import { render, screen } from '@testing-library/react';
import StyleProvider from '../../Providers/StyleProvider/StyleProvider';
import PrivateStoreProvider from '../../Providers/PrivateStoreProvider/PrivateStoreProvider';
import EventsTable from './EventsTable';

describe('<EventsTable />', () => {
  const events = {};
  const setup = () => {
    render(
      <StyleProvider>
        <PrivateStoreProvider>
          <EventsTable events={events} />
        </PrivateStoreProvider>
      </StyleProvider>
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('There are no records to display')).toBeDefined();
  });
});
