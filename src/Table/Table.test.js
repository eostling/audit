import { render, screen } from '@testing-library/react';
import Table from './Table';

describe('<Table />', () => {
  const mockItems = [];
  const mockColumns = [{ MockItem: 'MockItem' }];
  const mockSorting = {};
  const mockIdAccessor = {};

  const setup = () => {
    render(
      <Table
        items={mockItems}
        columns={mockColumns}
        sorting={mockSorting}
        idAccessor={mockIdAccessor}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByTestId('table')).toBeDefined();
  });
});
