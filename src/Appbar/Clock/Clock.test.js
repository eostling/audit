import {
  render,
  screen,
} from '../../../util/Testing/CustomRender/TestingUtils';

import Clock from './Clock';

describe('Clock Component', () => {
  const setup = () => {
    render(<Clock />);
  };

  test('renders date and time', () => {
    setup();
    // Regex Format: word, word ##, #### ##:## AAA
    expect(
      screen.getByText(/^\d{1,2}\/\d{1,2}\/\d{4} \d{1,2}:\d{1,2}:\d{1,2}/i)
    ).toBeDefined();
  });

  test('matches snapshot', () => {
    setup();
    expect(screen).toMatchSnapshot();
  });
});
