import React, { useLayoutEffect, useState } from 'react';
import { DateTime } from 'luxon';

import { Box, Text } from '@chakra-ui/react';

function getNow() {
  return DateTime.now().toFormat('M/d/yyyy HH:mm:ss');
}

const Clock = () => {
  const [now, setNow] = useState(getNow());

  useLayoutEffect(() => {
    const initialTimeout = 60 - DateTime.now().toFormat('ss');

    setTimeout(() => {
      setNow(getNow());
      setInterval(() => setNow(getNow()), 10);
    }, initialTimeout);
  }, []);

  return (
    <Box>
      <Text fontSize='md'>{now}</Text>
    </Box>
  );
};

export default Clock;
