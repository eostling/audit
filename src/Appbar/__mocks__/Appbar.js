import React from 'react';
import { Flex, Heading } from '@chakra-ui/react';

export default function Appbar() {
  return (
    <Flex
      as='nav'
      bgGradient='linear(135deg, gray.600, gray.700)'
      alignItems='center'
      justifyContent='space-between'
    >
      <Flex alignItems='center'>
        <Flex alignItems='flex-end'>
          <Heading
            fontSize='2em'
            letterSpacing='0.15em'
            textTransform='uppercase'
          >
            MOCKED APP NAME
          </Heading>
        </Flex>
      </Flex>
    </Flex>
  );
}
