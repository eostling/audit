import React from 'react';
import {
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuList,
  MenuItem,
  Tooltip,
} from '@chakra-ui/react';

export default function UserMenu() {
  const handleLogout = () => {};

  const onOpen = () => {};
  return (
    <Menu>
      <Tooltip label='User Menu' placement='bottom'>
        <MenuButton as={Button} variant='ghost'>
          MOCKED USER MEU
        </MenuButton>
      </Tooltip>
      <MenuList placement='bottom-end' zIndex='popover'>
        <MenuItem onClick={onOpen}>MOCKED User Settings</MenuItem>
        <MenuDivider />
        <MenuItem onClick={handleLogout}>MOCKED Sign Out</MenuItem>
      </MenuList>
    </Menu>
  );
}
