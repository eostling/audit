import { render, screen } from '@testing-library/react';
import UserAgreement from './UserAgreement';

describe('<UserAgreement />', () => {
  const setup = () => {
    render(<UserAgreement />);
  };

  test('Should render without error', () => {
    setup();
    expect(
      screen.getByText(/U.S. Government Information System User Agreement/i)
    ).toBeDefined();
  });
});
