import React, { useRef } from 'react';

import {
  Button,
  Divider,
  Flex,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Stack,
  Tag,
  Wrap,
  WrapItem,
  Avatar,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

// TODO: Rename to UserProfile?
export default function SecureDialog({ isOpen, onClose, user }) {
  const initialFocus = useRef();
  const RED_PERM = ['DEVELOPMENT'];
  const YLW_PERM = ['ACCESS'];
  const BLUE_PERM = [
    'CONTACT_MANAGEMENT',
    'AUDIT',
    'FACILITY_MANAGEMENT',
    'SENSOR_MANAGEMENT',
    'SETTINGS_MANAGEMENT',
    'USER_MANAGEMENT',
  ];

  function getColor(permission) {
    if (RED_PERM.includes(permission)) {
      return 'red';
    }
    if (YLW_PERM.includes(permission)) {
      return 'yellow';
    }
    if (BLUE_PERM.includes(permission)) {
      return 'blue';
    }
    return 'gray';
  }

  return (
    <>
      <Modal initialFocusRef={initialFocus} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            <Avatar name={user.username} size='sm' mr={2} />
            {user.username}
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Stack>
              <Flex color='white' fontWeight='bold'>
                User Permissions
              </Flex>
            </Stack>
            <Divider m={2} />
            <Stack>
              <Wrap>
                {user.permissions.map((permission) => {
                  return (
                    <WrapItem key={`item-${permission}`}>
                      <Tag
                        colorScheme={getColor(permission)}
                        key={permission}
                        data-testid='permissions'
                      >
                        {permission}
                      </Tag>
                    </WrapItem>
                  );
                })}
              </Wrap>
            </Stack>
          </ModalBody>
          <ModalFooter>
            <Button mr={3} onClick={onClose}>
              Done
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

SecureDialog.defaultProps = {
  isOpen: false,
  onClose: () => undefined,
  user: null,
};

SecureDialog.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  user: PropTypes.instanceOf(Object),
};
