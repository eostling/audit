import React from 'react';
import { Button } from '@chakra-ui/react';

const SecureDialog = () => {
  const onClose = () => {};
  return (
    <>
      Mocked Secure dialog
      <Button onClick={onClose}>Mocked Done</Button>
    </>
  );
};

export default SecureDialog;
