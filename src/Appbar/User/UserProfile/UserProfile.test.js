import { render, screen, fireEvent } from '@testing-library/react';
import SecureDialog from './UserProfile';
import StoreProvider from '../../../Providers/StoreProvider/StoreProvider';
import StyleProvider from '../../../Providers/StyleProvider/StyleProvider';

describe('<SecureDialog />', () => {
  const mockIsOpen = true;
  const mockOnClose = jest.fn();
  const mockUser = {
    username: 'brain',
    role: 'admin',
    permissions: ['all'],
  };
  const setup = () => {
    render(
      <StyleProvider>
        <StoreProvider>
          <SecureDialog
            isOpen={mockIsOpen}
            onClose={mockOnClose}
            user={mockUser}
          />
        </StoreProvider>
      </StyleProvider>
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText(/Done/i)).toBeDefined();
  });

  test('Should get colors without error', () => {
    setup();
    const getColor = screen.getByTestId('permissions');
    fireEvent.click(getColor);
  });
});
