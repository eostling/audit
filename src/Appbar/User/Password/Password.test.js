import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import PasswordInput from './PasswordInput';

describe('<PasswordInput />', () => {
  const mockForwardRef = React.createRef();
  const setup = () => {
    render(<PasswordInput forwardRef={mockForwardRef} isDisabled={{}} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByTestId('password')).toBeDefined();
  });

  test('should set show', () => {
    setup();
    const show = screen.getByTestId('password');
    fireEvent.click(show);
  });
});
