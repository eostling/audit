import React from 'react';

import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  IconButton,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { HideIcon, LockIcon, ShowIcon } from '../../../Icons';

const PasswordInput = ({ forwardRef, isDisabled, name, placeholder, rest }) => {
  const [show, setShow] = React.useState(false);
  return (
    <InputGroup name='password'>
      <InputLeftElement>
        <LockIcon />
      </InputLeftElement>
      <Input
        name={name}
        placeholder={placeholder}
        rest={rest}
        ref={forwardRef}
        type={show ? 'text' : 'password'}
        isDisabled={isDisabled}
      />
      <InputRightElement>
        <IconButton
          variant='link'
          icon={show ? <HideIcon /> : <ShowIcon />}
          arial-label={`${show ? 'Hide' : 'Show'} password`}
          onClick={() => setShow(!show)}
          isDisabled={isDisabled}
          data-testid='password'
        />
      </InputRightElement>
    </InputGroup>
  );
};

PasswordInput.defaultProps = {
  forwardRef: {},
  isDisabled: false,
  name: '',
  placeholder: '',
  rest: {},
};

PasswordInput.propTypes = {
  forwardRef: PropTypes.instanceOf(Object),
  isDisabled: PropTypes.bool,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  rest: PropTypes.instanceOf(Object),
};

export default PasswordInput;
