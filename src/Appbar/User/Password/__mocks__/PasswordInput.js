import React from 'react';
import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';

const PasswordInput = () => {
  return (
    <InputGroup name='password'>
      <InputLeftElement />
      <Input
        name='name'
        placeholder='Password Mocked Component'
        type='Mocked password'
        isDisabled={false}
      />
    </InputGroup>
  );
};

export default PasswordInput;
