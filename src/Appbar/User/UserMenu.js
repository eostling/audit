import React from 'react';
import { useStore } from 'mobx-store-provider';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuList,
  MenuItem,
  Tooltip,
  useDisclosure,
  Avatar,
} from '@chakra-ui/react';

import { ChevronDownIcon, GearIcon, LogoutIcon } from '../../Icons';

export default function UserMenu() {
  
  const history = useHistory();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleLogout = async () => {
    try {
      // user.logout();
      history.push('/login');
    } catch (e) {
      // Replace with loggly
      throw new Error(`Failed to logout with error: ${e.message}`);
    }
  };

  return (
    <Menu>
      <Tooltip label='User Menu' placement='bottom'>
        <MenuButton
          as={Button}
          leftIcon={<Avatar size='sm' />}
          rightIcon={<ChevronDownIcon />}
          variant='ghost'
        >
         username
        </MenuButton>
      </Tooltip>
      <MenuList placement='bottom-end' zIndex='popover'>
        <MenuItem onClick={onOpen}>
          <GearIcon mr={2} />
          User Settings
        </MenuItem>
        <MenuDivider />
        <MenuItem onClick={handleLogout}>
          <LogoutIcon mr={2} />
          Sign Out
        </MenuItem>
      </MenuList>
      {/*<UserProfile isOpen={isOpen} onClose={onClose} />*/}
    </Menu>
  );
}
