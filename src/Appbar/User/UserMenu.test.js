import {
  render,
  screen,
} from '../../../util/Testing/CustomRender/TestingUtils';
import UserMenu from './UserMenu';

jest.mock('./UserProfile/UserProfile');

describe('<UserMenu />', () => {
  const setup = () => {
    render(<UserMenu />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('User Settings')).toBeDefined();
  });
});
