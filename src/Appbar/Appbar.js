import React from 'react';
import { Flex, Heading, HStack } from '@chakra-ui/react';
import * as packageJson from '../../package.json';

import { AirForceIcon } from '../Icons';
import Clock from './Clock/Clock';
import UserMenu from './User/UserMenu';
import Version from './Version/Version';

export default function Appbar() {
  const { name: appName } = packageJson.default;
  return (
    <Flex
      as='nav'
      bgGradient='linear(135deg, gray.600, gray.700)'
      alignItems='center'
      justifyContent='space-between'
    >
      <Flex alignItems='center'>
        <AirForceIcon ml={3} mr={7} h='2.5em' w='2.5em' />
        <Flex alignItems='flex-end'>
          <Heading
            fontSize='2em'
            letterSpacing='0.15em'
            textTransform='uppercase'
          >
            {appName}
          </Heading>
          <Version />
        </Flex>
      </Flex>
      <HStack spacing={2} alignItems='center' p={2}>
        <Clock />
        <UserMenu />
      </HStack>
    </Flex>
  );
}
