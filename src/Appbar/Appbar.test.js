import { render, screen } from '../../util/Testing/CustomRender/TestingUtils';

import * as packageJson from '../../../package.json';
import Appbar from './Appbar';

jest.mock('./User/UserMenu');
jest.mock('./Clock/Clock');
jest.mock('./Version/Version');

describe('Appbar Component', () => {
  const { name: appName } = packageJson.default;
  const match = new RegExp(appName, 'i');

  const setup = () => {
    render(<Appbar />);
  };

  test('renders without error', () => {
    setup();
    expect(screen.getByText(match)).toBeDefined();
  });
});
