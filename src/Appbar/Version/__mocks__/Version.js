import React from 'react';
import { Box, Text } from '@chakra-ui/react';

export default function Version() {
  return (
    <Box title='Version MOCKED'>
      <Text color='gray.400' fontSize='xs' p='.2em'>
        vMOCKED
      </Text>
    </Box>
  );
}
