import React from 'react';
import { Box, Text } from '@chakra-ui/react';
import * as packageJson from '../../../package.json';

export default function Version() {
  const {
    default: { version },
  } = packageJson;
  return (
    <Box title={`Version ${version}`}>
      <Text color='gray.400' fontSize='xs' p='.2em'>{`v${version}`}</Text>
    </Box>
  );
}
