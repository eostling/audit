import {
  render,
  screen,
} from '../../../util/Testing/CustomRender/TestingUtils';
import StyleProvider from '../../Providers/StyleProvider/StyleProvider';
import Version from './Version';

describe('Version Component', () => {
  const setup = () => {
    render(
      <StyleProvider>
        <Version />
      </StyleProvider>
    );
  };

  test('renders version number', () => {
    setup();
    expect(screen.getByText(/\d+\.\d+\.\d+/i)).toBeDefined();
  });
});
