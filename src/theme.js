import { extendTheme, theme as defaultTheme } from '@chakra-ui/react';

const { blue, gray, green, red, yellow } = defaultTheme.colors;

// Theme configuration overrides
const config = {
  initialColorMode: 'dark',
  useSystemColorMode: false,
};

// Custom theme colors
const colors = {
  activeNav: blue[200],
  badgeColor: red[600],
  lightText: gray[100],
  darkText: gray[700],
  bgColor: gray[800],
  cardHeaderBg: gray[700],
  cardContentBg: gray[900],
  toolbarIcon: gray[800],
  toolbarIconActive: blue[500],
  blueForce: {
    normal: {
      fill: blue[600],
      border: blue[200],
    },
    distressed: {
      fill: red[600],
      border: red[200],
    },
  },
  states: {
    ACCESS: yellow[400],
    ACKNOWLEDGED: red[400],
    ACK_UNKNOWN: gray[200],
    ALARM: red[400],
    SECURE: green[600],
    UNKNOWN: gray[200],
  },
  choice: {
    accept: green[600],
    reject: red[400],
  },
  mapLayers: {
    active: blue,
  },
};

// Custom component style variants
const components = {
  Button: {
    variants: {
      'map-control-active': {
        background: blue[200],
        color: gray[900],
      },
      'map-control-inactive': {
        background: gray[400],
        color: gray[600],
      },
      'map-control': {
        background: gray[200],
        color: gray[900],
      },
    },
  },
};

// Custom styles
const styles = {
  global: {
    '.mapboxgl-popup-content': {
      background: gray[700],
      overflowY: 'auto',
      maxHeight: '40vh',
    },
    '.mapboxgl-popup-anchor-bottom .mapboxgl-popup-tip': {
      borderTopColor: gray[700],
    },
    '.mapboxgl-popup-anchor-bottom-left .mapboxgl-popup-tip': {
      borderTopColor: gray[700],
    },
    '.mapboxgl-popup-anchor-bottom-right .mapboxgl-popup-tip': {
      borderTopColor: gray[700],
    },
    '.mapboxgl-popup-anchor-top .mapboxgl-popup-tip': {
      borderBottomColor: gray[700],
    },
    '.mapboxgl-popup-anchor-top-left .mapboxgl-popup-tip': {
      borderBottomColor: gray[700],
    },
    '.mapboxgl-popup-anchor-top-right .mapboxgl-popup-tip': {
      borderBottomColor: gray[700],
    },
    '#chakra-toast-manager-top-left': {
      margin: '1em',
      transform: 'translate(5.5em, 3em)',
    },
    "input[type='date']::-webkit-calendar-picker-indicator": {
      filter: 'invert(1)',
    },
  },
};

const theme = extendTheme({ config, colors, components, styles });

export default theme;
