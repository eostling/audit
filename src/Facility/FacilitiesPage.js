import React from 'react';
import { useProvider, useCreateStore } from 'mobx-store-provider';
import Facilities, {
  initialFacilities,
  facilitiesStoreId,
} from '../../../models/Facilities/Facilities';

import DefaultPageLayout from '../DefaultPageLayout/DefaultPageLayout';
import FacilitiesList from './FacilitiesList/FacilitiesList';

export default function FacilitiesPage() {
  const FacilitiesProvider = useProvider(facilitiesStoreId);
  const facilities = useCreateStore(() => Facilities.create(initialFacilities));

  React.useLayoutEffect(() => {
    facilities.load();
  }, [facilities]);

  return (
    <DefaultPageLayout title='Facility Management'>
      <FacilitiesProvider value={facilities}>
        <FacilitiesList />
      </FacilitiesProvider>
    </DefaultPageLayout>
  );
}
