import renderer from 'react-test-renderer';
import StyleProvider from '../../../Providers/StyleProvider/StyleProvider';
import StoreProvider from '../../../Providers/StoreProvider/StoreProvider';
import PrivateStoreProvider from '../../../Providers/PrivateStoreProvider/PrivateStoreProvider';
import FacilityCard from './FacilityCard';

describe('Facility Card component', () => {
  const facility = {
    facilityID: '1600',
    displayName: 'Bathroom',
    lat: 10,
    lon: 15,
    numFloors: 2,
    zoneID: 6,
    state: 'UNKNOWN',
  };

  test('matches snapshot', () => {
    const tree = renderer.create(
      <StyleProvider>
        <StoreProvider>
          <PrivateStoreProvider>
            <FacilityCard facility={facility} />
          </PrivateStoreProvider>
        </StoreProvider>
      </StyleProvider>
    );

    expect(tree).toMatchSnapshot();
  });
});
