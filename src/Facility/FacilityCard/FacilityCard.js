import React from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';
import { Flex, Button, Stack, Tooltip } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { mapStoreId } from '../../../../models/Map/Map';
import { FacilitiesIcon } from '../../../Icons';

const FacilityCard = ({ facility }) => {
  const { lat, lon, displayName, facilityID, state } = facility;
  const { viewport } = useStore(mapStoreId);

  return (
    <Flex
      bg='gray.700'
      w='100%'
      flexDirection='column'
      borderRadius={2}
      overflow='hidden'
    >
      <Flex p={2} alignItems='center' justify='space-between'>
        <Stack spacing={2} direction='row'>
          <Tooltip label={`Facility state: ${state}`} placement='left'>
            <FacilitiesIcon color={`states.${state}`} />
          </Tooltip>
          <Tooltip label='Center on map' placement='top'>
            <Button
              maxWidth='430px'
              textAlign='left'
              overflow='hidden'
              textOverflow='ellipsis'
              display='block'
              whiteSpace='nowrap'
              variant='link'
              fontSize='.8em'
              onClick={() => {
                viewport.flyTo({ lat, lon, zoom: 18 });
              }}
            >
              {`Bldg. ${facilityID} - ${displayName}`}
            </Button>
          </Tooltip>
        </Stack>
      </Flex>
    </Flex>
  );
};

FacilityCard.defaultProps = {
  facility: {
    lat: 0,
    lon: 0,
    displayName: '',
    facilityID: 0,
    zoneID: 0,
    state: {},
  },
};

FacilityCard.propTypes = {
  facility: PropTypes.instanceOf(Object),
};

export default observer(FacilityCard);
