import React from 'react';
import { Stack } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import IconButtonWithTooltip from '../../../IconButtonWithTooltip/IconButtonWithTooltip';
import { AlarmAccessIcon, AlarmSecureIcon } from '../../../Icons';

export default function FacilityActionButtons({ justify, facility }) {
  return (
    <Stack direction='row' justify={justify}>
      <IconButtonWithTooltip
        icon={<AlarmAccessIcon />}
        label='Put Facility in ACCESS'
        placement='left'
        disabled={!facility.canAccess}
        onClick={facility.access}
        color='states.ACCESS'
        size='sm'
      />
      <IconButtonWithTooltip
        icon={<AlarmSecureIcon />}
        label='Put Facility in SECURE'
        placement='left'
        disabled={!facility.canSecure}
        onClick={facility.secure}
        color='states.SECURE'
        size='sm'
      />
    </Stack>
  );
}

FacilityActionButtons.defaultProps = {
  justify: '',
  facility: {
    canSecure: false,
    secure: false,
  },
};

FacilityActionButtons.propTypes = {
  justify: PropTypes.string,
  facility: PropTypes.instanceOf(Object),
};
