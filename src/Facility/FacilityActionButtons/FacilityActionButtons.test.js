import { render, screen } from '@testing-library/react';
import FacilityActionButtons from './FacilityActionButtons';

jest.mock('../../../IconButtonWithTooltip/IconButtonWithTooltip', () => () => (
  <div data-testid='mockICONS'>MOCK ICONS</div>
));

describe('<FacilityActionButtons />', () => {
  const justify = 'center';
  const facility = {};
  const setup = () => {
    render(<FacilityActionButtons justify={justify} facility={facility} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getAllByText('MOCK ICONS')).toBeDefined();
  });
});
