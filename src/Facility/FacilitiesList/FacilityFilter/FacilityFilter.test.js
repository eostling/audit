import {
  render,
  screen,
  fireEvent,
} from '../../../../../util/Testing/CustomRender/TestingUtils';
import FacilityFilter from './FacilityFilter';

describe('<FacilityFilter />', () => {
  const currentFilter = {};
  const filterChange = jest.fn();

  const setup = () => {
    render(
      <FacilityFilter
        currentFilter={currentFilter}
        filterChange={filterChange}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    fireEvent.click(screen.getByText('ACCESS'));
    expect(screen.getByText('ALL')).toBeDefined();
  });
});
