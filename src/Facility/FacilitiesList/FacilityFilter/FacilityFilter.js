import React from 'react';

import {
  Flex,
  HStack,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Select,
  Tooltip,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { CloseCircleIcon, FilterIcon, SearchIcon } from '../../../../Icons';

const FacilityFilter = ({
  stateFilter,
  onStateFilterChange,
  textFilter,
  onTextFilterChange,
}) => {
  const handleStateFilterChange = (evt) => {
    onStateFilterChange(evt.target.value);
  };

  const handleTextFilterChange = (evt) => {
    onTextFilterChange(evt.target.value);
  };

  const handleClearTextFilter = () => {
    onTextFilterChange('');
  };

  return (
    <Flex>
      <HStack width='100%' spacing={2}>
        <Tooltip label='Filter by facility name or bldg. no.' placement='top'>
          <InputGroup>
            <InputLeftElement pointerEvents='none'>
              <SearchIcon color='gray.300' />
            </InputLeftElement>
            <Input
              type='text'
              placeholder='Search Facilities'
              value={textFilter}
              onChange={handleTextFilterChange}
            />
            <InputRightElement>
              {textFilter !== '' && (
                <IconButton
                  aria-label='Clear search text'
                  variant='ghost'
                  size='xs'
                  icon={<CloseCircleIcon />}
                  onClick={handleClearTextFilter}
                />
              )}
            </InputRightElement>
          </InputGroup>
        </Tooltip>
        <Tooltip label='Filter by facility state' placement='top'>
          <Select
            bg='bgColor'
            color='lightText'
            icon={<FilterIcon />}
            onChange={handleStateFilterChange}
            value={stateFilter}
          >
            <option>ACTIVE</option>
            <option>ALL</option>
            <option>ACCESS</option>
            <option>ACKNOWLEDGED</option>
            <option>ALARM</option>
            <option>SECURE</option>
            <option>UNKNOWN</option>
          </Select>
        </Tooltip>
      </HStack>
    </Flex>
  );
};

FacilityFilter.defaultProps = {
  stateFilter: 'ACTIVE',
  onStateFilterChange: () => undefined,
  textFilter: '',
  onTextFilterChange: () => undefined,
};

FacilityFilter.propTypes = {
  stateFilter: PropTypes.string,
  onStateFilterChange: PropTypes.func,
  textFilter: PropTypes.string,
  onTextFilterChange: PropTypes.func,
};

export default FacilityFilter;
