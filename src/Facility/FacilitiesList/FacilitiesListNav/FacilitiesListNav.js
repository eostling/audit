import React from 'react';
import { observer } from 'mobx-react';

import { Stack, Text } from '@chakra-ui/react';
import PropTypes from 'prop-types';

function FacilitiesListNav({ length }) {
  return (
    <Stack justifyContent='space-between' isInline>
      <Stack alignItems='center' isInline>
        <Text>{`Displaying ${length} ${
          length === 1 ? 'Facility' : 'Facilities'
        }`}</Text>
      </Stack>
    </Stack>
  );
}

FacilitiesListNav.defaultProps = {
  length: 1,
};

FacilitiesListNav.propTypes = {
  length: PropTypes.number,
};

export default observer(FacilitiesListNav);
