import {
  render,
  screen,
} from '../../../../../util/Testing/CustomRender/TestingUtils';
import FacilitiesListNav from './FacilitiesListNav';

describe('<FacilitiesListNav />', () => {
  const facilities = {};
  const searched = {};
  const filtered = {};
  const length = 10;
  const offset = 1;
  const perPage = 1;
  const current = 1;
  const changeCurrent = jest.fn();

  const setup = () => {
    render(
      <FacilitiesListNav
        current={current}
        changeCurrent={changeCurrent}
        facilities={facilities}
        searched={searched}
        length={length}
        offset={offset}
        perPage={perPage}
        filtered={filtered}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('Displaying 10 Facilities')).toBeDefined();
  });
});
