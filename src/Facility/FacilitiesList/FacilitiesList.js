import React, { useState } from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';

import { Box, Stack, Flex, useDisclosure } from '@chakra-ui/react';
import Table from '../../../Table/Table';
import { facilitiesStoreId } from '../../../../models/Facilities/Facilities';
import FacilitiesListNav from './FacilitiesListNav/FacilitiesListNav';
import FacilityDialog from './FacilityDialog/FacilityDialog';
import FacilityFilter from './FacilityFilter/FacilityFilter';

function FacilitiesList() {
  const facilities = useStore(facilitiesStoreId);
  const activeFacilities = facilities.activeSortedById;
  const allFacilities = facilities.sortedById;
  const [textFilter, setTextFilter] = useState('');
  const [stateFilter, setStateFilter] = useState('ACTIVE');

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [facilityID, setFacilityID] = useState('');

  const openFacilityDialog = (facility) => {
    setFacilityID(facility.facilityID);
    onOpen();
  };

  const handleStateFilterChange = (val) => {
    setStateFilter(val);
  };

  const handleTextFilterChange = (newText) => {
    setTextFilter(newText);
  };

  const searchFacilities = (arr) => {
    const searched = arr.filter((facility) => {
      return (
        facility.facilityID.includes(textFilter) ||
        facility.displayName.toLowerCase().includes(textFilter.toLowerCase())
      );
    });
    return searched;
  };

  const filterFacilities = () => {
    const facilitiesToSearch =
      stateFilter === 'ALL' || stateFilter === 'UNKNOWN'
        ? allFacilities
        : activeFacilities;
    const searchedFacilities = textFilter
      ? searchFacilities(facilitiesToSearch)
      : facilitiesToSearch;

    const filteredFacilities = searchedFacilities.filter((facility) => {
      if (stateFilter === 'ALL' || stateFilter === 'ACTIVE') return facility;
      return facility.state === stateFilter;
    });

    return filteredFacilities;
  };

  const columns = [
    {
      label: 'Facility No.',
      accessor: 'facilityID',
      onClick: openFacilityDialog,
    },
    {
      label: 'Name',
      accessor: 'displayName',
      onClick: openFacilityDialog,
    },
    { label: 'Zone', accessor: 'zoneID', alignment: 'center' },
    { label: 'State', accessor: 'state' },
  ];

  return (
    <Stack height='100%' overflow='hidden'>
      <FacilityDialog
        isOpen={isOpen}
        onClose={onClose}
        facilityID={facilityID}
      />
      <Flex alignItems='center' justifyContent='space-between'>
        <Flex>
          <FacilityFilter
            textFilter={textFilter}
            onTextFilterChange={handleTextFilterChange}
            stateFilter={stateFilter}
            onStateFilterChange={handleStateFilterChange}
          />
        </Flex>
        <Flex>
          <FacilitiesListNav length={filterFacilities().length} />
        </Flex>
      </Flex>
      <Box overflow='auto'>
        <Table
          items={filterFacilities()}
          columns={columns}
          idAccessor='facilityID'
        />
      </Box>
    </Stack>
  );
}

export default observer(FacilitiesList);
