import React from 'react';
import { useStore } from 'mobx-store-provider';
import {
  Box,
  Flex,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Stack,
  Text,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';

import AlarmCard from '../../../Alarms/AlarmCard/AlarmCard';
import { alarmsStoreId } from '../../../../../models/Alarms/Alarms';
import { facilitiesStoreId } from '../../../../../models/Facilities/Facilities';

export default function FacilityDialog({ isOpen, onClose, facilityID }) {
  const alarms = useStore(alarmsStoreId);
  const facilities = useStore(facilitiesStoreId);
  const fac = facilities.details(facilityID);
  const facAlarms = alarms.filterByFacilityID(facilityID);

  return (
    <>
      <Modal
        size='xl'
        isOpen={isOpen}
        onClose={onClose}
        scrollBehavior='inside'
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color='white'>Facility Information</ModalHeader>
          <ModalCloseButton color='white' />
          <ModalBody>
            {fac && (
              <Stack color='white'>
                <Box>Facility ID: {fac.facilityID}</Box>
                <Box>Facility Name: {fac.displayName}</Box>
                <Box>Facility Address: {fac.streetAddress}</Box>
                {facAlarms.length ? (
                  <>
                    <Heading size='sm'>Alarms</Heading>
                    <Stack
                      bg='gray.900'
                      overflowY='auto'
                      padding='5px'
                      spacing={2}
                      flexGrow='1'
                    >
                      {facAlarms.map((alarm) => {
                        return (
                          <Flex key={alarm.alarmID}>
                            <AlarmCard
                              isChecked={false}
                              onCheck={undefined}
                              alarm={alarm}
                            />
                          </Flex>
                        );
                      })}
                    </Stack>
                  </>
                ) : (
                  <Text>There are no configured alarms for this facility.</Text>
                )}
              </Stack>
            )}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

FacilityDialog.defaultProps = {
  isOpen: false,
  onClose: () => undefined,
  facilityID: '',
};

FacilityDialog.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  facilityID: PropTypes.string,
};
