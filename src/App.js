import './App.css';
import AuditPage from '../src/Audit/AuditPage/AuditPage'

function App() {
  return (
    <div className="App">
     <AuditPage/>
    </div>
  );
}

export default App;
