import React from 'react';

// Components
import ListNav from '../../../../../ListNav/ListNav';

export function AuditListNav() {
  return (
    <ListNav
      currentPage={1}
      currentSize={100}
      disablePrev={false}
      disableNext={false}
      handlePrev={() => {}}
      handleNext={() => {}}
      handleChangeSize={() => {}}
      itemCount={100}
      pageCount={10}
      total={100}
      label='audit records'
    />
  );
}

export default AuditListNav;
