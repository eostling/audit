import { render, screen } from '@testing-library/react';
import AuditListNav from './AuditListNav';

jest.mock('../../../../ListNav/ListNav', () => () => (
  <div data-testid='listNav'>ListNav</div>
));
describe('<AuditListNav />', () => {
  const mockRecords = {
    countAll: 3,
    pages: 10,
    total: 100,
  };
  const setup = () => {
    render(<AuditListNav records={mockRecords} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('ListNav')).toBeDefined();
  });
});
