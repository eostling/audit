import React from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

// Components
import ListNav from '../../../ListNav/ListNav';

export function AuditListNav({ records }) {
  const { countAll, pages, total } = records;
  return (
    <ListNav
      currentPage={pages.currentPage}
      currentSize={pages.size}
      disablePrev={!pages.hasPrev}
      disableNext={!pages.hasNext}
      handlePrev={pages.prev}
      handleNext={pages.next}
      handleChangeSize={pages.changeSize}
      itemCount={countAll}
      pageCount={pages.pageCount}
      total={total}
      label='audit records'
    />
  );
}

AuditListNav.defaultProps = {
  records: {
    countAll: 0,
    pages: {
      currentPage: 0,
      size: 0,
      hasPrev: false,
      prev: () => undefined,
      next: () => undefined,
      changeSize: () => undefined,
      pageCount: 0,
    },
  },
};

AuditListNav.propTypes = {
  records: PropTypes.instanceOf(Object),
};

export default observer(AuditListNav);
