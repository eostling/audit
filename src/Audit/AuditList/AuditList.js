import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex, HStack, Stack, useDisclosure } from '@chakra-ui/react';

import { ExportIcon } from '../../Icons';
import AuditListNav from './AuditListNav/AuditListNav';
import AuditMessageButton from '../AuditMessageButton';
import ExportAuditRecordsModal from '../ExportAuditRecordsModal/ExportAuditRecordsModal';
import IconButtonWithTooltip from '../../IconButtonWithTooltip/IconButtonWithTooltip';
import Table from '../../Table/Table';

export default function AuditList({ records }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const columns = [
    {
      label: 'Time',
      accessor: 'timestamp',
    },
    {
      label: 'Event',
      accessor: 'eventType',
    },
    {
      label: 'Sensor',
      accessor: 'sensorName',
    },
    {
      label: 'Alarm Type',
      accessor: 'alarmType',
    },
    {
      label: 'State Change',
      accessor: 'stateInputUpdate',
    },
    {
      label: 'User',
      accessor: 'username',
    },
  ];

  return (
    <Stack height='100%' overflow='hidden'>
      <Flex justifyContent='space-between'>
        <HStack spacing={2}>
          <IconButtonWithTooltip
            icon={<ExportIcon />}
            label='Export Audit Records'
            onClick={onOpen}
          />
          <ExportAuditRecordsModal
            records={records}
            isOpen={isOpen}
            onClose={onClose}
          />
        </HStack>

        <AuditListNav records={records} />
      </Flex>
      <Box overflow='auto'>
        <Table
          items={records.list}
          columns={columns}
          idAccessor='eventID'
          rowActions={(rowData) => <AuditMessageButton record={rowData} />}
        />
      </Box>
    </Stack>
  );
}

AuditList.defaultProps = {
  records: {
    list: [],
  },
};

AuditList.propTypes = {
  records: PropTypes.instanceOf(Object),
};
