import { render, screen } from '@testing-library/react';
import AuditList from './AuditList';

jest.mock('../../../Table/Table');

jest.mock('./AuditListNav/AuditListNav');

jest.mock('../../../IconButtonWithTooltip/IconButtonWithTooltip');

jest.mock('../ExportAuditRecordsModal/ExportAuditRecordsModal');

describe('<AuditList />', () => {
  const mockRecords = {
    countAll: 3,
    pages: 10,
    total: 100,
  };
  const setup = () => {
    render(<AuditList records={mockRecords} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('MOCKED TABLE')).toBeDefined();
  });
});
