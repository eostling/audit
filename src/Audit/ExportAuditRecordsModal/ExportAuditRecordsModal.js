import React, { useState } from 'react';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  ModalHeader,
  Text,
  Flex,
  Stack,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import DatePicker from '../../Pickers/DatePicker/DatePicker';
import TimePicker from '../../Pickers/TimePicker/TimePicker';
import { ExportAuditRecordsButton } from '../ExportAuditRecordsButton/ExportAuditRecordsButton';

export default function ExportAuditRecordsModal({ isOpen, onClose }) {
  const [startDate, setStartDate] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endDate, setEndDate] = useState('');
  const [endTime, setEndTime] = useState('');

  const handleStartDate = (e) => {
    setStartDate(e.target.value);
  };

  const handleStartTime = (e) => {
    setStartTime(e.target.value);
  };

  const handleEndDate = (e) => {
    setEndDate(e.target.value);
  };

  const handleEndTime = (e) => {
    setEndTime(e.target.value);
  };

  const close = () => {
    setStartDate('');
    setEndDate('');
    setEndTime('');
    setStartTime('');
    onClose();
  };

  const makeDateTime = (date, time, range) => {
    if (!date && !time) {
      const dateTime = range === 'start' ? '1/1/1970' : '1/1/2099';
      return new Date(dateTime).getTime();
    }
    return new Date(date.concat(' ', time)).getTime();
  };

  return (
    <Modal isOpen={isOpen} onClose={close} size='xl'>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Export Audit Records</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack alignItems='center' mb={4}>
            <Flex>
              <DatePicker handleChange={handleStartDate} />
              <TimePicker handleChange={handleStartTime} />
            </Flex>
            <Flex>
              <Text>to</Text>
            </Flex>
            <Flex>
              <DatePicker handleChange={handleEndDate} />
              <TimePicker handleChange={handleEndTime} />
            </Flex>
          </Stack>
          <Flex justifyContent='flex-end'>
            <ExportAuditRecordsButton
              exportAll={!startDate && !endDate}
              start={makeDateTime(startDate, startTime, 'start')}
              end={makeDateTime(endDate, endTime, 'end')}
              onClose={onClose}
            />
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}

ExportAuditRecordsModal.defaultProps = {
  isOpen: false,
  onClose: () => undefined,
};

ExportAuditRecordsModal.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
};
