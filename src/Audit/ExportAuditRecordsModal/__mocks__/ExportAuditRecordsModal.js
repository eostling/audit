import React from 'react';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  ModalHeader,
  Stack,
} from '@chakra-ui/react';

export default function ExportAuditRecordsModal() {
  const close = () => {};

  return (
    <Modal isOpen onClose={close} size='xl'>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>MOCKED EXPORT AUDIT MODAL HEADER</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack alignItems='center' mb={4}>
            MOCKED EXPORT AUDIT MODAL
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
