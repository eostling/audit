import { render, screen } from '@testing-library/react';
import AuditMessageButton from './AuditMessageButton';

jest.mock('../../IconButtonWithTooltip/IconButtonWithTooltip');

describe('<AuditMessageButton />', () => {
  const mockRecords = {
    eventID: 1,
    message: 'MOCK MESSAGE',
  };
  const setup = () => {
    render(<AuditMessageButton record={mockRecords} />);
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByText('MOCKED ICON BUTTON')).toBeDefined();
  });
});
