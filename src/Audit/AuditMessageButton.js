import React from 'react';
import {
  Button,
  Code,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalFooter,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import PropTypes from 'prop-types';
import IconButtonWithTooltip from '../IconButtonWithTooltip/IconButtonWithTooltip';
import { JsonIcon } from '../Icons';

export default function AuditMessageButton({ record }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const handleOpen = () => {
    onOpen();
  };
  return (
    <>
      <IconButtonWithTooltip
        icon={<JsonIcon />}
        label='View message'
        placement='left'
        onClick={handleOpen}
      />
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{`Event Id: ${record.eventID}`}</ModalHeader>
          <ModalBody>
            <Text as='h4' mb={2}>
              Message:
            </Text>
            <Code wordBreak='break-word' p={2}>{`${record.message}`}</Code>
          </ModalBody>
          <ModalFooter>
            <Button onClick={onClose}>Done</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

AuditMessageButton.defaultProps = {
  record: {
    message: '',
    eventID: 1,
  },
};

AuditMessageButton.propTypes = {
  record: PropTypes.instanceOf(Object),
};
