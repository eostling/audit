import React, { useRef, useState } from 'react';
import { CSVLink } from 'react-csv';
import { Button, useToast } from '@chakra-ui/react';
import PropTypes from 'prop-types';
import { getAuditRecordsByDate } from '../.././services/Audit/audit';
import { formatDateShort, formatTime24 } from '../../util';

export const ExportAuditRecordsButton = ({
  start,
  end,
  onClose,
  exportAll,
}) => {
  const [dataForDownload, setDataForDownload] = useState([]);
  const csvLink = useRef();
  const toast = useToast();
  const showToast = (props) => {
    toast(props);
    return props;
  };

  const getRecords = async () => {
    if (start > end) {
      return showToast({
        title: 'Please enter a valid date range',
        description: `There are no records in selected date range`,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }

    try {
      await getAuditRecordsByDate(start, end).then((res) => {
        if (res.records.length === 0) {
          return showToast({
            title: 'No Records to Export',
            description: `There are no records in selected date range`,
            status: 'info',
            duration: 9000,
            isClosable: true,
          });
        }
        // convert timestamps in records
        const records = res.records.map((rec) => {
          const timestamp = Number(rec.time);
          const updatedTwinRecordString =
            '`' +
            JSON.stringify(rec.updatedTwinRecord).replaceAll('"', '') +
            '`';
          const previousTwinRecordString =
            '`' +
            JSON.stringify(rec.previousTwinRecord).replaceAll('"', '') +
            '`';
          return {
            ...rec,
            time: `${formatDateShort(timestamp)} ${formatTime24(timestamp)}`,
            updatedTwinRecord: updatedTwinRecordString,
            previousTwinRecord: previousTwinRecordString,
          };
        });

        setDataForDownload(records);
        onClose();
        return csvLink.current.link.click();
      });
      return null;
    } catch (error) {
      return showToast({
        title: 'Export Failed',
        description: error.message,
        status: 'error',
        duration: 9000,
        isClosable: true,
      });
    }
  };

  const formatDate = () => {
    if (exportAll) return 'ALL';
    if (start && end) {
      const startDateString = new Date(start).toISOString().split('.')[0];
      const endDateString = new Date(end).toISOString().split('.')[0];
      return `${startDateString}_${endDateString}`;
    }
    return null;
  };

  return (
    <>
      <Button data-testid='getRecords' onClick={getRecords}>
        {exportAll ? 'Export All' : 'Export Selected Records'}
      </Button>
      <CSVLink
        data={dataForDownload}
        filename={`AuditRecords_${formatDate()}.csv`}
        className='hidden'
        ref={csvLink}
        target='_blank'
      />
    </>
  );
};

ExportAuditRecordsButton.defaultProps = {
  start: 1,
  end: 100,
  onClose: () => undefined,
  exportAll: false,
};

ExportAuditRecordsButton.propTypes = {
  start: PropTypes.number,
  end: PropTypes.number,
  onClose: PropTypes.func,
  exportAll: PropTypes.bool,
};
