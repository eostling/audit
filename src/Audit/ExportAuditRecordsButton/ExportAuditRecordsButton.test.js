import { render, screen, fireEvent } from '@testing-library/react';
import { ExportAuditRecordsButton } from './ExportAuditRecordsButton';

describe('<ExportAuditRecordsButton />', () => {
  const mockStart = jest.fn();
  const mockEnd = jest.fn();
  const mockOnClose = jest.fn();
  const mockExportAll = jest.fn();
  const setup = () => {
    render(
      <ExportAuditRecordsButton
        end={mockEnd}
        onClose={mockOnClose}
        exportAll={mockExportAll}
        start={mockStart}
      />
    );
  };

  test('Should render without error', () => {
    setup();
    expect(screen.getByRole('button')).toBeDefined();
  });

  test('Should render without error', () => {
    setup();
    const records = screen.getByTestId('getRecords');
    fireEvent.click(records);
  });
});
