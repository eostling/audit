import React from 'react';
import { observer } from 'mobx-react';
import { useStore } from 'mobx-store-provider';
import { Text } from '@chakra-ui/react';
import { auditStoreId } from '../../models/Audit/Audit';

import DefaultPageLayout from '../../DefaultPageLayout/DefaultPageLayout';
import AuditList from '../AuditList/AuditList';

const AuditPage = () => {
  const auditRecords = useStore(auditStoreId);

  React.useLayoutEffect(() => {
    auditRecords.load();
  }, [auditRecords]);

  return (
    <DefaultPageLayout title='Audit Report'>
      {auditRecords.hasRecords ? (
        <AuditList records={auditRecords} />
      ) : (
        <Text>There are no audit events, yet.</Text>
      )}
    </DefaultPageLayout>
  );
};

export default observer(AuditPage);
